# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='Buyer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('object_id', models.PositiveIntegerField(verbose_name='\u0417\u043d\u0430\u0447\u0435\u043d\u0438\u0435')),
                ('content_type', models.ForeignKey(verbose_name='\u041f\u043e\u043a\u0443\u043f\u0430\u0442\u0435\u043b\u044c', to='contenttypes.ContentType')),
            ],
            options={
                'verbose_name': '\u041f\u043e\u043a\u0443\u043f\u0430\u0442\u0435\u043b\u044c',
                'verbose_name_plural': '\u041f\u043e\u043a\u0443\u043f\u0430\u0442\u0435\u043b\u0438',
            },
        ),
        migrations.CreateModel(
            name='PhysicalPerson',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('first_name', models.CharField(max_length=255, verbose_name='\u0418\u043c\u044f')),
                ('middle_name', models.CharField(max_length=255, null=True, verbose_name='\u041e\u0442\u0447\u0435\u0441\u0442\u0432\u043e', blank=True)),
                ('last_name', models.CharField(max_length=255, verbose_name='\u0424\u0430\u043c\u0438\u043b\u0438\u044f')),
            ],
            options={
                'verbose_name': '\u041f\u043e\u043a\u0443\u043f\u0430\u0442\u0435\u043b\u044c',
                'verbose_name_plural': '\u041f\u043e\u043a\u0443\u043f\u0430\u0442\u0435\u043b\u0438',
            },
        ),
    ]
