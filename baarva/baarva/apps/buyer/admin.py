# -*- coding: utf-8 -*-

from django.contrib import admin

from .models import Buyer, PhysicalPerson


@admin.register(Buyer)
class BuyerAdmin(admin.ModelAdmin):

    autocomplete_lookup_fields = {
        'generic': [['content_type', 'object_id']],
    }


@admin.register(PhysicalPerson)
class PhysicalPersonAdmin(admin.ModelAdmin):
    pass
