# -*- coding: utf-8 -*-

from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType


class Buyer(models.Model):

    content_type = models.ForeignKey(
        ContentType,
        verbose_name=u'Покупатель',
        on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField(
        verbose_name=u'Значение')

    content_object = GenericForeignKey('content_type', 'object_id')

    class Meta:
        verbose_name = u'Покупатель'
        verbose_name_plural = u"Покупатели"

    def __unicode__(self):
        return unicode(self.content_object)


class PhysicalPerson(models.Model):

    first_name = models.CharField(
        verbose_name=u'Имя',
        max_length=255)

    middle_name = models.CharField(
        verbose_name=u'Отчество',
        max_length=255,
        blank=True,
        null=True)

    last_name = models.CharField(
        verbose_name=u'Фамилия',
        max_length=255)

    class Meta:
        verbose_name = u'Физическое лицо'
        verbose_name_plural = u"Физические лица"

    def __unicode__(self):

        name = " ".join(
            [attr for attr in [
                self.first_name,
                self.middle_name,
                self.last_name
            ] if attr is not None])
        return name
