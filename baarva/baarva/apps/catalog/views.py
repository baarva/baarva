# -*- coding: utf-8 -*-
import json

from django.http.response import HttpResponse
from django.shortcuts import render, get_object_or_404
from django.template.loader import render_to_string
from django.views.generic import ListView

from baarva.apps.catalog.models import Category, Product, \
    FilterGOST, GOST, \
    Filter
from baarva.apps.producer.models import FilterProducer
from baarva.apps.seller.models import Seller, TAX_TYPES, BATCH_TYPES


def product_sellers(request):
    return render(request, 'catalog/product-sellers.jhtml')


class FilterContextMixin(object):

    def get_filter_context_data(self, request, **kwargs):
        search_tax_type = self.request.session.get('tax_type', "all")
        search_batch_type = self.request.session.get('batch_type', "all")
        context = {
            'search_tax_type': TAX_TYPES[search_tax_type],
            'search_batch_type': BATCH_TYPES[search_batch_type],
            'search_tax_type_id': search_tax_type,
            'search_batch_type_id': search_batch_type
        }
        return context


class BatchFilterContextMixin(object):

    def batch_filter(self, queryset):

        batch_type = self.request.GET.get('batch_type')
        if not batch_type:
            return queryset
        if batch_type not in ['any', 'all']:
            queryset = queryset.filter(batch_type__code__in=[batch_type])

        return queryset


class TaxFilterContextMixin(object):

    def tax_filter(self, queryset):

        tax_type = self.request.GET.get('tax_type')
        if not tax_type:
            return queryset

        if tax_type in ['osn-envd', 'usn-envd']:
            tax_type = tax_type.split('-')
            queryset = queryset.filter(tax_type__code__in=tax_type)
        elif tax_type in ['any', 'all']:
            pass
        elif tax_type in ['default']:
            pass
        elif tax_type:
            queryset = queryset.filter(tax_type__code__in=[tax_type])
        return queryset


class ProductCatalogView(ListView, FilterContextMixin,
                         BatchFilterContextMixin, TaxFilterContextMixin):

    template_name = 'catalog/prod-otdel-tovara.jhtml'
    paginate_by = 5

    def get_context_data(self, **kwargs):

        context = super(ProductCatalogView, self).get_context_data(**kwargs)
        context.update(self.get_filter_context_data(self.request, **kwargs))

        filter_id_list = FilterGOST.objects.filter(
            gost__in=[self.category.gost]).values_list('filter__id', flat=True)

        filter_gost_list = FilterGOST.objects.filter(
            filter__id__in=filter_id_list)

        filter_producer_list = FilterProducer.objects.filter(
            filter__id__in=filter_id_list)

        filter_ = Filter.objects.get(id=filter_id_list[0])

        rating_product_list = Product.objects.filter(rating=True).distinct()
        popular_product_list = Product.objects.filter(
            popularity=True).distinct()

        context.update({
            'active_gost': self.category.gost,
            'seller_list': self.object_list,
            'category': self.category,
            'search_default_value': self.category.title,
            'gost_list': filter_gost_list,
            'producer_list': filter_producer_list,
            'filter': filter_,
            'rating_product_list': rating_product_list,
            'popular_product_list': popular_product_list
        })
        return context

    def get_queryset(self):

        category_slug = self.kwargs['slug']
        category = get_object_or_404(Category, slug=category_slug)
        self.category = category
        category_list = category.get_descendants(include_self=True)
        product_list = Product.objects.filter(category__in=category_list)
        product_list = product_list.none()

        seller_list = Seller.objects.filter(
            products__in=product_list).distinct()

        seller_list = self.batch_filter(seller_list)
        seller_list = self.tax_filter(seller_list)

        return product_list


def catalog_filter(request):

    response_data = {}
    if request.method == 'GET':

        seller_list = Seller.objects.all()
        filter_id = request.GET.get('filter_id')

        try:
            filter_id = int(filter_id)
        except ValueError:
            seller_list = Seller.objects.none()
        else:
            seller_list = Seller.objects.all()

        try:
            filter_ = Filter.objects.get(id=filter_id)
        except:
            seller_list = Seller.objects.none()
        else:
            filter_list = filter_.filtergost_set.all()
            gost_list = GOST.objects.filter(
                filtergost__in=filter_list).values_list('id', flat=True)
            seller_list = seller_list.filter_by_gost(gost_list)

        context = {
            'object_list': seller_list
        }

        template_name = 'blocks/seller-catalog.jhtml'
        html = render_to_string(template_name, context)

        response_data['html'] = html

        return HttpResponse(
            json.dumps(response_data),
            content_type="application/json"
        )

    return HttpResponse(
        json.dumps({
            "nothing to see": "this isn't happening",
        }),
        content_type="application/json"
    )
