# -*- coding: utf-8 -*-
from autoslug import AutoSlugField

from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

from decimal import Decimal

from ckeditor.fields import RichTextField
from mptt.models import MPTTModel, TreeForeignKey

from baarva.apps.ads.models import Ads
from baarva.apps.catalog_armatura.models import SteelLength
from baarva.apps.web.utils import custom_slugify


class Category(MPTTModel):

    title = models.CharField(
        verbose_name=u"Название",
        max_length=1024)

    slug = AutoSlugField(
        populate_from='title',
        slugify=custom_slugify,
        unique=True)

    parent = TreeForeignKey(
        'self',
        verbose_name=u"Категория",
        null=True, blank=True,
        related_name='children',
        db_index=True)

    is_leaf = models.BooleanField(
        verbose_name=u'Конечный элемент?',
        default=False)

    has_goods = models.BooleanField(
        verbose_name=u'Есть товары в базе?',
        default=False)

    gost = models.ForeignKey(
        'catalog.GOST',
        verbose_name='ГОСТ',
        blank=True,
        null=True)

    description = RichTextField(
        verbose_name=u"Описание товаров подкатегорий",
        blank=True,
        null=True
    )

    class Meta:
        verbose_name = u"Кaтегория"
        verbose_name_plural = u"Кaтегории"

    def __unicode__(self):
        return self.title


class ProductQuerySet(models.QuerySet):

    def filter_by_attr(self, ct_name, value_id_list):

        ct = ContentType.objects.get(model=ct_name)
        attrs = Attribute.objects.filter(
            content_type=ct, object_id__in=value_id_list)

        prod_attrs = ProductAttribute.objects.filter(attribute=attrs)
        prod_attrs_id = prod_attrs.values_list('id', flat=True)

        product_list = self.filter(attributes__id__in=prod_attrs_id)
        return product_list

    def get_product2seller_list(self):
        product_list = []
        seller_list = []
        for product in self:
            if product.seller not in seller_list:
                seller_list.append(product.seller)
                product_list.append(product)
        return product_list

    def filter_by_steel_length_type(self, value_id_list,
                                    start=None, stop=None):

        ct = ContentType.objects.get(model='steellength')
        attrs = Attribute.objects.filter(content_type=ct)
        prod_attrs = ProductAttribute.objects.filter(attribute=attrs)

        steel_length_id_list = SteelLength.objects.filter(
            steel_length_type__id__in=value_id_list)\
            .values_list('id', flat=True)

        prod_attrs = prod_attrs.filter(
            attribute__object_id__in=steel_length_id_list)

        prod_attrs_id = prod_attrs.values_list('id', flat=True)

        product_list = self.filter(
            attributes__id__in=prod_attrs_id).values_list('id')
        product_list = self.filter(id__in=product_list)

        if start:
            steel_length_start_id_list = SteelLength.objects\
                .filter(value_from__lte=start)\
                .filter(value_to__gte=start)\
                .values_list('id', flat=True)

            prod_attrs = prod_attrs.filter(
                attribute__object_id__in=steel_length_start_id_list)

            product_list = self.filter(attributes__id__in=prod_attrs)

            prod_attrs_id = prod_attrs.values_list('id', flat=True)
            product_list = self.filter(
                attributes__id__in=prod_attrs_id).values_list('id')
            product_list = self.filter(id__in=product_list)

        if stop:
            steel_length_start_id_list = SteelLength.objects\
                .filter(value_to__gte=stop)\
                .values_list('id', flat=True)

            prod_attrs = prod_attrs.filter(
                attribute__object_id__in=steel_length_start_id_list)

            prod_attrs_id = prod_attrs.values_list('id', flat=True)
            product_list = self.filter(
                attributes__id__in=prod_attrs_id).values_list('id')
            product_list = self.filter(id__in=product_list)

            product_list = self.filter(attributes__id__in=prod_attrs_id)

        return product_list

    def filter_by_tax_type(self, tax_type_code):
        from baarva.apps.seller.models import Seller

        seller_list = Seller.objects.filter_by_tax_type_code(tax_type_code)
        product_list = self.filter(seller__in=seller_list)
        return product_list

    def filter_by_batch_type(self, batch_type_code):

        from baarva.apps.seller.models import Seller

        seller_list = Seller.objects.filter_by_batch_type_code(batch_type_code)
        product_list = self.filter(seller__in=seller_list)
        return product_list


class Product(models.Model):

    objects = ProductQuerySet.as_manager()

    title = models.CharField(
        verbose_name=u"Название",
        max_length=255)

    category = models.ForeignKey(
        u'Category',
        verbose_name=u"Категория",
        related_name="products")

    seller = models.ForeignKey(
        u'seller.Seller',
        verbose_name=u"Продавец",
        related_name="products")

    price = models.PositiveIntegerField(
        verbose_name=u'Цена за тонну',
        default=0)

    weight = models.ForeignKey(
        u'catalog_armatura.SteelWeight',
        verbose_name=u"Вес 1м(кг)",
        blank=True,
        null=True)

    amount = models.DecimalField(
        verbose_name=u"Наличие в килограммах",
        decimal_places=4,
        max_digits=14,
        default=0)

    image = models.ImageField(
        verbose_name=u"Изображение",
        upload_to="images",
        blank=True,
        null=True)

    popularity = models.BooleanField(
        verbose_name=u'Популярность',
        default=False)

    rating = models.BooleanField(
        verbose_name=u'Рейтинговость',
        default=False)

    class Meta:
        verbose_name = u"Товар"
        verbose_name_plural = u"Товары"
        unique_together = ('title', 'seller')

    def __unicode__(self):
        return '%s(%s)' % (self.title, unicode(self.seller))

    @property
    def ads(self):
        ads = Ads.objects.filter(products=self,
                                 active=True).distinct()
        return ads

    @property
    def description(self):
        if not self.category:
            return u""
        descr = self.category.parent.description
        return descr

    @property
    def is_hank(self):
        steel_length = self.get_steel_length()
        if not steel_length:
            return False
        if steel_length.steel_length_type.title == u"в мотках":
            return True
        return False

    def get_steel_diameter(self):
        ct = ContentType.objects.get(model='steeldiameter')
        diameter = ProductAttribute.objects.filter(product=self).filter(
            attribute__content_type=ct)
        if diameter:
            diameter = diameter[0].attribute.content_object
        else:
            diameter = None
        return diameter

    def get_GOST(self):
        ct = ContentType.objects.get(model='gost')
        gost = ProductAttribute.objects.filter(product=self).filter(
            attribute__content_type=ct)
        if gost:
            gost = gost[0].attribute.content_object
        else:
            gost = None
        return gost

    def get_steel_mark(self):
        ct = ContentType.objects.get(model='steelmark')
        steel_mark = ProductAttribute.objects.filter(product=self).filter(
            attribute__content_type=ct)
        if steel_mark:
            steel_mark = steel_mark[0].attribute.content_object
        else:
            steel_mark = None
        return steel_mark

    def get_steel_class(self):
        ct = ContentType.objects.get(model='steelclass')
        steel_class = ProductAttribute.objects.filter(product=self).filter(
            attribute__content_type=ct)
        if steel_class:
            steel_class = steel_class[0].attribute.content_object
        else:
            steel_class = None
        return steel_class

    def get_steel_profile(self):
        ct = ContentType.objects.get(model='steelprofile')
        steel_profile = ProductAttribute.objects.filter(product=self).filter(
            attribute__content_type=ct)
        if steel_profile:
            steel_profile = steel_profile[0].attribute.content_object
        else:
            steel_profile = None
        return steel_profile

    def get_steel_length(self):
        ct = ContentType.objects.get(model='steellength')
        steel_length = ProductAttribute.objects.filter(product=self).filter(
            attribute__content_type=ct)
        if steel_length:
            steel_length = steel_length[0].attribute.content_object
        else:
            steel_length = None
        return steel_length

    def get_producer(self):
        ct = ContentType.objects.get(model='producer')
        producer = ProductAttribute.objects.filter(product=self).filter(
            attribute__content_type=ct)
        if producer:
            producer = producer[0].attribute.content_object
        else:
            producer = None
        return producer

    def get_amount_weigth(self):
        amount = self.amount
        return amount.quantize(Decimal('.0001'))

    def get_amount_length(self, amount=None):
        if not self.amount and not amount:
            return Decimal('0')
        if not amount:
            amount = self.amount
        value = self.get_steel_diameter().weight
        amount = amount / Decimal(value)
        return amount.quantize(Decimal('.0001'))

    def get_amount(self, amount=None):
        if not amount:
            length = self.get_amount_length()
        else:
            length = amount
        amount = length / Decimal('11.7')
        return amount.quantize(Decimal('.0001'))

    def convert_length_to_weigth(self, length):
        value = self.get_steel_diameter().weight
        weight = length * Decimal(value)
        return weight

    def convert_amount_to_weight(self, amount):
        length = amount * Decimal('11.7')
        weight = self.convert_length_to_weigth(length)
        return weight

    def convert_values(self, value, value_type):

        values = {
            'weight': 0,
            'length': 0,
            'amount': 0,
        }

        weight = value
        if value_type == 'length':
            weight = self.convert_length_to_weigth(value)
        elif value_type == 'amount':
            weight = self.convert_amount_to_weight(value)

        length = self.get_amount_length(weight)
        amount = self.get_amount(length)

        values['weight'] = float(weight)
        values['length'] = float(length)
        values['amount'] = float(amount)
        return values

    def get_price(self, value, use_discount=True):

        ads = self.ads.filter(weight__lt=value).order_by('-discount')
        price = Decimal(self.price) / 1000 * value
        if ads.exists():
            price = price - price * ads[0].discount / 100
        return price

    def gost_title(self):
        diameter = self.get_steel_diameter()
        steel_class = self.get_steel_class()
        steel_mark = self.get_steel_mark()
        steel_length = self.get_steel_length()
        if steel_length:
            steel_length_type = steel_length.steel_length_type
        else:
            steel_length_type = None
        gost = self.get_GOST()
        title = u""
        if gost and gost.number != u"52544-2006":
            if diameter:
                title = title + " " + unicode(diameter.value)
            if steel_class:
                title = title + " " + steel_class.title
            if steel_mark:
                title = title + " " + steel_mark.title
        else:
            if steel_length_type:
                if steel_length_type.title == u"в мотках":
                    title = u"моток"
                    if diameter:
                        title = title + " " + unicode(diameter.value)
                    if steel_class:
                        title = title + " " + steel_class.title
                else:
                    if steel_length_type.title == u"мерная":
                        title = u"МД"
                    elif steel_length_type.title == u"немерная":
                        title = u"НД"
                    if diameter:
                        title = title + " " + unicode(diameter.value)
                    if steel_length:
                        title = title + " " + u"11.7"
                    if steel_class:
                        title = title + " " + steel_class.title
        return title


@receiver(post_save, sender=Product)
def updateCategory(sender, **kwargs):
    instance = kwargs['instance']
    category = instance.category

    category.has_goods = True
    category.save()

    parent = category.parent
    while parent:
        parent.has_goods = True
        parent.save()
        parent = parent.parent


class GOST(models.Model):

    number = models.CharField(
        verbose_name=u"Номер ГОСТа",
        max_length=255)

    gost_type = models.CharField(
        verbose_name=u"Тип ГОСТа",
        max_length=255)

    title = models.CharField(
        verbose_name=u"Название ГОСТа",
        max_length=1024)

    class Meta:
        verbose_name = u"ГОСТ"
        verbose_name_plural = u"ГОСТы"

    def __unicode__(self):
        return "%s %s (%s)" % (self.gost_type, self.number, self.title)


class Attribute(models.Model):

    title = models.CharField(
        verbose_name=u"Название",
        max_length=255)

    description = models.TextField(
        verbose_name=u"Описание",
        blank=True,
        null=True)

    order = models.PositiveIntegerField(
        verbose_name=u'Порядок',
        default=0,
        blank=False,
        null=False)

    content_type = models.ForeignKey(
        ContentType,
        verbose_name=u'Тип атрибута',
        on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField(
        verbose_name=u'Значение',
        db_index=True)

    content_object = GenericForeignKey('content_type', 'object_id')

    class Meta:
        verbose_name = u"Атрибут"
        verbose_name_plural = u"Атрибуты"
        ordering = ('-order', )

    def __unicode__(self):
        return self.title


class ProductAttribute(models.Model):

    product = models.ForeignKey(
        u'catalog.Product',
        verbose_name=u"Продукт",
        related_name='attributes')

    attribute = models.ForeignKey(
        u'catalog.Attribute',
        verbose_name=u"Атрибут")

    class Meta:
        verbose_name = u"Значение атрибута"
        verbose_name_plural = u"Значения атрибутов"
        unique_together = ('product', 'attribute')

    def __unicode__(self):
        return unicode(self.product) + " " + unicode(self.attribute)


class FilterGOST(models.Model):

    gost = models.OneToOneField(
        'catalog.GOST',
        verbose_name='Гост')

    filter = models.ForeignKey(
        'catalog.Filter',
        verbose_name='Фильтр',
        blank=True,
        null=True)

    class Meta:
        verbose_name = u"Гост для фильтра"
        verbose_name_plural = u"Госты для фильтра"

    def __unicode__(self):
        return unicode(self.gost)


class Filter(models.Model):

    title = models.CharField(
        verbose_name=u'Название фильтра',
        max_length=255)

    slug = AutoSlugField(
        populate_from='title',
        slugify=custom_slugify,
        unique=True)

    group = models.OneToOneField(
        'catalog.FilterGroup',
        verbose_name='Категория фильтра')

    class Meta:
        verbose_name = u"Фильтр"
        verbose_name_plural = u"Фильтры"

    def __unicode__(self):
        return u"Фильтр %s в группе %s" % (self.title, self.group.title)


class FilterGroup(models.Model):

    title = models.CharField(
        verbose_name=u'Название группы',
        max_length=255)

    slug = AutoSlugField(
        populate_from='title',
        slugify=custom_slugify,
        unique=True)

    class Meta:
        verbose_name = u"Группа фильтра"
        verbose_name_plural = u"Группы фильтров"

    def __unicode__(self):
        return u"Группа фильтра %s" % self.title
