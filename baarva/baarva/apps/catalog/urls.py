# -*- coding: utf-8 -*-

from django.conf.urls import url
from .views import product_sellers, \
    ProductCatalogView, catalog_filter


urlpatterns = [
    url(r'product/sellers/$', product_sellers, name='product_sellers'),
    url(r'section-(?P<slug>[^/]*)/$',
        ProductCatalogView.as_view(),
        name='seller_catalog'),
    url(r'filter/$',
        catalog_filter,
        name='catalog_filter')
]
