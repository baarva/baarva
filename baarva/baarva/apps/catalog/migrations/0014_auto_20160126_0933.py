# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0013_auto_20160126_0910'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='category',
            field=models.ForeignKey(related_name='products', verbose_name='\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u044f', to='catalog.Category'),
        ),
        migrations.AlterField(
            model_name='product',
            name='seller',
            field=models.ForeignKey(related_name='products', verbose_name='\u041f\u0440\u043e\u0434\u0430\u0432\u0435\u0446', to='seller.Seller'),
        ),
        migrations.AlterField(
            model_name='productattribute',
            name='product',
            field=models.ForeignKey(related_name='attributes', verbose_name='\u041f\u0440\u043e\u0434\u0443\u043a\u0442', to='catalog.Product'),
        ),
    ]
