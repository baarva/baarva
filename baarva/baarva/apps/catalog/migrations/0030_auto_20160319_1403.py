# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0029_auto_20160319_1312'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='attribute',
            options={'ordering': ('-order',), 'verbose_name': '\u0410\u0442\u0440\u0438\u0431\u0443\u0442', 'verbose_name_plural': '\u0410\u0442\u0440\u0438\u0431\u0443\u0442\u044b'},
        ),
    ]
