# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0023_auto_20160301_1034'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='popularity',
            field=models.BooleanField(default=False, verbose_name='\u041f\u043e\u043f\u0443\u043b\u044f\u0440\u043d\u043e\u0441\u0442\u044c'),
        ),
        migrations.AddField(
            model_name='product',
            name='rating',
            field=models.BooleanField(default=False, verbose_name='\u0420\u0435\u0439\u0442\u0438\u043d\u0433\u043e\u0432\u043e\u0441\u0442\u044c'),
        ),
    ]
