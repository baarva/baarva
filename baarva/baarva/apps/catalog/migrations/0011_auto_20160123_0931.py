# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0010_auto_20160123_0855'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='attribute',
            options={'verbose_name': '\u0410\u0442\u0440\u0438\u0431\u0443\u0442', 'verbose_name_plural': '\u0410\u0442\u0440\u0438\u0431\u0443\u0442\u044b'},
        ),
        migrations.AlterModelOptions(
            name='gost',
            options={'verbose_name': '\u0413\u041e\u0421\u0422', 'verbose_name_plural': '\u0413\u041e\u0421\u0422\u044b'},
        ),
    ]
