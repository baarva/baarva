# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0011_auto_20160123_0931'),
    ]

    operations = [
        migrations.AddField(
            model_name='gost',
            name='gost_type',
            field=models.CharField(default='\u0413\u043e\u0441\u0442', max_length=255, verbose_name='\u0422\u0438\u043f \u0413\u041e\u0421\u0422\u0430'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='gost',
            name='number',
            field=models.CharField(default='1', max_length=255, verbose_name='\u041d\u043e\u043c\u0435\u0440 \u0413\u041e\u0421\u0422\u0430'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='gost',
            name='title',
            field=models.CharField(max_length=1024, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u0413\u041e\u0421\u0422\u0430'),
        ),
    ]
