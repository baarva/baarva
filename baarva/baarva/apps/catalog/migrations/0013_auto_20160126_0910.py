# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0012_auto_20160124_0913'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='productattribute',
            unique_together=set([('product', 'attribute')]),
        ),
    ]
