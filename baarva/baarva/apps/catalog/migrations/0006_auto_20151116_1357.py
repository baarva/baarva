# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import autoslug.fields
import baarva.apps.catalog.models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0005_category_slug'),
    ]

    operations = [
        migrations.AlterField(
            model_name='category',
            name='slug',
            field=autoslug.fields.AutoSlugField(populate_from=b'title', editable=False, slugify=baarva.apps.catalog.models.custom_slugify),
        ),
    ]
