# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import autoslug.fields
import baarva.apps.web.utils


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0019_auto_20160211_1203'),
    ]

    operations = [
        migrations.CreateModel(
            name='Filter',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u0444\u0438\u043b\u044c\u0442\u0440\u0430')),
                ('slug', autoslug.fields.AutoSlugField(editable=False, populate_from=b'title', unique=True, slugify=baarva.apps.web.utils.custom_slugify)),
            ],
            options={
                'verbose_name': '\u0424\u0438\u043b\u044c\u0442\u0440',
                'verbose_name_plural': '\u0424\u0438\u043b\u044c\u0442\u0440\u044b',
            },
        ),
        migrations.CreateModel(
            name='FilterGOST',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('filter', models.ForeignKey(verbose_name=b'\xd0\xa4\xd0\xb8\xd0\xbb\xd1\x8c\xd1\x82\xd1\x80', blank=True, to='catalog.Filter', null=True)),
                ('gost', models.OneToOneField(verbose_name=b'\xd0\x93\xd0\xbe\xd1\x81\xd1\x82', to='catalog.GOST')),
            ],
            options={
                'verbose_name': '\u0413\u043e\u0441\u0442 \u0434\u043b\u044f \u0444\u0438\u043b\u044c\u0442\u0440\u0430',
                'verbose_name_plural': '\u0413\u043e\u0441\u0442\u044b \u0434\u043b\u044f \u0444\u0438\u043b\u044c\u0442\u0440\u0430',
            },
        ),
        migrations.CreateModel(
            name='FilterGroup',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u0433\u0440\u0443\u043f\u043f\u044b')),
                ('slug', autoslug.fields.AutoSlugField(editable=False, populate_from=b'title', unique=True, slugify=baarva.apps.web.utils.custom_slugify)),
            ],
            options={
                'verbose_name': '\u0413\u0440\u0443\u043f\u043f\u0430 \u0444\u0438\u043b\u044c\u0442\u0440\u0430',
                'verbose_name_plural': '\u0413\u0440\u0443\u043f\u043f\u044b \u0444\u0438\u043b\u044c\u0442\u0440\u043e\u0432',
            },
        ),
        migrations.AddField(
            model_name='filter',
            name='group',
            field=models.OneToOneField(verbose_name=b'\xd0\x9a\xd0\xb0\xd1\x82\xd0\xb5\xd0\xb3\xd0\xbe\xd1\x80\xd0\xb8\xd1\x8f \xd1\x84\xd0\xb8\xd0\xbb\xd1\x8c\xd1\x82\xd1\x80\xd0\xb0', to='catalog.FilterGroup'),
        ),
    ]
