# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0017_auto_20160211_0201'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='has_goods',
            field=models.BooleanField(default=False, verbose_name='\u0415\u0441\u0442\u044c \u0442\u043e\u0432\u0430\u0440\u044b \u0432 \u0431\u0430\u0437\u0435?'),
        ),
    ]
