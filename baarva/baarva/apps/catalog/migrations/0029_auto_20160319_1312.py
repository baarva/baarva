# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0028_product_amount'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='attribute',
            options={'ordering': ('order',), 'verbose_name': '\u0410\u0442\u0440\u0438\u0431\u0443\u0442', 'verbose_name_plural': '\u0410\u0442\u0440\u0438\u0431\u0443\u0442\u044b'},
        ),
        migrations.AddField(
            model_name='attribute',
            name='order',
            field=models.PositiveIntegerField(default=0, verbose_name='\u041f\u043e\u0440\u044f\u0434\u043e\u043a'),
        ),
    ]
