# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0020_auto_20160223_0544'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='price',
            field=models.PositiveIntegerField(default=0, verbose_name='\u0426\u0435\u043d\u0430 \u0437\u0430 \u0442\u043e\u043d\u043d\u0443'),
        ),
    ]
