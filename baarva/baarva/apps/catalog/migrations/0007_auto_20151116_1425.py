# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import autoslug.fields
import baarva.apps.catalog.models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0006_auto_20151116_1357'),
    ]

    operations = [
        migrations.AlterField(
            model_name='category',
            name='slug',
            field=autoslug.fields.AutoSlugField(editable=False, populate_from=b'title', always_update=True, slugify=baarva.apps.catalog.models.custom_slugify),
        ),
    ]
