# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('seller', '0001_initial'),
        ('catalog', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('image', models.ImageField(upload_to=b'', verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435')),
                ('popularity', models.IntegerField(verbose_name='\u041f\u043e\u043f\u0443\u043b\u044f\u0440\u043d\u043e\u0441\u0442\u044c')),
                ('rating', models.IntegerField(verbose_name='\u0420\u0435\u0439\u0442\u0438\u043d\u0433\u043e\u0432\u043e\u0441\u0442\u044c')),
            ],
            options={
                'verbose_name': '\u0422\u043e\u0432\u0430\u0440',
                'verbose_name_plural': '\u0422\u043e\u0432\u0430\u0440\u044b',
            },
        ),
        migrations.AlterModelOptions(
            name='category',
            options={'verbose_name': '\u041aa\u0442\u0435\u0433\u043e\u0440\u0438\u044f', 'verbose_name_plural': '\u041aa\u0442\u0435\u0433\u043e\u0440\u0438\u0438'},
        ),
        migrations.AddField(
            model_name='product',
            name='category',
            field=models.ForeignKey(verbose_name='\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u044f', to='catalog.Category'),
        ),
        migrations.AddField(
            model_name='product',
            name='seller',
            field=models.ForeignKey(verbose_name='\u041f\u043e\u0441\u0442\u0430\u0432\u0449\u0438\u043a', to='seller.Seller'),
        ),
    ]
