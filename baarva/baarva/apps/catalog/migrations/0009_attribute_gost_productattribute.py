# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        ('catalog', '0008_auto_20151121_0919'),
    ]

    operations = [
        migrations.CreateModel(
            name='Attribute',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('description', models.TextField(verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435')),
                ('object_id', models.PositiveIntegerField()),
                ('category', models.ManyToManyField(to='catalog.Category', verbose_name=b'\xd0\x9a\xd0\xb0\xd1\x82\xd0\xb5\xd0\xb3\xd0\xbe\xd1\x80\xd0\xb8\xd0\xb8')),
                ('content_type', models.ForeignKey(to='contenttypes.ContentType')),
            ],
            options={
                'verbose_name': '\u0410\u0442\u0442\u0440\u0438\u0431\u0443\u0442',
                'verbose_name_plural': '\u0410\u0442\u0442\u0440\u0438\u0431\u0443\u0442\u044b',
            },
        ),
        migrations.CreateModel(
            name='GOST',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
            ],
            options={
                'verbose_name': '\u0413\u041e\u0421\u0422',
                'verbose_name_plural': '\u0413\u041e\u0421\u0422\u042b',
            },
        ),
        migrations.CreateModel(
            name='ProductAttribute',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('attribute', models.ForeignKey(verbose_name='\u0410\u0442\u0440\u0438\u0431\u0443\u0442', to='catalog.Attribute')),
                ('product', models.ForeignKey(verbose_name='\u041f\u0440\u043e\u0434\u0443\u043a\u0442', to='catalog.Product')),
            ],
        ),
    ]
