# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0014_auto_20160126_0933'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='gost',
            field=models.ForeignKey(verbose_name=b'\xd0\x93\xd0\x9e\xd0\xa1\xd0\xa2', blank=True, to='catalog.GOST', null=True),
        ),
        migrations.AddField(
            model_name='category',
            name='is_leaf',
            field=models.BooleanField(default=False, verbose_name='\u041a\u043e\u043d\u0435\u0447\u043d\u044b\u0439 \u044d\u043b\u0435\u043c\u0435\u043d\u0442?'),
        ),
        migrations.AlterField(
            model_name='attribute',
            name='content_type',
            field=models.ForeignKey(verbose_name='\u0422\u0438\u043f \u0430\u0442\u0440\u0438\u0431\u0443\u0442\u0430', to='contenttypes.ContentType'),
        ),
        migrations.AlterField(
            model_name='attribute',
            name='object_id',
            field=models.PositiveIntegerField(verbose_name='\u0417\u043d\u0430\u0447\u0435\u043d\u0438\u0435'),
        ),
    ]
