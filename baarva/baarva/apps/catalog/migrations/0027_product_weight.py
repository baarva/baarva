# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog_armatura', '0015_auto_20160302_1229'),
        ('catalog', '0026_remove_product_weigth'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='weight',
            field=models.ForeignKey(verbose_name='\u0412\u0435\u0441 1\u043c(\u043a\u0433)', blank=True, to='catalog_armatura.SteelWeight', null=True),
        ),
    ]
