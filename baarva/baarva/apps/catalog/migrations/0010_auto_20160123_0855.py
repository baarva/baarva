# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0009_attribute_gost_productattribute'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='productattribute',
            options={'verbose_name': '\u0417\u043d\u0430\u0447\u0435\u043d\u0438\u0435 \u0430\u0442\u0440\u0438\u0431\u0443\u0442\u0430', 'verbose_name_plural': '\u0417\u043d\u0430\u0447\u0435\u043d\u0438\u044f \u0430\u0442\u0440\u0438\u0431\u0443\u0442\u043e\u0432'},
        ),
    ]
