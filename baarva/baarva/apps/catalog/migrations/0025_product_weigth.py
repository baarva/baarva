# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0024_auto_20160301_1034'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='weigth',
            field=models.DecimalField(default=0, verbose_name='\u0412\u0435\u0441 1\u043c', max_digits=14, decimal_places=6),
        ),
    ]
