# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import autoslug.fields
import baarva.apps.web.utils


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0016_auto_20160211_0147'),
    ]

    operations = [
        migrations.AlterField(
            model_name='category',
            name='slug',
            field=autoslug.fields.AutoSlugField(editable=False, populate_from=b'title', unique=True, slugify=baarva.apps.web.utils.custom_slugify),
        ),
    ]
