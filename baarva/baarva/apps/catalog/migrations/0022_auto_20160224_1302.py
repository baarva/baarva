# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0021_product_price'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='attribute',
            name='category',
        ),
        migrations.AlterField(
            model_name='attribute',
            name='description',
            field=models.TextField(null=True, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True),
        ),
    ]
