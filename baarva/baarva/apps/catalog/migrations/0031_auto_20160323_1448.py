# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0030_auto_20160319_1403'),
    ]

    operations = [
        migrations.AlterField(
            model_name='attribute',
            name='object_id',
            field=models.PositiveIntegerField(verbose_name='\u0417\u043d\u0430\u0447\u0435\u043d\u0438\u0435', db_index=True),
        ),
    ]
