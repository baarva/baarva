# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0022_auto_20160224_1302'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='product',
            name='popularity',
        ),
        migrations.RemoveField(
            model_name='product',
            name='rating',
        ),
    ]
