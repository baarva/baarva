# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import mptt.fields


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0007_auto_20151116_1425'),
    ]

    operations = [
        migrations.AlterField(
            model_name='category',
            name='parent',
            field=mptt.fields.TreeForeignKey(related_name='children', verbose_name='\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u044f', blank=True, to='catalog.Category', null=True),
        ),
    ]
