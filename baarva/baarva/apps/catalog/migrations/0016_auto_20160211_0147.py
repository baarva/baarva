# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import autoslug.fields
import baarva.apps.web.utils


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0015_auto_20160208_1306'),
    ]

    operations = [
        migrations.AlterField(
            model_name='category',
            name='slug',
            field=autoslug.fields.AutoSlugField(editable=False, populate_from=b'title', always_update=True, unique=True, slugify=baarva.apps.web.utils.custom_slugify),
        ),
        migrations.AlterField(
            model_name='category',
            name='title',
            field=models.CharField(max_length=1024, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435'),
        ),
    ]
