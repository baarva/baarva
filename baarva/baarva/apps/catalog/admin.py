# -*- coding: utf-8 -*-
from adminsortable2.admin import SortableAdminMixin

from django import forms

from django.contrib import admin
from django.db import models

from feincms.admin import tree_editor
from mptt.forms import TreeNodeChoiceField

from .models import Category, Product, GOST, Attribute,\
    ProductAttribute, Filter, FilterGOST, FilterGroup


@admin.register(Category)
class CategoryAdmin(tree_editor.TreeEditor):
    search_fields = ('title',)


@admin.register(GOST)
class GOSTAdmin(admin.ModelAdmin):
    search_fields = ('title',)


@admin.register(Attribute)
class AttributeAdmin(SortableAdminMixin, admin.ModelAdmin):

    fieldsets = (
        (None, {
            'fields': (
                'title',
                'description',
            )
        }),
        (u"Тип атрибута", {
            'classes': ('grp-collapse grp-open',),
            'fields': ('content_type', 'object_id', )
        }),
    )
    autocomplete_lookup_fields = {
        'generic': [['content_type', 'object_id']],
    }


"""
@admin.register(ProductAttribute)
class ProductAttributeAdmin(admin.ModelAdmin):
    search_fields = ('title',)
    list_display = ('__unicode__', 'get_seller')
    list_filter = ('product__seller',)

    def get_seller(self, obj):
        return obj.product.seller
    get_seller.short_description = u'Продавец'
    get_seller.admin_order_field = 'product__seller'
"""


class ProductAttributeInlineAdmin(admin.TabularInline):
    model = ProductAttribute


class ProductForm(forms.ModelForm):
    category = TreeNodeChoiceField(
        queryset=Category.objects.all())

    class Meta:
        model = Product
        exclude = ()


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = (
        'title', 'gost_title', 'seller',
        'price', 'popularity', 'rating', 'attrs')
    list_editable = ('price', 'popularity', 'rating')
    search_fields = ('title',)
    exclude = ('weight',)
    list_filter = ('seller', 'category',)
    form = ProductForm
    inlines = [ProductAttributeInlineAdmin]

    def get_queryset(self, request):
        qs = super(ProductAdmin, self).get_queryset(request)
        qs = qs.annotate(models.Count('attributes'))
        return qs

    def attrs(self, instance):
        return instance.attributes.count()
    attrs.admin_order_field = 'attributes__count'
    attrs.short_description = 'Количество атирибутов'

    def gost_title(self, instance):
        title = instance.gost_title()
        gost = instance.get_GOST()
        if gost:
            title = "%s %s %s" % (title, gost.gost_type, gost.number)
        return title
    gost_title.short_description = 'Название по госту'


@admin.register(FilterGOST)
class FilterGOSTAdmin(admin.ModelAdmin):
    pass


@admin.register(Filter)
class FilterAdmin(admin.ModelAdmin):
    pass


@admin.register(FilterGroup)
class FilterGroupAdmin(admin.ModelAdmin):
    pass
