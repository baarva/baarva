# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0030_auto_20160319_1403'),
        ('producer', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='FilterProducer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('filter', models.ForeignKey(verbose_name=b'\xd0\xa4\xd0\xb8\xd0\xbb\xd1\x8c\xd1\x82\xd1\x80', blank=True, to='catalog.Filter', null=True)),
                ('producer', models.OneToOneField(verbose_name=b'\xd0\x9f\xd1\x80\xd0\xbe\xd0\xb8\xd0\xb7\xd0\xb2\xd0\xbe\xd0\xb4\xd0\xb8\xd1\x82\xd0\xb5\xd0\xbb\xd1\x8c', to='producer.Producer')),
            ],
            options={
                'verbose_name': '\u041f\u0440\u043e\u0438\u0437\u0432\u043e\u0434\u0438\u0442\u0435\u043b \u0434\u043b\u044f \u0444\u0438\u043b\u044c\u0442\u0440\u0430',
                'verbose_name_plural': '\u041f\u0440\u043e\u0438\u0437\u0432\u043e\u0434\u0438\u0442\u0435\u043b\u0438 \u0434\u043b\u044f \u0444\u0438\u043b\u044c\u0442\u0440\u0430',
            },
        ),
    ]
