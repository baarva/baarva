# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('producer', '0002_filterproducer'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='filterproducer',
            options={'verbose_name': '\u041f\u0440\u043e\u0438\u0437\u0432\u043e\u0434\u0438\u0442\u0435\u043b\u044c \u0434\u043b\u044f \u0444\u0438\u043b\u044c\u0442\u0440\u0430', 'verbose_name_plural': '\u041f\u0440\u043e\u0438\u0437\u0432\u043e\u0434\u0438\u0442\u0435\u043b\u0438 \u0434\u043b\u044f \u0444\u0438\u043b\u044c\u0442\u0440\u0430'},
        ),
    ]
