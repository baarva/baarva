# -*- coding: utf-8 -*-

from django.contrib import admin

from .models import Producer, FilterProducer


@admin.register(Producer)
class ProducerAdmin(admin.ModelAdmin):
    search_fields = ('title',)


@admin.register(FilterProducer)
class FilterProducerAdmin(admin.ModelAdmin):
    pass
