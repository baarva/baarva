# -*- coding: utf-8 -*-

from django.db import models

from autoslug import AutoSlugField

from baarva.apps.web.utils import custom_slugify


class Producer(models.Model):

    title = models.CharField(
        verbose_name=u"Название",
        max_length=1024)

    slug = AutoSlugField(
        populate_from='title',
        slugify=custom_slugify,
        unique=True)

    class Meta:
        verbose_name = u'Производитель'
        verbose_name_plural = u"Производители"

    def __unicode__(self):
        return self.title


class FilterProducer(models.Model):

    producer = models.OneToOneField(
        'producer.Producer',
        verbose_name='Производитель')

    filter = models.ForeignKey(
        'catalog.Filter',
        verbose_name='Фильтр',
        blank=True,
        null=True)

    class Meta:
        verbose_name = u"Производитель для фильтра"
        verbose_name_plural = u"Производители для фильтра"

    def __unicode__(self):
        return unicode(self.producer)
