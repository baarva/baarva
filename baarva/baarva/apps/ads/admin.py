# -*- coding: utf-8 -*-

from django.contrib import admin

from .models import Ads
from baarva.apps.seller.models import Seller


@admin.register(Ads)
class AdsAdmin(admin.ModelAdmin):

    list_display = ('__unicode__', 'get_seller')
    filter_horizontal = ('products',)

    def get_seller(self, instance):
        seller_id_list = set(
            instance.products.values_list('seller__id', flat=True))

        seller_list = Seller.objects.filter(id__in=seller_id_list)

        return " ".join([unicode(seller) for seller in seller_list])
    get_seller.short_description = u'Продавец'
