# -*- coding: utf-8 -*-

from django.db import models

from ckeditor.fields import RichTextField


class Ads(models.Model):

    products = models.ManyToManyField(
        'catalog.Product',
        verbose_name=u"Товары по акции",
    )

    pub_date = models.DateField(
        verbose_name=u'Дата публикации')

    end_date = models.DateField(
        verbose_name=u'Дата Окончания')

    active = models.BooleanField(
        verbose_name=u"Активна",
        default=True)

    weight = models.DecimalField(
        verbose_name=u'Вес от',
        max_digits=14,
        decimal_places=2)

    discount = models.DecimalField(
        verbose_name=u'Скидка в процентах',
        max_digits=4,
        decimal_places=2)

    title = models.CharField(
        verbose_name=u"Заголовок",
        max_length=255)

    content = RichTextField(
        verbose_name=u"Содержание",
    )

    class Meta:
        verbose_name = u'Рекламная акция'
        verbose_name_plural = u'Рекламные акции'
        ordering = ['-pub_date']

    def __unicode__(self):
        return self.title
