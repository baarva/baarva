# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ads', '0003_ads_products'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='ads',
            name='seller',
        ),
    ]
