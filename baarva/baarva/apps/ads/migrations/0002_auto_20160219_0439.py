# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ads', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='ads',
            options={'ordering': ['-pub_date'], 'verbose_name': '\u0420\u0435\u043a\u043b\u0430\u043c\u043d\u0430\u044f \u0430\u043a\u0446\u0438\u044f', 'verbose_name_plural': '\u0420\u0435\u043a\u043b\u0430\u043c\u043d\u044b\u0435 \u0430\u043a\u0446\u0438\u0438'},
        ),
    ]
