# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('seller', '0013_seller_about'),
    ]

    operations = [
        migrations.CreateModel(
            name='Ads',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('pub_date', models.DateField(verbose_name='\u0414\u0430\u0442\u0430 \u043f\u0443\u0431\u043b\u0438\u043a\u0430\u0446\u0438\u0438')),
                ('end_date', models.DateField(verbose_name='\u0414\u0430\u0442\u0430 \u041e\u043a\u043e\u043d\u0447\u0430\u043d\u0438\u044f')),
                ('title', models.CharField(max_length=255, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a')),
                ('content', ckeditor.fields.RichTextField(verbose_name='\u0421\u043e\u0434\u0435\u0440\u0436\u0430\u043d\u0438\u0435')),
                ('seller', models.ForeignKey(related_name='ads', verbose_name='\u041f\u0440\u043e\u0434\u0430\u0432\u0435\u0446', to='seller.Seller')),
            ],
            options={
                'ordering': ['-pub_date'],
                'verbose_name': '\u0420\u0435\u043a\u043b\u0430\u043c\u043d\u0430\u044f \u0430\u043a\u0446\u0438\u044f',
                'verbose_name_plural': '\u0420\u0435\u043a\u0430\u043b\u043d\u044b\u0435 \u0430\u043a\u0446\u0438\u0438',
            },
        ),
    ]
