# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ads', '0005_auto_20160422_0038'),
    ]

    operations = [
        migrations.RenameField(
            model_name='ads',
            old_name='is_active',
            new_name='active',
        ),
    ]
