# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ads', '0006_auto_20160422_0040'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ads',
            name='products',
            field=models.ManyToManyField(to='catalog.Product', verbose_name='\u0422\u043e\u0432\u0430\u0440\u044b \u043f\u043e \u0430\u043a\u0446\u0438\u0438'),
        ),
    ]
