# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0028_product_amount'),
        ('ads', '0002_auto_20160219_0439'),
    ]

    operations = [
        migrations.AddField(
            model_name='ads',
            name='products',
            field=models.ManyToManyField(related_name='ads', verbose_name='\u0422\u043e\u0432\u0430\u0440\u044b \u043f\u043e \u0430\u043a\u0446\u0438\u0438', to='catalog.Product'),
        ),
    ]
