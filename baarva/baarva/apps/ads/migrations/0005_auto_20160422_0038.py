# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ads', '0004_remove_ads_seller'),
    ]

    operations = [
        migrations.AddField(
            model_name='ads',
            name='discount',
            field=models.FloatField(default=0, verbose_name='\u0421\u043a\u0438\u0434\u043a\u0430 \u0432 \u043f\u0440\u043e\u0446\u0435\u043d\u0442\u0430\u0445'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='ads',
            name='is_active',
            field=models.BooleanField(default=True, verbose_name='\u0410\u043a\u0442\u0438\u0432\u043d\u0430'),
        ),
        migrations.AddField(
            model_name='ads',
            name='weight',
            field=models.FloatField(default=0, verbose_name='\u0412\u0435\u0441 \u043e\u0442'),
            preserve_default=False,
        ),
    ]
