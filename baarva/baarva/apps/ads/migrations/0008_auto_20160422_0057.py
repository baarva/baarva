# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ads', '0007_auto_20160422_0048'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ads',
            name='discount',
            field=models.DecimalField(verbose_name='\u0421\u043a\u0438\u0434\u043a\u0430 \u0432 \u043f\u0440\u043e\u0446\u0435\u043d\u0442\u0430\u0445', max_digits=4, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='ads',
            name='weight',
            field=models.DecimalField(verbose_name='\u0412\u0435\u0441 \u043e\u0442', max_digits=14, decimal_places=2),
        ),
    ]
