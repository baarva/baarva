# -*- coding: utf-8 -*-

from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'register/general/$',
        views.RegisterGeneralView.as_view(),
        name='register_general'),
    url(r'register/juridical/$',
        views.RegisterJuridicalView.as_view(),
        name='register_juridical'),
    url(r'seller-(?P<pk>[^/]*)/$',
        views.SellerView.as_view(),
        name='seller')
]
