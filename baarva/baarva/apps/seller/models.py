# -*- coding: utf-8 -*-

from ckeditor.fields import RichTextField

from django.contrib.contenttypes.models import ContentType
from django.db import models

from embed_video.fields import EmbedVideoField
from embed_video.backends import YoutubeBackend

from baarva.apps.ads.models import Ads
from baarva.apps.catalog.models import Attribute, \
    ProductAttribute, Product


TAX_TYPES = {
    "all": u"Все",
    "default": u"По умолчанию",
    "envd": u"ЕНВД",
    "usn": u"УСН",
    "psn": u"ПСН",
    "eshn": u"ЕСХН",
    "osn-envd": u"ОСН-ЕНВД",
    "usn-envd": u"УСН-ЕНВД",
    "osn": u"ОСН"
}

BATCH_TYPES = {
    "all": u"Все",
    "opt": u"Опт",
    'one': u"Розница",
    'any': u'Опт / Розница',
    'opt-one': u'Опт / Розница'
}


OWNER_TYPE = [
    ("ooo", u"ООО"),
    ("zao", u"ЗАО"),
    ("pao", u"ПАО"),
]


class SellerQuerySet(models.QuerySet):

    def filter_by_gost(self, gost_id_list):

        ct = ContentType.objects.get(model='gost')
        attrs = Attribute.objects.filter(
            content_type=ct, object_id__in=gost_id_list)

        prod_attrs = ProductAttribute.objects.filter(attribute=attrs)
        products = Product.objects.filter(attributes=prod_attrs)

        seller_list = self.filter(products=products).distinct()
        return seller_list

    def filter_by_steel_class(self, class_id_list):

        ct = ContentType.objects.get(model='steelclass')
        attrs = Attribute.objects.filter(
            content_type=ct, object_id__in=class_id_list)

        prod_attrs = ProductAttribute.objects.filter(attribute=attrs)
        products = Product.objects.filter(attributes=prod_attrs)

        seller_list = self.filter(products=products).distinct()
        return seller_list

    def filter_by_profile(self, profile_id_list):

        ct = ContentType.objects.get(model='steelprofile')
        attrs = Attribute.objects.filter(
            content_type=ct, object_id__in=profile_id_list)

        prod_attrs = ProductAttribute.objects.filter(attribute=attrs)
        products = Product.objects.filter(attributes=prod_attrs)

        seller_list = self.filter(products=products).distinct()
        return seller_list

    def filter_by_diameter(self, diameter_id_list):

        ct = ContentType.objects.get(model='steeldiameter')
        attrs = Attribute.objects.filter(
            content_type=ct, object_id=diameter_id_list)

        prod_attrs = ProductAttribute.objects.filter(attribute=attrs)
        products = Product.objects.filter(attributes=prod_attrs)

        seller_list = self.filter(products=products).distinct()
        return seller_list

    def filter_by_tax_type_code(self, code):

        if code in ["all", "default"]:
            return self
        code = code.split('-')
        seller_list = self.filter(tax_type__code__in=code)
        return seller_list

    def filter_by_batch_type_code(self, code):

        if code in ["all"]:
            return self
        elif code in ['any']:
            seller_list = self.filter(batch_type__code__in=["opt"])
            seller_list = seller_list.filter(batch_type__code__in=["one"])
        else:
            seller_list = self.filter(batch_type__code__in=[code])
        return seller_list


class Seller(models.Model):

    objects = SellerQuerySet.as_manager()

    title = models.CharField(
        verbose_name=u"Название",
        max_length=255)

    image = models.ImageField(verbose_name=u"Логотип",
                              upload_to="images",
                              blank=True,
                              null=True)

    address = models.ForeignKey(
        'address.Address',
        verbose_name=u"Адрес",
        related_name='sellers',
        blank=True,
        null=True,
    )

    phone = models.CharField(verbose_name=u"Телефон",
                             max_length=255,
                             blank=True,
                             null=True)

    site = models.URLField(verbose_name='Сайт компании',
                           blank=True,
                           null=True)

    owner_type = models.CharField(
        verbose_name=u"Вид собственности",
        max_length=255,
        choices=OWNER_TYPE,
        blank=True,
        null=True)

    main_activity_category = models.ForeignKey(
        'seller.ActivityCategory',
        verbose_name=u"Основной вид деятельности",
        related_name='main_activity_sellers')

    activity_category_list = models.ManyToManyField(
        u'seller.ActivityCategory',
        verbose_name=u"Виды деятельности",
        related_name='activity_sellers',
        blank=True)

    tax_type = models.ManyToManyField(
        u'seller.TaxType',
        verbose_name=u"Налогообложение")

    batch_type = models.ManyToManyField(
        u'seller.BatchType',
        verbose_name=u"Опт/Розница")

    about = RichTextField(
        verbose_name=u"О компании",
        blank=True,
        null=True)

    about_company_video = EmbedVideoField(
        verbose_name=u"О компании видео",
        blank=True,
        null=True)

    seller_rating = models.PositiveIntegerField(
        verbose_name=u'Ретинг продажи',
        default=0)

    buyer_rating = models.PositiveIntegerField(
        verbose_name=u'Ретинг покупки',
        default=0)

    quality_rating = models.PositiveIntegerField(
        verbose_name=u'Ретинг качества',
        default=0)

    class Meta:

        verbose_name = u"Юридическое лицо"
        verbose_name_plural = u"Юридические лица"

    def __unicode__(self):
        return self.title

    def get_about_video_backend(self):
        if not self.about_company_video:
            return None
        backend = YoutubeBackend(self.about_company_video)
        return backend

    @property
    def ads(self):
        ads = Ads.objects.filter(products=self.products.all(),
                                 active=True).distinct()
        return ads

    @property
    def get_splitted_phone(self):
        parts = ['', '']
        index = self.phone.find(')')
        if index != -1:
            parts = self.phone[:index + 1], self.phone[index + 1:]
        return parts


class TaxType(models.Model):

    title = models.CharField(verbose_name=u"Название",
                             max_length=255)

    code = models.CharField(verbose_name=u"Код",
                            max_length=255)

    class Meta:

        verbose_name = u"Тип налогообложения"
        verbose_name_plural = u"Типы налогообложения"

    def __unicode__(self):
        return self.title


class ActivityCategory(models.Model):

    title = models.CharField(verbose_name=u"Название",
                             max_length=255)

    code = models.CharField(verbose_name=u"ОКВЭД",
                            max_length=255)

    class Meta:

        verbose_name = u"Вид деятельности"
        verbose_name_plural = u"Виды деятельности"

    def __unicode__(self):
        return self.title


class BatchType(models.Model):

    title = models.CharField(verbose_name=u"Название",
                             max_length=255)

    code = models.CharField(verbose_name=u"Код",
                            max_length=255)

    class Meta:

        verbose_name = u"Опт/Розница"
        verbose_name_plural = u"Опт/Розница"

    def __unicode__(self):
        return self.title
