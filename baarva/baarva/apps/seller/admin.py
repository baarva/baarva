# -*- coding: utf-8 -*-

from django import forms
from django.contrib import admin

from mptt.forms import TreeNodeChoiceField

from .models import Seller, TaxType, BatchType, ActivityCategory

from baarva.apps.address.models import Address


class SellerForm(forms.ModelForm):
    address = TreeNodeChoiceField(
        queryset=Address.objects.all(),
        label=u"Адрес")

    class Meta:
        model = Seller
        exclude = ()


@admin.register(Seller)
class SellerAdmin(admin.ModelAdmin):
    form = SellerForm


@admin.register(TaxType)
class TaxTypeAdmin(admin.ModelAdmin):
    pass


@admin.register(BatchType)
class BatchTypeAdmin(admin.ModelAdmin):
    pass


@admin.register(ActivityCategory)
class ActivityCategoryAdmin(admin.ModelAdmin):
    pass
