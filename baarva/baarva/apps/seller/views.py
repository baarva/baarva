# -*- coding: utf-8 -*-

from django.shortcuts import render
from django.views.generic.detail import DetailView
from django.views.generic import TemplateView

from baarva.apps.catalog.models import Product
from baarva.apps.catalog.views import FilterContextMixin
from baarva.apps.seller.models import TAX_TYPES, BATCH_TYPES

from .models import Seller


class RegisterGeneralView(TemplateView):
    template_name = 'seller/register-general.jhtml'

    def get_context_data(self, **kwargs):

        search_tax_type = self.request.session.get('tax_type', "all")
        search_batch_type = self.request.session.get('batch_type', "all")
        rating_product_list = Product.objects.filter(rating=True).distinct()
        popular_product_list = Product.objects.filter(
            popularity=True).distinct()

        context = {
            'search_tax_type': TAX_TYPES[search_tax_type],
            'search_batch_type': BATCH_TYPES[search_batch_type],
            'search_tax_type_id': search_tax_type,
            'search_batch_type_id': search_batch_type,
            'rating_product_list': rating_product_list,
            'popular_product_list': popular_product_list,
            'disable_filter': True,
        }

        return context


class RegisterJuridicalView(TemplateView):
    template_name = 'seller/register-jur.jhtml'

    def get_context_data(self, **kwargs):

        search_tax_type = self.request.session.get('tax_type', "all")
        search_batch_type = self.request.session.get('batch_type', "all")

        context = {
            'search_tax_type': TAX_TYPES[search_tax_type],
            'search_batch_type': BATCH_TYPES[search_batch_type],
            'search_tax_type_id': search_tax_type,
            'search_batch_type_id': search_batch_type,
            'disable_filter': True,
        }

        return context


class SellerView(DetailView, FilterContextMixin):

    model = Seller
    template_name = u"seller/about-company.jhtml"

    def get_context_data(self, **kwargs):

        context = super(SellerView, self).get_context_data(**kwargs)
        context.update(self.get_filter_context_data(self.request, **kwargs))

        search_tax_type = self.request.session.get('tax_type', "all")
        search_batch_type = self.request.session.get('batch_type', "all")

        rating_product_list = Product.objects.filter(rating=True).distinct()
        popular_product_list = Product.objects.filter(
            popularity=True).distinct()

        context.update({
            'search_tax_type': TAX_TYPES[search_tax_type],
            'search_batch_type': BATCH_TYPES[search_batch_type],
            'search_tax_type_id': search_tax_type,
            'search_batch_type_id': search_batch_type,
            'rating_product_list': rating_product_list,
            'popular_product_list': popular_product_list,
            'disable_filter': True
        })

        return context
