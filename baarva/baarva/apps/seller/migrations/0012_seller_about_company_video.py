# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import embed_video.fields


class Migration(migrations.Migration):

    dependencies = [
        ('seller', '0011_auto_20160218_1442'),
    ]

    operations = [
        migrations.AddField(
            model_name='seller',
            name='about_company_video',
            field=embed_video.fields.EmbedVideoField(null=True, verbose_name='\u041e \u043a\u043e\u043c\u043f\u0430\u043d\u0438\u0438 \u0432\u0438\u0434\u0435\u043e', blank=True),
        ),
    ]
