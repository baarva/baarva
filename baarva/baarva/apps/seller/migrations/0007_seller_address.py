# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('address', '0001_initial'),
        ('seller', '0006_auto_20160127_0243'),
    ]

    operations = [
        migrations.AddField(
            model_name='seller',
            name='address',
            field=models.ForeignKey(related_name='sellers', verbose_name='\u0410\u0434\u0440\u0435\u0441', blank=True, to='address.Address', null=True),
        ),
    ]
