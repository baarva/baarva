# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('seller', '0004_auto_20160126_1221'),
    ]

    operations = [
        migrations.AddField(
            model_name='seller',
            name='activity_category_list',
            field=models.ManyToManyField(related_name='activity_sellers', null=True, verbose_name='\u0412\u0438\u0434\u044b \u0434\u0435\u044f\u0442\u0435\u043b\u044c\u043d\u043e\u0441\u0442\u0438', to='seller.ActivityCategory', blank=True),
        ),
        migrations.AddField(
            model_name='seller',
            name='main_activity_category',
            field=models.ForeignKey(related_name='main_activity_sellers', verbose_name='\u041e\u0441\u043d\u043e\u0432\u043d\u043e\u0439 \u0432\u0438\u0434 \u0434\u0435\u044f\u0442\u0435\u043b\u044c\u043d\u043e\u0441\u0442\u0438', blank=True, to='seller.ActivityCategory', null=True),
        ),
    ]
