# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('seller', '0008_auto_20160127_0346'),
    ]

    operations = [
        migrations.AlterField(
            model_name='activitycategory',
            name='code',
            field=models.CharField(max_length=255, verbose_name='\u041e\u041a\u0412\u042d\u0414'),
        ),
    ]
