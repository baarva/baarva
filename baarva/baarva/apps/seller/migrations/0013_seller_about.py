# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('seller', '0012_seller_about_company_video'),
    ]

    operations = [
        migrations.AddField(
            model_name='seller',
            name='about',
            field=ckeditor.fields.RichTextField(null=True, verbose_name='\u041e \u043a\u043e\u043c\u043f\u0430\u043d\u0438\u0438', blank=True),
        ),
    ]
