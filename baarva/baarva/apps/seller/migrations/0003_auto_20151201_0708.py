# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('seller', '0002_auto_20151121_0919'),
    ]

    operations = [
        migrations.CreateModel(
            name='BatchType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('code', models.CharField(max_length=255, verbose_name='\u041a\u043e\u0434')),
            ],
            options={
                'verbose_name': '\u041e\u043f\u0442/\u0420\u043e\u0437\u043d\u0438\u0446\u0430',
                'verbose_name_plural': '\u041e\u043f\u0442/\u0420\u043e\u0437\u043d\u0438\u0446\u0430',
            },
        ),
        migrations.CreateModel(
            name='TaxType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('code', models.CharField(max_length=255, verbose_name='\u041a\u043e\u0434')),
            ],
            options={
                'verbose_name': '\u0422\u0438\u043f \u043d\u0430\u043b\u043e\u0433\u043e\u043e\u0431\u043b\u043e\u0436\u0435\u043d\u0438\u044f',
                'verbose_name_plural': '\u0422\u0438\u043f\u044b \u043d\u0430\u043b\u043e\u0433\u043e\u043e\u0431\u043b\u043e\u0436\u0435\u043d\u0438\u044f',
            },
        ),
        migrations.AddField(
            model_name='seller',
            name='batch_type',
            field=models.ManyToManyField(to='seller.BatchType', verbose_name='\u041e\u043f\u0442/\u0420\u043e\u0437\u043d\u0438\u0446\u0430'),
        ),
        migrations.AddField(
            model_name='seller',
            name='tax_type',
            field=models.ManyToManyField(to='seller.TaxType', verbose_name='\u041d\u0430\u043b\u043e\u0433\u043e\u043e\u0431\u043b\u043e\u0436\u0435\u043d\u0438\u0435'),
        ),
    ]
