# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('seller', '0003_auto_20151201_0708'),
    ]

    operations = [
        migrations.CreateModel(
            name='ActivityCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('code', models.CharField(max_length=255, verbose_name='\u041a\u043e\u0434')),
            ],
            options={
                'verbose_name': '\u0412\u0438\u0434 \u0434\u0435\u044f\u0442\u0435\u043b\u044c\u043d\u043e\u0441\u0442\u0438',
                'verbose_name_plural': '\u0412\u0438\u0434\u044b \u0434\u0435\u044f\u0442\u0435\u043b\u044c\u043d\u043e\u0441\u0442\u0438',
            },
        ),
        migrations.AddField(
            model_name='seller',
            name='owner_type',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='\u0412\u0438\u0434 \u0441\u043e\u0431\u0441\u0442\u0432\u0435\u043d\u043d\u043e\u0441\u0442\u0438', choices=[(b'ooo', '\u041e\u041e\u041e'), (b'zao', '\u0417\u0410\u041e'), (b'pao', '\u041f\u0410\u041e')]),
        ),
    ]
