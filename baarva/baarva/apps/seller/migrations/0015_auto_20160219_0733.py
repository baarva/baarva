# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('seller', '0014_auto_20160219_0519'),
    ]

    operations = [
        migrations.AddField(
            model_name='seller',
            name='buyer_rating',
            field=models.PositiveIntegerField(default=0, verbose_name='\u0420\u0435\u0442\u0438\u043d\u0433 \u043f\u043e\u043a\u0443\u043f\u043a\u0438'),
        ),
        migrations.AddField(
            model_name='seller',
            name='quality_rating',
            field=models.PositiveIntegerField(default=0, verbose_name='\u0420\u0435\u0442\u0438\u043d\u0433 \u043a\u0430\u0447\u0435\u0441\u0442\u0432\u0430'),
        ),
        migrations.AddField(
            model_name='seller',
            name='seller_rating',
            field=models.PositiveIntegerField(default=0, verbose_name='\u0420\u0435\u0442\u0438\u043d\u0433 \u043f\u0440\u043e\u0434\u0430\u0436\u0438'),
        ),
    ]
