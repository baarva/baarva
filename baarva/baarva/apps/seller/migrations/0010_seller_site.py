# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('seller', '0009_auto_20160208_1306'),
    ]

    operations = [
        migrations.AddField(
            model_name='seller',
            name='site',
            field=models.URLField(default='', verbose_name=b'\xd0\xa1\xd0\xb0\xd0\xb9\xd1\x82 \xd0\xba\xd0\xbe\xd0\xbc\xd0\xbf\xd0\xb0\xd0\xbd\xd0\xb8\xd0\xb8'),
        ),
    ]
