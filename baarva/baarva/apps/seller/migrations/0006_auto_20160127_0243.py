# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('seller', '0005_auto_20160126_1234'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='seller',
            name='address',
        ),
        migrations.AlterField(
            model_name='seller',
            name='activity_category_list',
            field=models.ManyToManyField(related_name='activity_sellers', verbose_name='\u0412\u0438\u0434\u044b \u0434\u0435\u044f\u0442\u0435\u043b\u044c\u043d\u043e\u0441\u0442\u0438', to='seller.ActivityCategory'),
        ),
        migrations.AlterField(
            model_name='seller',
            name='image',
            field=models.ImageField(upload_to=b'images', null=True, verbose_name='\u041b\u043e\u0433\u043e\u0442\u0438\u043f', blank=True),
        ),
    ]
