# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import embed_video.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='BaarvaEmbedVideo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u043d\u0430 \u0441\u0430\u0439\u0442\u0435')),
                ('video', embed_video.fields.EmbedVideoField(verbose_name='\u0421\u0441\u044b\u043b\u043a\u0430 \u043d\u0430 YouTube')),
                ('order', models.PositiveIntegerField(default=0, verbose_name='\u041f\u043e\u0440\u044f\u0434\u043e\u043a')),
            ],
            options={
                'ordering': ('-order',),
                'verbose_name': '\u041e \u0442\u043e\u0440\u0433\u043e\u0432\u043e\u0439 \u043f\u043b\u043e\u0449\u0430\u0434\u043a\u0435 \u0432\u0438\u0434\u0435\u043e',
                'verbose_name_plural': '\u041e \u0442\u043e\u0440\u0433\u043e\u0432\u043e\u0439 \u043f\u043b\u043e\u0449\u0430\u0434\u043a\u0435 \u0432\u0438\u0434\u0435\u043e',
            },
        ),
    ]
