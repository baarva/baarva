# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='baarvaembedvideo',
            name='popup_title',
            field=models.CharField(default='', max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u0432 \u0432\u0441\u043f\u043b\u044b\u0432\u0430\u0448\u043a\u0435'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='baarvaembedvideo',
            name='title',
            field=ckeditor.fields.RichTextField(verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u043d\u0430 \u0441\u0430\u0439\u0442\u0435'),
        ),
    ]
