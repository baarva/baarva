# -*- coding: utf-8 -*-

from django.db import models

from embed_video.backends import YoutubeBackend
from embed_video.fields import EmbedVideoField


class BaarvaEmbedVideo(models.Model):

    title = models.CharField(
        verbose_name=u"Название на сайте",
        max_length=255)

    popup_title = models.CharField(
        verbose_name=u"Название в всплывашке",
        max_length=255)

    video = EmbedVideoField(
        verbose_name=u"Ссылка на YouTube")

    order = models.PositiveIntegerField(
        verbose_name=u'Порядок',
        default=0,
        blank=False,
        null=False)

    class Meta:

        verbose_name = u'О торговой площадке видео'
        verbose_name_plural = u'О торговой площадке видео'
        ordering = ('order',)

    def __unicode__(self):
        return self.title

    def youtube_backend(self):
        if not self.video:
            return None
        backend = YoutubeBackend(self.video)
        return backend
