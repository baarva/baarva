# -*- coding: utf-8 -*-

from __future__ import absolute_import

from jinja2 import Environment

from .templatetags import thumbnail, margin, format_number


def environment(**options):
    env = Environment(**options)
    env.globals.update({
        'thumbnail': thumbnail,
        'margin': margin,
        'format_number': format_number
    })
    return env
