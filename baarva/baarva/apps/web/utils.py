# -*- coding: utf-8 -*-

from autoslug.settings import slugify as default_slugify


def custom_slugify(value):
    return default_slugify(value).replace('-', '_')
