# -*- coding: utf-8 -*-
import json

from django.http.response import HttpResponse

from django.views.decorators.csrf import requires_csrf_token
from django.views.defaults import page_not_found as default_page_not_found
from django.views.defaults import server_error as default_server_error
from django.views.generic import TemplateView

from baarva.apps.catalog.models import Category, Product
from baarva.apps.seller.models import TAX_TYPES, BATCH_TYPES

from baarva.apps.web.models import BaarvaEmbedVideo


class IndexView(TemplateView):

    template_name = "index.jhtml"

    def get_context_data(self, **kwargs):

        roots = Category.objects.get(id=1).get_children()
        search_tax_type = self.request.session.get('tax_type', "all")
        search_batch_type = self.request.session.get('batch_type', "all")
        rating_product_list = Product.objects.filter(rating=True).distinct()
        popular_product_list = Product.objects.filter(
            popularity=True).distinct()

        context = {
            'roots': roots,
            'search_tax_type': TAX_TYPES[search_tax_type],
            'search_batch_type': BATCH_TYPES[search_batch_type],
            'search_tax_type_id': search_tax_type,
            'search_batch_type_id': search_batch_type,
            'rating_product_list': rating_product_list,
            'popular_product_list': popular_product_list,
            'disable_filter': True,
        }

        return context


def save_user_params(request):

    if request.method == 'POST':
        response_data = {}

        tax_type = request.POST.get('tax_type', "default")
        batch_type = request.POST.get('batch_type', "all")

        session = request.session

        session['tax_type'] = tax_type
        session['batch_type'] = batch_type
        session.save()

        response_data['200'] = "ok"
        return HttpResponse(
            json.dumps(response_data),
            content_type="application/json"
        )

    return HttpResponse(
        json.dumps({
            "nothing to see": "this isn't happening",
        }),
        content_type="application/json"
    )


@requires_csrf_token
def page_not_found(request, template_name='404.jhtml'):
    return default_page_not_found(request, template_name)


@requires_csrf_token
def server_error(request, template_name='500.jhtml'):
    return default_server_error(request, template_name)


class DevelopView(TemplateView):

    template_name = "in-developing.jhtml"

    def get_context_data(self, **kwargs):

        search_tax_type = self.request.session.get('tax_type', "all")
        search_batch_type = self.request.session.get('batch_type', "all")
        rating_product_list = Product.objects.filter(rating=True).distinct()
        popular_product_list = Product.objects.filter(
            popularity=True).distinct()

        context = {
            'search_tax_type': TAX_TYPES[search_tax_type],
            'search_batch_type': BATCH_TYPES[search_batch_type],
            'search_tax_type_id': search_tax_type,
            'search_batch_type_id': search_batch_type,
            'rating_product_list': rating_product_list,
            'popular_product_list': popular_product_list,
            'disable_filter': True,
        }

        return context


class InformBaarvaView(TemplateView):

    template_name = "inform-baarva.jhtml"

    def get_context_data(self, **kwargs):

        search_tax_type = self.request.session.get('tax_type', "all")
        search_batch_type = self.request.session.get('batch_type', "all")
        rating_product_list = Product.objects.filter(rating=True).distinct()
        popular_product_list = Product.objects.filter(
            popularity=True).distinct()

        video_list = BaarvaEmbedVideo.objects.all()

        context = {
            'search_tax_type': TAX_TYPES[search_tax_type],
            'search_batch_type': BATCH_TYPES[search_batch_type],
            'search_tax_type_id': search_tax_type,
            'search_batch_type_id': search_batch_type,
            'rating_product_list': rating_product_list,
            'popular_product_list': popular_product_list,
            'disable_filter': True,
            'video_list': video_list
        }

        return context
