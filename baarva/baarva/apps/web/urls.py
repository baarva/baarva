# -*- coding: utf-8 -*-

from django.conf.urls import url
from .views import IndexView, DevelopView, save_user_params, InformBaarvaView


urlpatterns = [
    url(r'^$', IndexView.as_view(), name='index'),
    url(r'^user/params/save/$', save_user_params, name='user_params_save'),
    url(r'^develop/$', DevelopView.as_view(), name='develop'),
    url(r'inform-baarva/$', InformBaarvaView.as_view(), name='inform-baarva')
]
