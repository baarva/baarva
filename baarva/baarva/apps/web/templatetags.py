# -*- coding: utf-8 -*-

import decimal

from sorl.thumbnail import default
from sorl.thumbnail.conf import settings as sorl_settings
from sorl.thumbnail.images import ImageFile, DummyImageFile
from sorl.thumbnail.shortcuts import get_thumbnail
from sorl.thumbnail.parsers import parse_geometry


def thumbnail(file_, geometry_string, **options):
    try:
        im = get_thumbnail(file_, geometry_string, **options)
    except IOError:
        im = None
    return im


def margin(file_, geometry_string):
    """
    Returns the calculated margin for an image and geometry
    """
    if not file_:
        return 'auto'
    elif (sorl_settings.THUMBNAIL_DUMMY or isinstance(file_, DummyImageFile)):
        return 'auto'

    margin = [0, 0, 0, 0]

    image_file = default.kvstore.get_or_set(ImageFile(file_))

    x, y = parse_geometry(geometry_string, image_file.ratio)
    ex = x - image_file.x
    margin[3] = ex / 2
    margin[1] = ex / 2

    if ex % 2:
        margin[1] += 1

    ey = y - image_file.y
    margin[0] = ey / 2
    margin[2] = ey / 2

    if ey % 2:
        margin[2] += 1

    return ' '.join(['%dpx' % n for n in margin])


def format_number(num):
    try:
        dec = decimal.Decimal(num)
    except:
        return 'bad'
    tup = dec.as_tuple()
    delta = len(tup.digits) + tup.exponent
    digits = ''.join(str(d) for d in tup.digits)
    if delta <= 0:
        zeros = abs(tup.exponent) - len(tup.digits)
        val = '0.' + ('0'*zeros) + digits
    else:
        val = digits[:delta] + ('0'*tup.exponent) + '.' + digits[delta:]
    val = val.rstrip('0')
    if val[-1] == '.':
        val = val[:-1]
    if tup.sign:
        return '-' + val
    return val