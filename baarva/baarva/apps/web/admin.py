from django.contrib import admin

from .models import BaarvaEmbedVideo


@admin.register(BaarvaEmbedVideo)
class BaarvaEmbedVideoAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'order')
    list_editable = ('order',)
