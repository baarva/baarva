# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog_armatura', '0017_auto_20160310_1353'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='SteelLenghtType',
            new_name='SteelLengthType',
        ),
    ]
