# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog_armatura', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='steellength',
            options={'verbose_name': '\u0414\u043b\u0438\u043d\u0430 \u0441\u0442\u0430\u043b\u0438', 'verbose_name_plural': '\u0414\u043b\u0438\u043d\u044b \u0441\u0442\u0430\u043b\u0438'},
        ),
        migrations.AlterModelOptions(
            name='steelmark',
            options={'verbose_name': '\u041c\u0430\u0440\u043a\u0430 \u0441\u0442\u0430\u043b\u0438', 'verbose_name_plural': '\u041c\u0430\u0440\u043a\u0438 \u0441\u0442\u0430\u043b\u0438'},
        ),
    ]
