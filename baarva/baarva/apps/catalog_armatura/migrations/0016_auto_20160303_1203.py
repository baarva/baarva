# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog_armatura', '0015_auto_20160302_1229'),
    ]

    operations = [
        migrations.RenameField(
            model_name='steelweight',
            old_name='weigth',
            new_name='value',
        ),
    ]
