# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog_armatura', '0012_auto_20160301_1248'),
    ]

    operations = [
        migrations.AlterField(
            model_name='steellength',
            name='value',
            field=models.FloatField(max_length=255, null=True, verbose_name='\u0417\u043d\u0430\u0447\u0435\u043d\u0438\u0435', blank=True),
        ),
    ]
