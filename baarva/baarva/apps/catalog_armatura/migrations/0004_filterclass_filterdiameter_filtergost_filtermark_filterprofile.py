# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0011_auto_20160123_0931'),
        ('catalog_armatura', '0003_steellength_title'),
    ]

    operations = [
        migrations.CreateModel(
            name='FilterClass',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('gost', models.ForeignKey(verbose_name=b'\xd0\x93\xd0\xbe\xd1\x81\xd1\x82', to='catalog.GOST')),
                ('steel_class', models.ForeignKey(verbose_name=b'\xd0\x9a\xd0\xbb\xd0\xb0\xd1\x81\xd1\x81 \xd1\x81\xd1\x82\xd0\xb0\xd0\xbb\xd0\xb8', to='catalog_armatura.SteelClass')),
            ],
            options={
                'verbose_name': '\u0414\u043b\u0438\u043d\u0430 \u0441\u0442\u0430\u043b\u0438 \u0434\u043b\u044f \u0444\u0438\u043b\u044c\u0442\u0440\u0430',
                'verbose_name_plural': '\u0414\u043b\u0438\u043d\u044b \u0441\u0442\u0430\u043b\u0438 \u0434\u043b\u044f \u0444\u0438\u043b\u044c\u0442\u0440\u0430',
            },
        ),
        migrations.CreateModel(
            name='FilterDiameter',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('steel_class', models.ForeignKey(verbose_name=b'\xd0\x9a\xd0\xbb\xd0\xb0\xd1\x81\xd1\x81 \xd1\x81\xd1\x82\xd0\xb0\xd0\xbb\xd0\xb8', to='catalog_armatura.SteelClass')),
                ('steel_diameter', models.ForeignKey(verbose_name=b'\xd0\x94\xd0\xb8\xd0\xb0\xd0\xbc\xd0\xb5\xd1\x82\xd1\x80', to='catalog_armatura.SteelDiameter')),
            ],
            options={
                'verbose_name': '\u0414\u0438\u0430\u043c\u0435\u0442\u0440\u044b \u0441\u0442\u0430\u043b\u0438 \u0434\u043b\u044f \u0444\u0438\u043b\u044c\u0442\u0440\u0430',
                'verbose_name_plural': '\u0414\u0438\u0430\u043c\u0435\u0442\u0440\u044b \u0441\u0442\u0430\u043b\u0438 \u0434\u043b\u044f \u0444\u0438\u043b\u044c\u0442\u0440\u0430',
            },
        ),
        migrations.CreateModel(
            name='FilterGOST',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('gost', models.ForeignKey(verbose_name=b'\xd0\x93\xd0\xbe\xd1\x81\xd1\x82', to='catalog.GOST')),
            ],
            options={
                'verbose_name': '\u0413\u043e\u0441\u0442 \u0434\u043b\u044f \u0444\u0438\u043b\u044c\u0442\u0440\u0430',
                'verbose_name_plural': '\u0413\u043e\u0441\u0442\u044b \u0434\u043b\u044f \u0444\u0438\u043b\u044c\u0442\u0440\u0430',
            },
        ),
        migrations.CreateModel(
            name='FilterMark',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('steel_class', models.ForeignKey(verbose_name=b'\xd0\x9a\xd0\xbb\xd0\xb0\xd1\x81\xd1\x81 \xd1\x81\xd1\x82\xd0\xb0\xd0\xbb\xd0\xb8', to='catalog_armatura.SteelClass')),
                ('steel_mark', models.ForeignKey(verbose_name=b'\xd0\x9f\xd1\x80\xd0\xbe\xd1\x84\xd0\xb8\xd0\xbb\xd1\x8c', to='catalog_armatura.SteelMark')),
            ],
            options={
                'verbose_name': '\u041c\u0430\u0440\u043a\u0430 \u0441\u0442\u0430\u043b\u0438 \u0434\u043b\u044f \u0444\u0438\u043b\u044c\u0442\u0440\u0430',
                'verbose_name_plural': '\u041c\u0430\u0440\u043a\u0438 \u0441\u0442\u0430\u043b\u0438 \u0434\u043b\u044f \u0444\u0438\u043b\u044c\u0442\u0440\u0430',
            },
        ),
        migrations.CreateModel(
            name='FilterProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('steel_class', models.ForeignKey(verbose_name=b'\xd0\x9a\xd0\xbb\xd0\xb0\xd1\x81\xd1\x81 \xd1\x81\xd1\x82\xd0\xb0\xd0\xbb\xd0\xb8', to='catalog_armatura.SteelClass')),
                ('steel_profile', models.ForeignKey(verbose_name=b'\xd0\x9f\xd1\x80\xd0\xbe\xd1\x84\xd0\xb8\xd0\xbb\xd1\x8c', to='catalog_armatura.SteelProfile')),
            ],
            options={
                'verbose_name': '\u041f\u0440\u043e\u0444\u0438\u043b\u044c \u0434\u043b\u044f \u0444\u0438\u043b\u044c\u0442\u0440\u0430',
                'verbose_name_plural': '\u041f\u0440\u043e\u0444\u0438\u043b\u0438 \u0434\u043b\u044f \u0444\u0438\u043b\u044c\u0442\u0440\u0430',
            },
        ),
    ]
