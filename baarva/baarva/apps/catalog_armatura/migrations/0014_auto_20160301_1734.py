# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0026_remove_product_weigth'),
        ('catalog_armatura', '0013_auto_20160301_1259'),
    ]

    operations = [
        migrations.CreateModel(
            name='FilterSteelLengthType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('steel_class', models.ForeignKey(verbose_name=b'\xd0\x9a\xd0\xbb\xd0\xb0\xd1\x81\xd1\x81 \xd1\x81\xd1\x82\xd0\xb0\xd0\xbb\xd0\xb8', to='catalog_armatura.SteelClass')),
                ('steel_diameter', models.ForeignKey(verbose_name=b'\xd0\x94\xd0\xb8\xd0\xb0\xd0\xbc\xd0\xb5\xd1\x82\xd1\x80', to='catalog_armatura.SteelDiameter')),
                ('steel_lenght_type', models.ForeignKey(verbose_name=b'\xd0\xa2\xd0\xb8\xd0\xbf \xd0\xb4\xd0\xbb\xd0\xb8\xd0\xbd\xd1\x8b \xd1\x81\xd1\x82\xd0\xb0\xd0\xbb\xd0\xb8', to='catalog_armatura.SteelLenghtType')),
                ('steel_profile', models.ForeignKey(verbose_name=b'\xd0\x9f\xd1\x80\xd0\xbe\xd1\x84\xd0\xb8\xd0\xbb\xd1\x8c', to='catalog_armatura.SteelProfile')),
            ],
            options={
                'verbose_name': '\u0422\u0438\u043f \u0434\u043b\u0438\u043d\u044b \u0441\u0442\u0430\u043b\u0438 \u0434\u043b\u044f \u0444\u0438\u043b\u044c\u0442\u0440\u0430',
                'verbose_name_plural': '\u0422\u0438\u043f\u044b \u0434\u043b\u0438\u043d\u044b \u0441\u0442\u0430\u043b\u0438 \u0444\u0438\u043b\u044c\u0442\u0440\u0430',
            },
        ),
        migrations.CreateModel(
            name='SteelWeight',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('weigth', models.DecimalField(default=0, verbose_name='\u0412\u0435\u0441 1\u043c(\u043a\u0433)', max_digits=14, decimal_places=6)),
            ],
            options={
                'verbose_name': '\u0412\u0435\u0441 \u0441\u0442\u0430\u043b\u0438',
                'verbose_name_plural': '\u0412\u0435\u0441\u0430 \u0421\u0442\u0430\u043b\u0438',
            },
        ),
        migrations.CreateModel(
            name='SteelWeightAttribute',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('attribute', models.ForeignKey(verbose_name='\u0410\u0442\u0440\u0438\u0431\u0443\u0442', to='catalog.Attribute')),
                ('weight', models.ForeignKey(related_name='attributes', verbose_name='\u0412\u0435\u0441', to='catalog_armatura.SteelWeight')),
            ],
            options={
                'verbose_name': '\u0417\u043d\u0430\u0447\u0435\u043d\u0438\u0435 \u0430\u0442\u0440\u0438\u0431\u0443\u0442\u0430',
                'verbose_name_plural': '\u0417\u043d\u0430\u0447\u0435\u043d\u0438\u044f \u0430\u0442\u0440\u0438\u0431\u0443\u0442\u043e\u0432',
            },
        ),
        migrations.AlterUniqueTogether(
            name='filtersteellenghttype',
            unique_together=set([]),
        ),
        migrations.RemoveField(
            model_name='filtersteellenghttype',
            name='steel_class',
        ),
        migrations.RemoveField(
            model_name='filtersteellenghttype',
            name='steel_diameter',
        ),
        migrations.RemoveField(
            model_name='filtersteellenghttype',
            name='steel_lenght_type',
        ),
        migrations.RemoveField(
            model_name='filtersteellenghttype',
            name='steel_profile',
        ),
        migrations.DeleteModel(
            name='FilterSteelLenghtType',
        ),
        migrations.AlterUniqueTogether(
            name='steelweightattribute',
            unique_together=set([('weight', 'attribute')]),
        ),
        migrations.AlterUniqueTogether(
            name='filtersteellengthtype',
            unique_together=set([('steel_diameter', 'steel_class', 'steel_profile')]),
        ),
    ]
