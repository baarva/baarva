# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog_armatura', '0019_auto_20160315_0821'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='filtersteellengthtype',
            unique_together=set([('steel_diameter', 'steel_class', 'steel_profile', 'steel_length_type')]),
        ),
    ]
