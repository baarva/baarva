# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog_armatura', '0018_auto_20160310_1354'),
    ]

    operations = [
        migrations.RenameField(
            model_name='filtersteellengthtype',
            old_name='steel_lenght_type',
            new_name='steel_length_type',
        ),
    ]
