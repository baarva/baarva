# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog_armatura', '0016_auto_20160303_1203'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='steellength',
            name='steel_lenght_type',
        ),
        migrations.AddField(
            model_name='steellength',
            name='steel_length_type',
            field=models.ForeignKey(verbose_name=b'\xd0\xa2\xd0\xb8\xd0\xbf \xd0\xb4\xd0\xbb\xd0\xb8\xd0\xbd\xd1\x8b \xd1\x81\xd1\x82\xd0\xb0\xd0\xbb\xd0\xb8', blank=True, to='catalog_armatura.SteelLenghtType', null=True),
        ),
    ]
