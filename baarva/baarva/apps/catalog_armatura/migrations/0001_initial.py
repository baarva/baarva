# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('common_data', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='SteelClass',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
            ],
            options={
                'verbose_name': '\u041a\u043b\u0430\u0441\u0441 \u0441\u0442\u0430\u043b\u0438',
                'verbose_name_plural': '\u041a\u043b\u0430\u0441\u0441\u044b \u0441\u0442\u0430\u043b\u0438',
            },
        ),
        migrations.CreateModel(
            name='SteelDiameter',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.FloatField(max_length=255, verbose_name='\u0417\u043d\u0430\u0447\u0435\u043d\u0438\u0435')),
                ('measure_unit', models.ForeignKey(verbose_name='\u0415\u0434\u0438\u043d\u0438\u0446\u0430 \u0438\u043c\u0435\u0440\u0435\u043d\u0438\u044f', to='common_data.MeasureUnit')),
            ],
            options={
                'verbose_name': '\u0414\u0438\u0430\u043c\u0435\u0442\u0440 \u0441\u0442\u0430\u043b\u0438',
                'verbose_name_plural': '\u0414\u0438\u0430\u043c\u0435\u0442\u0440\u044b \u0441\u0442\u0430\u043b\u0438',
            },
        ),
        migrations.CreateModel(
            name='SteelLength',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.FloatField(max_length=255, verbose_name='\u0417\u043d\u0430\u0447\u0435\u043d\u0438\u0435')),
                ('measure_unit', models.ForeignKey(verbose_name='\u0415\u0434\u0438\u043d\u0438\u0446\u0430 \u0438\u043c\u0435\u0440\u0435\u043d\u0438\u044f', to='common_data.MeasureUnit')),
            ],
            options={
                'verbose_name': '\u0414\u0438\u0430\u043c\u0435\u0442\u0440 \u0441\u0442\u0430\u043b\u0438',
                'verbose_name_plural': '\u0414\u0438\u0430\u043c\u0435\u0442\u0440\u044b \u0441\u0442\u0430\u043b\u0438',
            },
        ),
        migrations.CreateModel(
            name='SteelMark',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
            ],
            options={
                'verbose_name': '\u041a\u043b\u0430\u0441\u0441 \u0441\u0442\u0430\u043b\u0438',
                'verbose_name_plural': '\u041a\u043b\u0430\u0441\u0441\u044b \u0441\u0442\u0430\u043b\u0438',
            },
        ),
        migrations.CreateModel(
            name='SteelProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
            ],
            options={
                'verbose_name': '\u041f\u0440\u043e\u0444\u0438\u043b\u044c \u0441\u0442\u0430\u043b\u0438',
                'verbose_name_plural': '\u041f\u0440\u043e\u0444\u0438\u043b\u0438 \u0441\u0442\u0430\u043b\u0438',
            },
        ),
    ]
