# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog_armatura', '0007_auto_20160124_0928'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='filterprofile',
            options={'verbose_name': '\u041f\u0440\u043e\u0444\u0438\u043b\u044c \u0441\u0442\u0430\u043b\u0438 \u0434\u043b\u044f \u0444\u0438\u043b\u044c\u0442\u0440\u0430', 'verbose_name_plural': '\u041f\u0440\u043e\u0444\u0438\u043b\u0438 \u0441\u0442\u0430\u043b\u0438 \u0434\u043b\u044f \u0444\u0438\u043b\u044c\u0442\u0440\u0430'},
        ),
    ]
