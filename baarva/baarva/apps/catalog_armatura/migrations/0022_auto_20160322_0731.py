# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog_armatura', '0021_steeldiameter_weight'),
    ]

    operations = [
        migrations.AddField(
            model_name='steellength',
            name='value_from',
            field=models.FloatField(max_length=255, null=True, verbose_name='\u0417\u043d\u0430\u0447\u0435\u043d\u0438\u0435 \u043e\u0442', blank=True),
        ),
        migrations.AddField(
            model_name='steellength',
            name='value_to',
            field=models.FloatField(max_length=255, null=True, verbose_name='\u0417\u043d\u0430\u0447\u0435\u043d\u0438\u0435 \u0434\u043e', blank=True),
        ),
    ]
