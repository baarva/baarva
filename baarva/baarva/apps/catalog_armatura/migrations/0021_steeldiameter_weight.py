# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog_armatura', '0020_auto_20160316_1033'),
    ]

    operations = [
        migrations.AddField(
            model_name='steeldiameter',
            name='weight',
            field=models.DecimalField(default=0, verbose_name='\u0412\u0435\u0441 1\u043c(\u043a\u0433)', max_digits=14, decimal_places=6),
        ),
    ]
