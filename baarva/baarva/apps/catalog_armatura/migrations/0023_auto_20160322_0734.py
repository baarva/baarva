# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog_armatura', '0022_auto_20160322_0731'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='steellength',
            options={'ordering': ('steel_length_type', 'value_from'), 'verbose_name': '\u0414\u043b\u0438\u043d\u0430 \u0441\u0442\u0430\u043b\u0438', 'verbose_name_plural': '\u0414\u043b\u0438\u043d\u044b \u0441\u0442\u0430\u043b\u0438'},
        ),
        migrations.AddField(
            model_name='steellength',
            name='step',
            field=models.FloatField(max_length=255, null=True, verbose_name='\u0428\u0430\u0433', blank=True),
        ),
    ]
