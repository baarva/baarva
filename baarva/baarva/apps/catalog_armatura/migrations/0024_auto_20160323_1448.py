# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog_armatura', '0023_auto_20160322_0734'),
    ]

    operations = [
        migrations.AlterField(
            model_name='steellength',
            name='value_from',
            field=models.FloatField(db_index=True, max_length=255, null=True, verbose_name='\u0417\u043d\u0430\u0447\u0435\u043d\u0438\u0435 \u043e\u0442', blank=True),
        ),
        migrations.AlterField(
            model_name='steellength',
            name='value_to',
            field=models.FloatField(db_index=True, max_length=255, null=True, verbose_name='\u0417\u043d\u0430\u0447\u0435\u043d\u0438\u0435 \u0434\u043e', blank=True),
        ),
        migrations.AlterField(
            model_name='steellengthtype',
            name='title',
            field=models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435', db_index=True),
        ),
        migrations.AlterField(
            model_name='steelweight',
            name='value',
            field=models.DecimalField(default=0, verbose_name='\u0412\u0435\u0441 1\u043c(\u043a\u0433)', max_digits=14, decimal_places=6, db_index=True),
        ),
    ]
