# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog_armatura', '0004_filterclass_filterdiameter_filtergost_filtermark_filterprofile'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='filterclass',
            options={'verbose_name': '\u041a\u043b\u0430\u0441\u0441\u044b \u0441\u0442\u0430\u043b\u0438 \u0434\u043b\u044f \u0444\u0438\u043b\u044c\u0442\u0440\u0430', 'verbose_name_plural': '\u041a\u043b\u0430\u0441\u0441\u044b \u0441\u0442\u0430\u043b\u0438 \u0434\u043b\u044f \u0444\u0438\u043b\u044c\u0442\u0440\u0430'},
        ),
        migrations.AlterField(
            model_name='filtergost',
            name='gost',
            field=models.OneToOneField(verbose_name=b'\xd0\x93\xd0\xbe\xd1\x81\xd1\x82', to='catalog.GOST'),
        ),
        migrations.AlterField(
            model_name='filtermark',
            name='steel_mark',
            field=models.ForeignKey(verbose_name=b'\xd0\x9c\xd0\xb0\xd1\x80\xd0\xba\xd0\xb0 \xd1\x81\xd1\x82\xd0\xb0\xd0\xbb\xd0\xb8', to='catalog_armatura.SteelMark'),
        ),
        migrations.AlterUniqueTogether(
            name='filterclass',
            unique_together=set([('gost', 'steel_class')]),
        ),
        migrations.AlterUniqueTogether(
            name='filterdiameter',
            unique_together=set([('steel_diameter', 'steel_class')]),
        ),
        migrations.AlterUniqueTogether(
            name='filtermark',
            unique_together=set([('steel_mark', 'steel_class')]),
        ),
        migrations.AlterUniqueTogether(
            name='filterprofile',
            unique_together=set([('steel_profile', 'steel_class')]),
        ),
    ]
