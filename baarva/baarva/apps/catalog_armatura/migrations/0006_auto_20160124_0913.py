# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0012_auto_20160124_0913'),
        ('catalog_armatura', '0005_auto_20160123_1018'),
    ]

    operations = [
        migrations.CreateModel(
            name='Filter',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('category', models.ForeignKey(verbose_name=b'\xd0\x9a\xd0\xb0\xd1\x82\xd0\xb5\xd0\xb3\xd0\xbe\xd1\x80\xd0\xb8\xd1\x8f \xd1\x84\xd0\xb8\xd0\xbb\xd1\x8c\xd1\x82\xd1\x80\xd0\xb0', to='catalog.Category')),
            ],
            options={
                'verbose_name': '\u0424\u0438\u043b\u044c\u0442\u0440',
                'verbose_name_plural': '\u0424\u0438\u043b\u044c\u0442\u0440\u044b',
            },
        ),
        migrations.AddField(
            model_name='filtergost',
            name='filter',
            field=models.ForeignKey(verbose_name=b'\xd0\xa4\xd0\xb8\xd0\xbb\xd1\x8c\xd1\x82\xd1\x80', blank=True, to='catalog_armatura.Filter', null=True),
        ),
    ]
