# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog_armatura', '0010_auto_20160301_1244'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='filtersteellenghttype',
            name='title',
        ),
    ]
