# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog_armatura', '0024_auto_20160323_1448'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='steellength',
            name='step',
        ),
        migrations.RemoveField(
            model_name='steellength',
            name='value',
        ),
    ]
