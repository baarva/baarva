# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog_armatura', '0014_auto_20160301_1734'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='steelweightattribute',
            unique_together=set([]),
        ),
        migrations.RemoveField(
            model_name='steelweightattribute',
            name='attribute',
        ),
        migrations.RemoveField(
            model_name='steelweightattribute',
            name='weight',
        ),
        migrations.AddField(
            model_name='steelweight',
            name='title',
            field=models.CharField(default=b'', max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435'),
        ),
        migrations.DeleteModel(
            name='SteelWeightAttribute',
        ),
    ]
