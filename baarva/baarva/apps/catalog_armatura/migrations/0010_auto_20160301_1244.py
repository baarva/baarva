# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog_armatura', '0009_auto_20160223_0544'),
    ]

    operations = [
        migrations.CreateModel(
            name='FilterSteelLenghtType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('steel_class', models.ForeignKey(verbose_name=b'\xd0\x9a\xd0\xbb\xd0\xb0\xd1\x81\xd1\x81 \xd1\x81\xd1\x82\xd0\xb0\xd0\xbb\xd0\xb8', to='catalog_armatura.SteelClass')),
                ('steel_diameter', models.ForeignKey(verbose_name=b'\xd0\x94\xd0\xb8\xd0\xb0\xd0\xbc\xd0\xb5\xd1\x82\xd1\x80', to='catalog_armatura.SteelDiameter')),
            ],
            options={
                'verbose_name': '\u0422\u0438\u043f \u0434\u043b\u0438\u043d\u044b \u0441\u0442\u0430\u043b\u0438 \u0434\u043b\u044f \u0444\u0438\u043b\u044c\u0442\u0440\u0430',
                'verbose_name_plural': '\u0422\u0438\u043f\u044b \u0434\u043b\u0438\u043d\u044b \u0441\u0442\u0430\u043b\u0438 \u0444\u0438\u043b\u044c\u0442\u0440\u0430',
            },
        ),
        migrations.CreateModel(
            name='SteelLenghtType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
            ],
            options={
                'verbose_name': '\u0422\u0438\u043f \u0434\u043b\u0438\u043d\u044b \u0441\u0442\u0430\u043b\u0438',
                'verbose_name_plural': '\u0422\u0438\u043f\u044b \u0434\u043b\u0438\u043d\u044b \u0441\u0442\u0430\u043b\u0438',
            },
        ),
        migrations.RemoveField(
            model_name='steellength',
            name='title',
        ),
        migrations.AddField(
            model_name='filterdiameter',
            name='steel_profile',
            field=models.ForeignKey(verbose_name=b'\xd0\x9f\xd1\x80\xd0\xbe\xd1\x84\xd0\xb8\xd0\xbb\xd1\x8c', blank=True, to='catalog_armatura.SteelProfile', null=True),
        ),
        migrations.AddField(
            model_name='filtersteellenghttype',
            name='steel_lenght_type',
            field=models.ForeignKey(verbose_name=b'\xd0\xa2\xd0\xb8\xd0\xbf \xd0\xb4\xd0\xbb\xd0\xb8\xd0\xbd\xd1\x8b \xd1\x81\xd1\x82\xd0\xb0\xd0\xbb\xd0\xb8', to='catalog_armatura.SteelLenghtType'),
        ),
        migrations.AddField(
            model_name='filtersteellenghttype',
            name='steel_profile',
            field=models.ForeignKey(verbose_name=b'\xd0\x9f\xd1\x80\xd0\xbe\xd1\x84\xd0\xb8\xd0\xbb\xd1\x8c', to='catalog_armatura.SteelProfile'),
        ),
        migrations.AddField(
            model_name='steellength',
            name='steel_lenght_type',
            field=models.ForeignKey(verbose_name=b'\xd0\xa2\xd0\xb8\xd0\xbf \xd0\xb4\xd0\xbb\xd0\xb8\xd0\xbd\xd1\x8b \xd1\x81\xd1\x82\xd0\xb0\xd0\xbb\xd0\xb8', blank=True, to='catalog_armatura.SteelLenghtType', null=True),
        ),
        migrations.AlterUniqueTogether(
            name='filtersteellenghttype',
            unique_together=set([('steel_diameter', 'steel_class', 'steel_profile')]),
        ),
    ]
