# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('common_data', '0002_measureunit_symbol'),
        ('catalog_armatura', '0011_remove_filtersteellenghttype_title'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='steellength',
            name='measure_unit',
        ),
        migrations.AddField(
            model_name='steellenghttype',
            name='measure_unit',
            field=models.ForeignKey(verbose_name='\u0415\u0434\u0438\u043d\u0438\u0446\u0430 \u0438\u043c\u0435\u0440\u0435\u043d\u0438\u044f', blank=True, to='common_data.MeasureUnit', null=True),
        ),
    ]
