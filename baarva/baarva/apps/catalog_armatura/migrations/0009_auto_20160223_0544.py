# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog_armatura', '0008_auto_20160208_1306'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='filter',
            name='category',
        ),
        migrations.RemoveField(
            model_name='filtergost',
            name='filter',
        ),
        migrations.RemoveField(
            model_name='filtergost',
            name='gost',
        ),
        migrations.DeleteModel(
            name='Filter',
        ),
        migrations.DeleteModel(
            name='FilterGOST',
        ),
    ]
