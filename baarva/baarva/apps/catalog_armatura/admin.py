# -*- coding: utf-8 -*-

from django.contrib import admin


from .models import SteelClass, SteelDiameter, SteelLength, \
    SteelMark, SteelProfile, FilterClass, FilterDiameter, \
    FilterMark, FilterProfile, SteelLengthType, FilterSteelLengthType


@admin.register(SteelClass)
class SteelClassAdmin(admin.ModelAdmin):
    pass


@admin.register(SteelLength)
class SteelLengthAdmin(admin.ModelAdmin):
    pass


@admin.register(SteelDiameter)
class SteelDiameterAdmin(admin.ModelAdmin):
    pass


@admin.register(SteelMark)
class SteelMarkAdmin(admin.ModelAdmin):
    pass


@admin.register(SteelProfile)
class SteelProfileAdmin(admin.ModelAdmin):
    pass


@admin.register(SteelLengthType)
class SteelLenghtTypeAdmin(admin.ModelAdmin):
    pass


@admin.register(FilterClass)
class FilterClassAdmin(admin.ModelAdmin):
    pass


@admin.register(FilterDiameter)
class FilterDiameterAdmin(admin.ModelAdmin):
    pass


@admin.register(FilterMark)
class FilterMark(admin.ModelAdmin):
    pass


@admin.register(FilterProfile)
class FilterProfileAdmin(admin.ModelAdmin):
    pass


@admin.register(FilterSteelLengthType)
class FilterSteelLengthTypeAdmin(admin.ModelAdmin):
    pass


