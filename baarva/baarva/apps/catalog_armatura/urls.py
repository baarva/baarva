# -*- coding: utf-8 -*-

from django.conf.urls import url
from .views import filter_gost, filter_steel_class, \
    armatura_form_submit, filter_steel_length, more_met, \
    calc_update, calc_price


urlpatterns = [
    url(r'gost/$',
        filter_gost,
        name='filter_gost'),

    url(r'steel/$',
        filter_steel_class,
        name='filter_steel_class'),

    url(r'form/$',
        armatura_form_submit,
        name='filter_armatura_catalog',
        ),

    url(r'length/$',
        filter_steel_length,
        name='filtel_armatura_catalog',
        ),

    url(r'more_met/(?P<product_id>[0-9]+)/$',
        more_met,
        name='more_met',
        ),

    url(r'calc/$',
        calc_update,
        name='calc_update',
        ),

    url(r'calc_price/$',
        calc_price,
        name='calc_price',
        ),
]
