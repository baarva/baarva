# -*- coding: utf-8 -*-
from django.db import models


class SteelClass(models.Model):

    title = models.CharField(
        verbose_name=u"Название",
        max_length=255)

    class Meta:
        verbose_name = u"Класс стали"
        verbose_name_plural = u"Классы стали"

    def __unicode__(self):
        return self.title


class SteelMark(models.Model):

    title = models.CharField(
        verbose_name=u"Название",
        max_length=255)

    class Meta:
        verbose_name = u"Марка стали"
        verbose_name_plural = u"Марки стали"

    def __unicode__(self):
        return self.title


class SteelProfile(models.Model):

    title = models.CharField(
        verbose_name=u"Название",
        max_length=255)

    class Meta:
        verbose_name = u"Профиль стали"
        verbose_name_plural = u"Профили стали"

    def __unicode__(self):
        return self.title


class SteelDiameter(models.Model):

    measure_unit = models.ForeignKey(
        'common_data.MeasureUnit',
        verbose_name=u"Единица имерения")

    value = models.FloatField(
        verbose_name=u"Значение",
        max_length=255)

    weight = models.DecimalField(
        verbose_name=u'Вес 1м(кг)',
        decimal_places=6,
        max_digits=14,
        default=0)

    class Meta:
        verbose_name = u"Диаметр стали"
        verbose_name_plural = u"Диаметры стали"

    def __unicode__(self):
        return unicode(self.value) + " " + unicode(self.measure_unit.symbol)


class SteelLengthType(models.Model):

    title = models.CharField(
        verbose_name=u"Название",
        max_length=255,
        db_index=True)

    measure_unit = models.ForeignKey(
        'common_data.MeasureUnit',
        verbose_name=u"Единица имерения",
        blank=True,
        null=True)

    class Meta:
        verbose_name = u"Тип длины стали"
        verbose_name_plural = u"Типы длины стали"

    def __unicode__(self):
        measure_unit = self.measure_unit or ""
        return "%s %s" % (self.title, unicode(measure_unit))


class SteelLength(models.Model):

    steel_length_type = models.ForeignKey(
        'catalog_armatura.SteelLengthType',
        verbose_name='Тип длины стали',
        blank=True,
        null=True)

    value_from = models.FloatField(
        verbose_name=u"Значение от",
        max_length=255,
        blank=True,
        null=True,
        db_index=True)

    value_to = models.FloatField(
        verbose_name=u"Значение до",
        max_length=255,
        blank=True,
        null=True,
        db_index=True)

    class Meta:
        verbose_name = u"Длина стали"
        verbose_name_plural = u"Длины стали"
        ordering = ('steel_length_type', 'value_from')

    def __unicode__(self):
        return u"%s от %s до %s" % (
            unicode(self.steel_length_type),
            unicode(self.value_from),
            unicode(self.value_to),
        )


class SteelWeight(models.Model):

    title = models.CharField(
        verbose_name=u"Название",
        max_length=255,
        default=u"")

    value = models.DecimalField(
        verbose_name=u'Вес 1м(кг)',
        decimal_places=6,
        max_digits=14,
        default=0,
        db_index=True)

    class Meta:
        verbose_name = u"Вес стали"
        verbose_name_plural = u"Веса Стали"

    def __unicode__(self):
        return unicode(self.title)


class FilterSteelLengthType(models.Model):

    steel_class = models.ForeignKey(
        'catalog_armatura.SteelClass',
        verbose_name='Класс стали')

    steel_profile = models.ForeignKey(
        'catalog_armatura.SteelProfile',
        verbose_name='Профиль')

    steel_diameter = models.ForeignKey(
        'catalog_armatura.SteelDiameter',
        verbose_name='Диаметр')

    steel_length_type = models.ForeignKey(
        'catalog_armatura.SteelLengthType',
        verbose_name='Тип длины стали')

    class Meta:
        verbose_name = u"Тип длины стали для фильтра"
        verbose_name_plural = u"Типы длины стали фильтра"
        unique_together = (
            'steel_diameter',
            'steel_class',
            'steel_profile',
            'steel_length_type')

    def __unicode__(self):
        return "%s %s %s %s" % \
            (unicode(self.steel_length_type),
             unicode(self.steel_profile),
             unicode(self.steel_diameter),
             unicode(self.steel_class))


class FilterDiameter(models.Model):

    steel_profile = models.ForeignKey(
        'catalog_armatura.SteelProfile',
        verbose_name='Профиль',
        blank=True,
        null=True)

    steel_diameter = models.ForeignKey(
        'catalog_armatura.SteelDiameter',
        verbose_name='Диаметр')

    steel_class = models.ForeignKey(
        'catalog_armatura.SteelClass',
        verbose_name='Класс стали')

    class Meta:
        verbose_name = u"Диаметры стали для фильтра"
        verbose_name_plural = u"Диаметры стали для фильтра"
        unique_together = ('steel_diameter', 'steel_class')

    def __unicode__(self):
        return unicode(self.steel_diameter) + " " + unicode(self.steel_class)


class FilterMark(models.Model):

    steel_mark = models.ForeignKey(
        'catalog_armatura.SteelMark',
        verbose_name='Марка стали')

    steel_class = models.ForeignKey(
        'catalog_armatura.SteelClass',
        verbose_name='Класс стали')

    class Meta:
        verbose_name = u"Марка стали для фильтра"
        verbose_name_plural = u"Марки стали для фильтра"
        unique_together = ('steel_mark', 'steel_class')

    def __unicode__(self):
        return unicode(self.steel_mark) + " " + unicode(self.steel_class)


class FilterProfile(models.Model):

    steel_profile = models.ForeignKey(
        'catalog_armatura.SteelProfile',
        verbose_name='Профиль')

    steel_class = models.ForeignKey(
        'catalog_armatura.SteelClass',
        verbose_name='Класс стали')

    class Meta:
        verbose_name = u"Профиль стали для фильтра"
        verbose_name_plural = u"Профили стали для фильтра"
        unique_together = ('steel_profile', 'steel_class')

    def __unicode__(self):
        return unicode(self.steel_profile) + " " + unicode(self.steel_class)


class FilterClass(models.Model):

    steel_class = models.ForeignKey(
        'catalog_armatura.SteelClass',
        verbose_name='Класс стали')

    gost = models.ForeignKey(
        'catalog.GOST',
        verbose_name='Гост')

    class Meta:
        verbose_name = u"Классы стали для фильтра"
        verbose_name_plural = u"Классы стали для фильтра"
        unique_together = ('gost', 'steel_class')

    def __unicode__(self):
        return unicode(self.steel_class) + " " + unicode(self.gost)
