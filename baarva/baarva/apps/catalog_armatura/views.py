# -*- coding: utf-8 -*-
import json
from decimal import Decimal

from django.http.response import HttpResponse
from django.template.loader import render_to_string

from .models import FilterClass, FilterDiameter, \
    FilterProfile, FilterMark, FilterSteelLengthType

from baarva.apps.catalog.models import Product
from baarva.apps.producer.models import FilterProducer


def filter_gost(request):

    if request.method == 'GET':
        response_data = {}
        gost_id = request.GET.get('gost_id')
        try:
            gost_id = int(gost_id)
        except:
            gost_id = ""
        if gost_id:
            filter_class_list = FilterClass.objects.filter(gost=gost_id)

            steel_class_list = [f.steel_class for f in filter_class_list]
            context = {
                'steel_class_list': steel_class_list
            }
            template_name = 'blocks/filter/armatura/steel_classes.jhtml'
            html = render_to_string(template_name, context)

            response_data['html'] = html
            return HttpResponse(
                json.dumps(response_data),
                content_type="application/json"
            )

    return HttpResponse(
        json.dumps({
            "nothing to see": "this isn't happening",
        }),
        content_type="application/json"
    )


def render_cs_select(option_list):

    context = {
        'option_list': option_list
    }

    template_name = 'blocks/filter/armatura/cs-select.jhtml'
    html = render_to_string(template_name, context)

    return html


def render_steel_length_field(option_list):

    context = {
        'option_list': option_list
    }

    template_name = 'blocks/filter/armatura/steel_length.jhtml'
    html = render_to_string(template_name, context)

    return html


def filter_steel_class(request):

    if request.method == 'GET':
        response_data = {}
        class_id_list = request.GET.getlist('class_list[]')

        try:
            class_id_list = [int(c) for c in class_id_list]
        except:
            class_id_list = []

        if class_id_list:

            profile_list = FilterProfile.\
                objects.filter(steel_class__in=class_id_list)

            option_list = [
                (
                    p.steel_profile.id,
                    p.steel_profile.title)
                for p in profile_list]
            profile_list_html = render_cs_select(option_list)

            steel_mark_list = FilterMark.\
                objects.filter(steel_class__in=class_id_list)

            option_list = [
                (
                    p.steel_mark.id,
                    p.steel_mark.title)
                for p in steel_mark_list]

            steel_mark_list_html = render_cs_select(option_list)

            diameter_list = FilterDiameter.\
                objects.filter(steel_class__in=class_id_list).\
                order_by('steel_diameter__value')

            option_list = [
                (
                    d.steel_diameter.id,
                    unicode(d.steel_diameter))
                for d in diameter_list]

            steel_diameter_list_html = render_cs_select(option_list)

            response_data = {
                'profile_list_html': profile_list_html,
                'steel_mark_list_html': steel_mark_list_html,
                'steel_diameter_list_html': steel_diameter_list_html,
            }

            return HttpResponse(
                json.dumps(response_data),
                content_type="application/json"
            )

    return HttpResponse(
        json.dumps({
            "nothing to see": "this isn't happening",
        }),
        content_type="application/json"
    )


def filter_steel_length(request):

    response_data = {}
    if request.method == 'GET':

        steel_class = request.GET.getlist('class_list[]')
        steel_profile = request.GET.getlist('profile_list[]')
        steel_diameter = request.GET.getlist('diameter_list[]')
        steel_length_types = FilterSteelLengthType.objects.all()
        filter_id = request.GET.get('filter_id')

        try:
            class_id_list = [int(c) for c in steel_class]
        except:
            class_id_list = []

        if class_id_list:
            steel_length_types = steel_length_types.\
                filter(steel_class__id__in=class_id_list)

        try:
            steel_profile_id_list = [int(p) for p in steel_profile]
        except:
            steel_profile_id_list = []

        if steel_profile_id_list:
            steel_length_types = steel_length_types.\
                filter(steel_profile__id__in=steel_profile_id_list)

        try:
            steel_diameter_id_list = [int(d) for d in steel_diameter]
        except:
            steel_diameter_id_list = []

        if steel_diameter_id_list:
            steel_length_types = steel_length_types.\
                filter(steel_diameter__id__in=steel_diameter_id_list)
        steel_length_types = steel_length_types.distinct()
        option_list = [
            (
                l.steel_length_type.id,
                l.steel_length_type.title,
                l.steel_length_type.measure_unit.title)
            for l in steel_length_types]

        steel_length_types_html = render_steel_length_field(option_list)

        proizvoditel = []
        try:
            filter_id = int(filter_id)
        except:
            pass
        else:
            producer_list = FilterProducer.objects.filter(
                filter__id=filter_id)
            proizvoditel = [
                {
                    'label': unicode(pr.producer),
                    'value': unicode(pr.producer),
                    'id': pr.producer.id
                }
                for pr in producer_list
            ]

        response_data = {
            'steel_length_types_html': steel_length_types_html,
            'proizvoditel': proizvoditel,
        }

        return HttpResponse(
            json.dumps(response_data),
            content_type="application/json"
        )

    return HttpResponse(
        json.dumps({
            "nothing to see": "this isn't happening",
        }),
        content_type="application/json"
    )


def to_id_list(value_list):
    try:
        value_list = [int(v) for v in value_list]
    except ValueError:
        value_list = []
    return value_list


def to_decimal(value):
    if not value:
        return None
    value = value.replace(',', '.')
    try:
        value = Decimal(value)
    except:
        value = None
    return value


def to_int(value):
    if not value:
        return None
    try:
        value = int(value)
    except:
        value = None
    return value


def armatura_form_submit(request):

    response_data = {}
    if request.method == 'GET':

        product_list = Product.objects.all()

        values_list = (
            ('gost', request.GET.getlist('gost_id_list[]')),
            ('steelclass', request.GET.getlist('class_list[]')),
            ('steelprofile', request.GET.getlist('profile_list[]')),
            ('steeldiameter', request.GET.getlist('diameter_list[]')),
            ('steelmark', request.GET.getlist('mark_list[]')),
            ('producer', request.GET.getlist('proizvoditel_list[]'))
        )
        for index, (attr, value_list) in enumerate(values_list):
            values = to_id_list(value_list)
            if values:
                product_list = product_list.filter_by_attr(attr, values)
                id_list = list(product_list.values_list('id', flat=True))
                product_list = Product.objects.filter(id__in=id_list)
                if product_list.exists():
                    continue
        steel_length_type_list = request.GET.getlist(
            'steel_length_type_list[]')
        steel_length_from = to_decimal(request.GET.get('steel_length_from'))
        steel_length_to = to_decimal(request.GET.get('steel_length_to'))

        values = to_id_list(steel_length_type_list)
        if values:
            product_list = product_list.filter_by_steel_length_type(
                values, steel_length_from, steel_length_to)
            id_list = product_list.values_list('id', flat=True)
            product_list = Product.objects.filter(id__in=id_list)

        tax_type = request.GET.get('tax_type')
        if tax_type:
            product_list = product_list.filter_by_tax_type(tax_type)
            id_list = product_list.values_list('id', flat=True)
            product_list = Product.objects.filter(id__in=id_list)

        batch_type = request.GET.get('batch_type')
        if batch_type:
            product_list = product_list.filter_by_batch_type(batch_type)
            id_list = product_list.values_list('id', flat=True)
            product_list = Product.objects.filter(id__in=id_list)

        product_list = product_list.get_product2seller_list()

        context = {
            'object_list': product_list
        }

        template_name = 'blocks/prod-otdel-tovara.jhtml'
        html = render_to_string(template_name, context)

        response_data['html'] = html

        return HttpResponse(
            json.dumps(response_data),
            content_type="application/json"
        )

    return HttpResponse(
        json.dumps({
            "nothing to see": "this isn't happening",
        }),
        content_type="application/json"
    )


def more_met(request, product_id):

    response_data = {}
    if request.method == 'GET' and product_id:

        product = Product.objects.get(id=product_id)

        context = {
            'product': product
        }

        template_name = 'blocks/seller/more-met.jhtml'
        more_met_html = render_to_string(template_name, context)
        response_data['more_met_html'] = more_met_html

        return HttpResponse(
            json.dumps(response_data),
            content_type="application/json"
        )

    return HttpResponse(
        json.dumps({
            "nothing to see": "this isn't happening",
        }),
        content_type="application/json"
    )


def calc_update(request):

    values = {
        'weight': 0,
        'length': 0,
        'amount': 0,
    }
    response_data = {
        'values': values
    }

    if request.method == 'GET':

        product_id = to_int(request.GET.get('product_id'))
        value = to_decimal(request.GET.get('value'))
        value_type = request.GET.get('value_type')
        if product_id and value:
            try:
                product = Product.objects.get(id=product_id)
            except Product.DoesNotExist:
                return HttpResponse(
                    json.dumps({
                        "nothing to see": "this isn't happening",
                    }),
                    content_type="application/json"
                )
            else:
                values = product.convert_values(value, value_type)
                ads = product.ads.filter(weight__lte=values['weight'])\
                                 .order_by('-discount')
                if ads.exists():
                    ads_id = ads[0].id
                    response_data['ads_id'] = ads_id
            response_data['values'] = values

            return HttpResponse(
                json.dumps(response_data),
                content_type="application/json"
            )

    return HttpResponse(
        json.dumps(response_data),
        content_type="application/json"
    )


def calc_price(request):

    response_data = {}
    if request.method == 'GET':

        product_id = to_int(request.GET.get('product_id'))
        weight = to_decimal(request.GET.get('weight'))
        if product_id and weight:
            try:
                product = Product.objects.get(id=product_id)
            except Product.DoesNotExist:
                return HttpResponse(
                    json.dumps({
                        "nothing to see": "this isn't happening",
                    }),
                    content_type="application/json"
                )
            else:
                result = product.get_price(weight)

            response_data['result'] = float(result.quantize(Decimal('.01')))

            return HttpResponse(
                json.dumps(response_data),
                content_type="application/json"
            )

    return HttpResponse(
        json.dumps({
            "nothing to see": "this isn't happening",
        }),
        content_type="application/json"
    )
