# -*- coding: utf-8 -*-
from django.db import models


class MeasureUnit(models.Model):

    title = models.CharField(
        verbose_name=u"Название",
        max_length=255)

    symbol = models.CharField(
        verbose_name=u"Обозначение",
        max_length=255,
        blank=True,
        null=True)

    class Meta:
        verbose_name = u"Единица измерения"
        verbose_name_plural = u"Единицы измерения"

    def __unicode__(self):
        return self.title
