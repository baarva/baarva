from django.contrib import admin

from .models import MeasureUnit


@admin.register(MeasureUnit)
class MeasureUnitAdmin(admin.ModelAdmin):
    pass
