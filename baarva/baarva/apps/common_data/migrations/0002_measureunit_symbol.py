# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('common_data', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='measureunit',
            name='symbol',
            field=models.CharField(max_length=255, null=True, verbose_name='\u041e\u0431\u043e\u0437\u043d\u0430\u0447\u0435\u043d\u0438\u0435', blank=True),
        ),
    ]
