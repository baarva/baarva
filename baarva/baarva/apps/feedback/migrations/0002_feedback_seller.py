# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('seller', '0014_auto_20160219_0519'),
        ('feedback', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='feedback',
            name='seller',
            field=models.ForeignKey(default=1, verbose_name=b'\xd0\x9f\xd1\x80\xd0\xbe\xd0\xb4\xd0\xb0\xd0\xb2\xd0\xb5\xd1\x86', to='seller.Seller'),
            preserve_default=False,
        ),
    ]
