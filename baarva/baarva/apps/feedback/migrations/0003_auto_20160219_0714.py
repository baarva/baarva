# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0002_feedback_seller'),
    ]

    operations = [
        migrations.AlterField(
            model_name='feedback',
            name='seller',
            field=models.ForeignKey(related_name='feedbacks', verbose_name=b'\xd0\x9f\xd1\x80\xd0\xbe\xd0\xb4\xd0\xb0\xd0\xb2\xd0\xb5\xd1\x86', to='seller.Seller'),
        ),
    ]
