# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('buyer', '0002_auto_20160219_0520'),
    ]

    operations = [
        migrations.CreateModel(
            name='Feedback',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('pub_date', models.DateField(verbose_name='\u0414\u0430\u0442\u0430 \u043f\u0443\u0431\u043b\u0438\u043a\u0430\u0446\u0438\u0438')),
                ('title', models.CharField(max_length=255, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a')),
                ('content', ckeditor.fields.RichTextField(verbose_name='\u0421\u043e\u0434\u0435\u0440\u0436\u0430\u043d\u0438\u0435')),
                ('value', models.PositiveIntegerField(verbose_name='\u0417\u043d\u0430\u0447\u0435\u043d\u0438\u0435 \u043e\u0442\u0437\u044b\u0432\u0430')),
                ('approved', models.BooleanField(default=False, verbose_name='\u041f\u043e\u0442\u0432\u0435\u0440\u0436\u0434\u0435\u043d?')),
                ('buyer', models.ForeignKey(verbose_name='\u041f\u043e\u043a\u0443\u043f\u0430\u0442\u0435\u043b\u044c', to='buyer.Buyer')),
            ],
            options={
                'verbose_name': '\u041e\u0442\u0437\u044b\u0432',
                'verbose_name_plural': '\u041e\u0442\u0437\u044b\u0432\u044b',
            },
        ),
    ]
