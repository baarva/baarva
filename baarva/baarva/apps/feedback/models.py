# -*- coding: utf-8 -*-

from django.db import models

from ckeditor.fields import RichTextField


class Feedback(models.Model):

    pub_date = models.DateField(
        verbose_name=u'Дата публикации')

    buyer = models.ForeignKey(
        'buyer.Buyer',
        verbose_name=u"Покупатель")

    seller = models.ForeignKey(
        'seller.Seller',
        verbose_name='Продавец',
        related_name='feedbacks')

    title = models.CharField(
        verbose_name=u"Заголовок",
        max_length=255)

    content = RichTextField(
        verbose_name=u"Содержание",
    )

    value = models.PositiveIntegerField(
        verbose_name=u'Значение отзыва')

    is_positive = models.BooleanField(
        verbose_name=u'Положительный отзыв?',
        default=True)

    approved = models.BooleanField(
        verbose_name=u'Потвержден?',
        default=False)

    image = models.ImageField(
        verbose_name=u"Изображение",
        upload_to="images",
        blank=True,
        null=True)

    class Meta:
        verbose_name = u"Отзыв"
        verbose_name_plural = u"Отзывы"
        ordering = ["-pub_date"]

    def __unicode__(self):
        return self.title
