# -*- coding: utf-8 -*-

from django.db import models

from mptt.models import MPTTModel, TreeForeignKey


class AddressType(models.Model):

    title = models.CharField(
        verbose_name=u"Название",
        max_length=255,
        unique=True)

    short_title = models.CharField(
        verbose_name=u"Сокращение",
        max_length=255,
        default=u""
    )

    use_as_delimeter = models.BooleanField(
        verbose_name=u'Использовать как разделитель?',
        default=False)

    class Meta:
        verbose_name = "Тип в адресе"
        verbose_name_plural = "Типы в адресах"

    def __unicode__(self):
        return self.title


class Address(MPTTModel):

    title = models.CharField(
        verbose_name=u"Название",
        max_length=255,
    )

    address_type = models.ForeignKey(
        'address.AddressType',
        verbose_name=u"Тип",
        related_name='nodes',
        blank=True,
        null=True,
    )

    parent = TreeForeignKey(
        'self',
        verbose_name=u"Адм. единица",
        null=True, blank=True,
        related_name='children',
        db_index=True)

    class Meta:
        verbose_name = "Адрес"
        verbose_name_plural = "Адреса"

    def __unicode__(self):
        title = "%s %s" % (self.title, unicode(self.address_type))
        return title

    def full_title(self):
        title = u" ".join(unit.get_short_title()
                          for unit in self.get_ancestors(include_self=True))
        return title

    def get_short_title(self):
        return "%s %s" % (self.address_type.short_title, self.title)

    def split_address(self):
        ancestors = self.get_ancestors(include_self=True)
        parts = [[], []]
        count = 0
        for part in ancestors:
            if not part.address_type.use_as_delimeter:

                parts[count].append(part)
            else:
                count += 1
                parts[count].append(part)
        return parts

    def get_part_short_title(self, index):
        split_address = self.split_address()
        try:
            part = split_address[index]
        except IndexError:
            return u""
        else:
            title = u" ".join(unit.get_short_title()
                              for unit in part)
            return title
