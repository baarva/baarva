# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('address', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='address',
            options={'verbose_name': '\u0410\u0434\u0440\u0435\u0441', 'verbose_name_plural': '\u0410\u0434\u0440\u0435\u0441\u0430'},
        ),
        migrations.AlterModelOptions(
            name='addresstype',
            options={'verbose_name': '\u0422\u0438\u043f \u0432 \u0430\u0434\u0440\u0435\u0441\u0435', 'verbose_name_plural': '\u0422\u0438\u043f\u044b \u0432 \u0430\u0434\u0440\u0435\u0441\u0430\u0445'},
        ),
    ]
