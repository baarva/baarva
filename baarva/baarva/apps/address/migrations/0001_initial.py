# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import mptt.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Address',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('lft', models.PositiveIntegerField(editable=False, db_index=True)),
                ('rght', models.PositiveIntegerField(editable=False, db_index=True)),
                ('tree_id', models.PositiveIntegerField(editable=False, db_index=True)),
                ('level', models.PositiveIntegerField(editable=False, db_index=True)),
            ],
            options={
                'verbose_name': '\u0410\u0434\u0440\u0435\u0441\u0430',
            },
        ),
        migrations.CreateModel(
            name='AddressType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(unique=True, max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
            ],
            options={
                'verbose_name': '\u0422\u0438\u043f\u044b \u0432 \u0430\u0434\u0440\u0435\u0441\u0430\u0445',
            },
        ),
        migrations.AddField(
            model_name='address',
            name='address_type',
            field=models.ForeignKey(related_name='nodes', verbose_name='\u0422\u0438\u043f', blank=True, to='address.AddressType', null=True),
        ),
        migrations.AddField(
            model_name='address',
            name='parent',
            field=mptt.fields.TreeForeignKey(related_name='children', verbose_name='\u0410\u0434\u043c. \u0435\u0434\u0438\u043d\u0438\u0446\u0430', blank=True, to='address.Address', null=True),
        ),
    ]
