# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('address', '0003_addresstype_short_title'),
    ]

    operations = [
        migrations.AlterField(
            model_name='addresstype',
            name='short_title',
            field=models.CharField(default='', max_length=255, verbose_name='\u0421\u043e\u043a\u0440\u0430\u0449\u0435\u043d\u0438\u0435'),
        ),
    ]
