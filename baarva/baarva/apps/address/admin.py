# -*- coding: utf-8 -*-

from django import forms
from django.contrib import admin

from feincms.admin import tree_editor
from mptt.forms import TreeNodeChoiceField

from .models import Address, AddressType


class AddressForm(forms.ModelForm):
    parent = TreeNodeChoiceField(
        queryset=Address.objects.all(),
        label="Родитель",
        required=False)

    class Meta:
        model = Address
        exclude = ()


@admin.register(Address)
class AddressAdmin(tree_editor.TreeEditor):
    form = AddressForm


@admin.register(AddressType)
class AddressTypeAdmin(admin.ModelAdmin):
    pass
