# -*- coding: utf-8 -*-

from django.db import models

from ckeditor.fields import RichTextField


class News(models.Model):

    seller = models.ForeignKey(
        'seller.Seller',
        verbose_name=u"Прдавец",
        related_name='news',
    )
    pub_date = models.DateField(
        verbose_name=u'Дата публикации')

    title = models.CharField(
        verbose_name=u"Заголовок",
        max_length=255)

    content = RichTextField(
        verbose_name=u"Содержание",
    )

    class Meta:
        verbose_name = u'Новость'
        verbose_name_plural = u'Новости'
        ordering = ['-pub_date']

    def __unicode__(self):
        return self.title
