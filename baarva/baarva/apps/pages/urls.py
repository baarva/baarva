# -*- coding: utf-8 -*-

from django.conf.urls import url
from .views import FlatPageView


urlpatterns = [
    url(r'page-(?P<slug>[^/]*)/$',
        FlatPageView.as_view(),
        name='flatpage')]
