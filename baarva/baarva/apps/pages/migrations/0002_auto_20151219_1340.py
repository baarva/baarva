# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='flatpage',
            options={'verbose_name': '\u0421\u0442\u0430\u0442\u0438\u0447\u043d\u0430\u044f \u0441\u0442\u0440\u0438\u043d\u0446\u0430', 'verbose_name_plural': '\u0421\u0442\u0430\u0442\u0438\u0447\u043d\u044b\u0435 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b'},
        ),
        migrations.AlterField(
            model_name='flatpage',
            name='slug',
            field=models.SlugField(max_length=255, verbose_name='\u0418\u043c\u044f \u043d\u0430 \u0441\u0430\u0439\u0442\u0435'),
        ),
    ]
