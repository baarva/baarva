from django.contrib import admin

from .models import FlatPage
# Register your models here.


@admin.register(FlatPage)
class FlatPageAdmin(admin.ModelAdmin):
    pass
