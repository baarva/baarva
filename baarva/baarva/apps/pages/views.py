# -*- coding: utf-8 -*-

from django.views.generic.detail import DetailView

from baarva.apps.catalog.views import FilterContextMixin

from .models import FlatPage


class FlatPageView(DetailView, FilterContextMixin):

    model = FlatPage
    template_name = u"pages/flatpage.jhtml"

    def get_context_data(self, **kwargs):

        context = super(FlatPageView, self).get_context_data(**kwargs)
        context.update(self.get_filter_context_data(self.request, **kwargs))

        return context
