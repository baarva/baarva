# -*- coding: utf-8 -*-

from django.db import models

from ckeditor.fields import RichTextField


class FlatPage(models.Model):

    title = models.CharField(
        verbose_name=u"Название",
        max_length=255)

    slug = models.SlugField(
        verbose_name=u"Имя на сайте",
        max_length=255)

    content = RichTextField(verbose_name=u"Содержание")

    class Meta:
        verbose_name = u'Статичная стринца'
        verbose_name_plural = u"Статичные страницы"

    def __unicode__(self):
        return self.title
