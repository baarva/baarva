﻿(function($){

     $(document).ready(function(){

  		$(".listKatalog").navgoco({
			accordion: true,
			openClass: 'activeUl',
			slide: {
				duration: 'string',
				easing: 'swing'
			}
		});

		$('.menuKatalog li ul').each(function() {

			if($(this).find('ul').is('ul')){
				$(this).removeClass('lastUl').parent('li').removeClass('lastLI');
			} else {
				$(this).addClass('lastUl').parent('li').addClass('lastLI');
			}

      	}) 


	});


})(jQuery);