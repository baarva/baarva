$(document).ready(function() {

  	var calc_timer = 0;
    $( document ).on('keydown', '.calc-input',  function() {
        clearTimeout(calc_timer);
        window.el = $(this);
        calc_timer = setTimeout(update_calc, 2000); 
      });

    $( document ).on('keypress', '.calc-input',  function(evt){
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode != 46 && charCode > 31 
          && (charCode < 48 || charCode > 57))
           return false;
        return true;
      });

    update_calc = function(){
    	el = window.el
    	$('.calc-input').prop('disabled', 'disabled');
      $('.bl-actions-m').removeClass('select-actions');

    	value_type = el.data("value-type");
    	value = el.val();
    	product_id = $('#moreMet').data('product-id'); 
    	
    	$.ajax({
			url: '/catalog/armatura/calc/',
            type : "GET",
          	data : { 
          		value_type: value_type,
          		value: value,
          		product_id: product_id,
          		},
          	success : function(json) {

                $('.calc-weight').val(json.values.weight);
                $('.calc-length').val(json.values.length);
                $('.calc-amount').val(json.values.amount);

                $('.default-price').show();
                $('.result-price').text('');
                $('.current-price').show();
              
              if (json.values.weight !="0"){
            	
            		$('.btn-calculator').removeClass('disabled');


                if (json.ads_id){
                  selector = "#ads_" + json.ads_id;
                  $(selector).addClass('select-actions');
                  }
            		}
              else{
                $('.btn-calculator').addClass('disabled');
                }

              }

          });

    	$('.calc-input').prop('disabled', '');
    };

    $( document ).on('click', '.btn-calculator', function(){
    	if ($(this).hasClass('disabled')){

    	}
    	else{

    		weight = parseFloat($('.calc-weight').val());
	    	product_id = $('#moreMet').data('product-id'); 

    		$.ajax({
				url: '/catalog/armatura/calc_price/',
	            type : "GET",
	          	data : { 
	          		weight: weight,
	          		product_id: product_id,
	          		},
	          	success : function(json) {
	          		$('.default-price').hide();
	          		$('.result-price').text(json.result + ' p ');
	          		$('.current-price').show();
	          		}

	          });
    	}
		$(this).addClass('disabled');    	
    });
    init_more_met_button();

});