﻿(function($){

    $(document).ready(function(){

		var npochtyua = $(".form-yua input[name='npocht']").val(),
		    nrfyua = $(".form-yua input[name='nrf']").val(),
		    npoyasyua = $(".form-yua input[name='npoyas']").val(),
		    nrayonyua = $(".form-yua input[name='nrayon']").val(),
		    nsityyua = $(".form-yua input[name='nsity']").val(),
		    nposnyua = $(".form-yua input[name='nposn']").val(),
		    nstyua = $(".form-yua input[name='nst']").val(),
		    ndomyua = $(".form-yua input[name='ndom']").val(),
		    nofisyua = $(".form-yua input[name='nofis']").val();

		var npochtpocht = $(".form-pocht input[name='npocht']").val(),
		    nrfpocht = $(".form-pocht input[name='nrf']").val(),
		    npoyaspocht = $(".form-pocht input[name='npoyas']").val(),
		    nrayonpocht = $(".form-pocht input[name='nrayon']").val(),
		    nsitypocht = $(".form-pocht input[name='nsity']").val(),
		    nposnpocht = $(".form-pocht input[name='nposn']").val(),
		    nstpocht = $(".form-pocht input[name='nst']").val(),
		    ndompocht = $(".form-pocht input[name='ndom']").val(),
		    nofispocht = $(".form-pocht input[name='nofis']").val();

	    $('.remove-file').each(function() {
		    $(this).on('click', function() {		    	
				$(this).parents('.wh-3').find("input").val('');
		    });
	    });

	    $('.reg-pocht .matches-jur').on('click', function() {
	    	$("html, body").animate({ scrollTop: $('.reg-pocht').offset().top }, 500);

			$(this).parents('.rj').find(".form-pocht input[name='npocht']").val($(this).parents('.rj').find(".form-yua input[name='npocht']").val());
			$(this).parents('.rj').find(".form-pocht input[name='nrf']").val($(this).parents('.rj').find(".form-yua input[name='nrf']").val());
			$(this).parents('.rj').find(".form-pocht input[name='npoyas']").val($(this).parents('.rj').find(".form-yua input[name='npoyas']").val());
			$(this).parents('.rj').find(".form-pocht input[name='nrayon']").val($(this).parents('.rj').find(".form-yua input[name='nrayon']").val());
			$(this).parents('.rj').find(".form-pocht input[name='nsity']").val($(this).parents('.rj').find(".form-yua input[name='nsity']").val());
			$(this).parents('.rj').find(".form-pocht input[name='nposn']").val($(this).parents('.rj').find(".form-yua input[name='nposn']").val());
			$(this).parents('.rj').find(".form-pocht input[name='nst']").val($(this).parents('.rj').find(".form-yua input[name='nst']").val());
			$(this).parents('.rj').find(".form-pocht input[name='ndom']").val($(this).parents('.rj').find(".form-yua input[name='ndom']").val());
			$(this).parents('.rj').find(".form-pocht input[name='nofis']").val($(this).parents('.rj').find(".form-yua input[name='nofis']").val());

	    });

	    $('.reg-facta .matches-mail').on('click', function() {
	    	$("html, body").animate({ scrollTop: $('.reg-facta').offset().top }, 500);

			$(this).parents('.rj').find(".form-facta input[name='npocht']").val($(this).parents('.rj').find(".form-pocht input[name='npocht']").val());
			$(this).parents('.rj').find(".form-facta input[name='nrf']").val($(this).parents('.rj').find(".form-pocht input[name='nrf']").val());
			$(this).parents('.rj').find(".form-facta input[name='npoyas']").val($(this).parents('.rj').find(".form-pocht input[name='npoyas']").val());
			$(this).parents('.rj').find(".form-facta input[name='nrayon']").val($(this).parents('.rj').find(".form-pocht input[name='nrayon']").val());
			$(this).parents('.rj').find(".form-facta input[name='nsity']").val($(this).parents('.rj').find(".form-pocht input[name='nsity']").val());
			$(this).parents('.rj').find(".form-facta input[name='nposn']").val($(this).parents('.rj').find(".form-pocht input[name='nposn']").val());
			$(this).parents('.rj').find(".form-facta input[name='nst']").val($(this).parents('.rj').find(".form-pocht input[name='nst']").val());
			$(this).parents('.rj').find(".form-facta input[name='ndom']").val($(this).parents('.rj').find(".form-pocht input[name='ndom']").val());
			$(this).parents('.rj').find(".form-facta input[name='nofis']").val($(this).parents('.rj').find(".form-pocht input[name='nofis']").val());	

	    });

	    $('.reg-facta .matches-jur').on('click', function() {
	    	$("html, body").animate({ scrollTop: $('.reg-facta').offset().top }, 500);

			$(this).parents('.rj').find(".form-facta input[name='npocht']").val($(this).parents('.rj').find(".form-yua input[name='npocht']").val());
			$(this).parents('.rj').find(".form-facta input[name='nrf']").val($(this).parents('.rj').find(".form-yua input[name='nrf']").val());
			$(this).parents('.rj').find(".form-facta input[name='npoyas']").val($(this).parents('.rj').find(".form-yua input[name='npoyas']").val());
			$(this).parents('.rj').find(".form-facta input[name='nrayon']").val($(this).parents('.rj').find(".form-yua input[name='nrayon']").val());
			$(this).parents('.rj').find(".form-facta input[name='nsity']").val($(this).parents('.rj').find(".form-yua input[name='nsity']").val());
			$(this).parents('.rj').find(".form-facta input[name='nposn']").val($(this).parents('.rj').find(".form-yua input[name='nposn']").val());
			$(this).parents('.rj').find(".form-facta input[name='nst']").val($(this).parents('.rj').find(".form-yua input[name='nst']").val());
			$(this).parents('.rj').find(".form-facta input[name='ndom']").val($(this).parents('.rj').find(".form-yua input[name='ndom']").val());
			$(this).parents('.rj').find(".form-facta input[name='nofis']").val($(this).parents('.rj').find(".form-yua input[name='nofis']").val());			

	    });

	    $('.block-registration-jur a[href="#reg1"]').on('click', function() {

			$(this).parents('.block-registration-jur').find('#reg1 .form-identif').animate({
				height: "toggle",
				opacity: "toggle"
				}, {
				duration: "slow"
			}).addClass('fhide');      			

			$(this).parents('.block-registration-jur').find('#reg1 .reg-identif').removeClass('disabled');
	    });

	    $('.block-registration-jur a[href="#reg2"]').on('click', function() {

			$(this).parents('.block-registration-jur').find('#reg2 .form-identif').animate({
				height: "toggle",
				opacity: "toggle"
				}, {
				duration: "slow"
			});			

			$(this).parents('.block-registration-jur').find('#reg2 .form-identif').addClass('fhide');
	    });  	    

	    $('.reg-osn .form-edit').on('click', function() {
			$(this).parents('.block-registration-jur').find('.form-osn').animate({
				height: "toggle",
				opacity: "toggle"
				}, {
				duration: "slow"
			});      
			$('.form-yua').hide();
			$('.form-pocht').hide();
			$('.form-facta').hide();
			$('.form-bank-rekv').hide();
			$('.form-sved').hide();
			$('.form-punkt-vud').hide();
			$('.form-identif').hide();
			$('.form-docum').hide();
			$(this).parents('.rj').find(".form-osn input[name='emailOsn']").val($(this).parents('.rj').find(".form-identif input[name='emailOsn']").val());
	    })  	    

	    $('.reg-yua .form-edit').on('click', function() {
			$(this).parents('.block-registration-jur').find('.form-yua').animate({
				height: "toggle",
				opacity: "toggle"
				}, {
				duration: "slow"
			});    
			$('.form-osn').hide();
			$('.form-pocht').hide();
			$('.form-facta').hide();
			$('.form-bank-rekv').hide();
			$('.form-sved').hide();
			$('.form-punkt-vud').hide();
			$('.form-identif').hide();
			$('.form-docum').hide();
	    })  	    

	    $('.reg-pocht .form-edit').on('click', function() {
			$(this).parents('.block-registration-jur').find('.form-pocht').animate({
				height: "toggle",
				opacity: "toggle"
				}, {
				duration: "slow"
			});    
			$('.form-osn').hide();
			$('.form-yua').hide();
			$('.form-facta').hide();
			$('.form-bank-rekv').hide();
			$('.form-sved').hide();
			$('.form-punkt-vud').hide();
			$('.form-identif').hide();
			$('.form-docum').hide();
	    }) 	    

	    $('.reg-facta .form-edit').on('click', function() {
			$(this).parents('.block-registration-jur').find('.form-facta').animate({
				height: "toggle",
				opacity: "toggle"
				}, {
				duration: "slow"
			});    
			$('.form-osn').hide();
			$('.form-yua').hide();
			$('.form-pocht').hide();
			$('.form-bank-rekv').hide();
			$('.form-sved').hide();
			$('.form-punkt-vud').hide();
			$('.form-identif').hide();
			$('.form-docum').hide();
	    })  	    

	    $('.reg-bank-rekv .form-edit').on('click', function() {
			$(this).parents('.block-registration-jur').find('.form-bank-rekv').animate({
				height: "toggle",
				opacity: "toggle"
				}, {
				duration: "slow"
			});    
			$('.form-osn').hide();
			$('.form-yua').hide();
			$('.form-pocht').hide();
			$('.form-facta').hide();
			$('.form-sved').hide();
			$('.form-punkt-vud').hide();
			$('.form-identif').hide();
			$('.form-docum').hide();
	    })   

	    $('.reg-sved .form-edit').on('click', function() {
			$(this).parents('.block-registration-jur').find('.form-sved').animate({
				height: "toggle",
				opacity: "toggle"
				}, {
				duration: "slow"
			});    
			$('.form-osn').hide();
			$('.form-yua').hide();
			$('.form-pocht').hide();
			$('.form-facta').hide();
			$('.form-bank-rekv').hide();
			$('.form-punkt-vud').hide();
			$('.form-identif').hide();
			$('.form-docum').hide();
	    })   
	    
	    $('.reg-punkt-vud .form-edit').on('click', function() {
			$(this).parents('.block-registration-jur').find('.form-punkt-vud').animate({
				height: "toggle",
				opacity: "toggle"
				}, {
				duration: "slow"
			});    
			$('.form-osn').hide();
			$('.form-yua').hide();
			$('.form-pocht').hide();
			$('.form-facta').hide();
			$('.form-bank-rekv').hide();
			$('.form-sved').hide();
			$('.form-identif').hide();
			$('.form-docum').hide();
	    }) 	 
	       
	    $('.reg-identif .form-edit').on('click', function() {
			$(this).parents('.block-registration-jur').find('.form-identif').animate({
				height: "toggle",
				opacity: "toggle"
				}, {
				duration: "slow"
			});    
			$('.form-osn').hide();
			$('.form-yua').hide();
			$('.form-pocht').hide();
			$('.form-facta').hide();
			$('.form-bank-rekv').hide();
			$('.form-sved').hide();
			$('.form-punkt-vud').hide();
			$('.form-docum').hide();
	    })  	    

	    $('.reg-docum .form-edit').on('click', function() {
			$(this).parents('.block-registration-jur').find('.form-docum').animate({
				height: "toggle",
				opacity: "toggle"
				}, {
				duration: "slow"
			});    
			$('.form-osn').hide();
			$('.form-yua').hide();
			$('.form-pocht').hide();
			$('.form-facta').hide();
			$('.form-bank-rekv').hide();
			$('.form-sved').hide();
			$('.form-punkt-vud').hide();
			$('.form-identif').hide();
	    })   

		$('.rj').each(function(){

			var formosn = $(this).find('.form-osn'),
				formyua = $(this).find('.form-yua'),
				formpocht = $(this).find('.form-pocht'),
				formfacta = $(this).find('.form-facta'),
				formbankrekv = $(this).find('.form-bank-rekv'),
				formsved = $(this).find('.form-sved'),
				formpunktvud = $(this).find('.form-punkt-vud'),
				formidentif = $(this).find('.form-identif'),
				formdocum = $(this).find('.form-docum'),
		        forms = $(this),
		        btnall = $(this).find('.form-separator-title'),
		        btnosn = $(this).find('.reg-osn'),
		        btnyua = $(this).find('.reg-yua'),
		        btnpocht = $(this).find('.reg-pocht'),
				btnfacta = $(this).find('.reg-facta'),
				btnbankrekv = $(this).find('.reg-bank-rekv'),
				btnsved = $(this).find('.reg-sved'),
				btnpunktvud = $(this).find('.reg-punkt-vud'),
				btnidentif = $(this).find('.reg-identif'),
				btndocum = $(this).find('.reg-docum'),	        
				btnap = $(this).find('.bt-apply'),
				blcapcha = $(this).find('.block-captcha');

			$(this).find('.rsurely').addClass('empty-surely');
			$(this).find('.rnumber').addClass('empty-surely');
			$(this).find('.remail').addClass('empty-surely');
			$(this).find('.remailob').addClass('empty-surely');
			$(this).find('.website').addClass('empty-surely');
			$(this).find('.rinn').addClass('empty-surely');
			$(this).find('.rkpp').addClass('empty-surely');
			$(this).find('.rogrn').addClass('empty-surely');
			$(this).find('.rokato').addClass('empty-surely');
			$(this).find('.password').addClass('empty-surely');
			$(this).find('.rfile').addClass('empty-surely');
			$(this).find('.rall').addClass('empty-surely');
			$(this).find('.rnamelogin').addClass('empty-surely');
			$(this).find('.rcaptcha').addClass('empty-surely');
			$(this).find('.rfcity').addClass('empty-surely');


		    // Функция проверки полей формы
		    function checkInputYua(){


				forms.find('.aut').each(function(){

		      		if ( $(this).val() == 0) {
				       $(this).addClass('empty-surely');
				    } else { 			      
				       $(this).removeClass('empty-surely');
				       $(this).removeAttr('style');
				    } 

				}); 

			    forms.find('.rsurely').each(function(){

		      		if ( $(this).val() == 0) {
				       $(this).addClass('empty-surely');
				    } else if ( !isValidN($(this).val()) ) { 	
				       $(this).addClass('empty-surely');
			            $(this).css({'border-color':'#cc0000'});	
				    } else { 			      
				       $(this).removeClass('empty-surely');
				       $(this).removeAttr('style');
				    } 

			    });				    

			    forms.find('.rfpnd').each(function(){

					if ( !isValidN($(this).val()) ) { 	
				       	$(this).addClass('empty-surely');
			            $(this).blur(function(){
			            	$(this).css({'border-color':'#cc0000'});			            	
			            })
				    } else { 			      
				       	$(this).removeClass('empty-surely');
				       	$(this).removeAttr('style');
				    } 

			    });				    


			    forms.find('.rfcity').each(function(){

		      		if ( $(this).val() == 0) {
				        $(this).addClass('empty-surely');
				    } else if ( !isValidN($(this).val()) ) { 	
				       $(this).addClass('empty-surely');
			           $(this).css({'border-color':'#cc0000'});	
				    } else { 			      
				       $(this).removeClass('empty-surely');
				       $(this).removeAttr('style');

				       $(this).parents('.form-separator').find('.rfpnd').each(function(){
							if ( $(this).val() == 0) {
						       $(this).removeClass('empty-surely');
						       $(this).removeAttr('style');
					    	} else	if ( !isValidN($(this).val()) ) { 	
						       	$(this).addClass('empty-surely');
					            $(this).blur(function(){
					            	$(this).css({'border-color':'#cc0000'});			            	
					            })
						    } else { 			      
						       	$(this).removeClass('empty-surely');
						       	$(this).removeAttr('style');
						    } 
					    });



				    } 

			    });	

			    forms.find('.rall').each(function(){

		      		if ( $(this).val() == 0) {
				       $(this).addClass('empty-surely');
				    } else if ( !isValidAll($(this).val()) ) { 	
				       $(this).addClass('empty-surely');
			            $(this).blur(function(){
			            	$(this).css({'border-color':'#cc0000'});			            	
			            })
				    } else { 			      
				       $(this).removeClass('empty-surely');
				       $(this).removeAttr('style');
				    } 

			    });				    

			    forms.find('.rnamelogin').each(function(){

		      		if ( $(this).val() == 0) {
				       $(this).addClass('empty-surely');
				    } else if ( !isValidLoginName($(this).val()) ) { 	
				       $(this).addClass('empty-surely');
			            $(this).blur(function(){
			            	$(this).css({'border-color':'#cc0000'});			            	
			            })
				    } else { 			      
				       $(this).removeClass('empty-surely');
				       $(this).removeAttr('style');
				    } 

			    });				    

			    forms.find('.rnumber').each(function(){

		      		if ( $(this).val() == 0 ) {
				       $(this).addClass('empty-surely');
				    } else if ( !isValidNumber($(this).val()) ) { 	
				       $(this).addClass('empty-surely');
			            $(this).blur(function(){
			            	$(this).css({'border-color':'#cc0000'});			            	
			            })
				    } else { 			      
				       $(this).removeClass('empty-surely');
				       $(this).removeAttr('style');
				    } 

			    });	

			    forms.find('.rinn').each(function(){

		      		if ( $(this).val() == '' && $(this).val().length < 10 ) {
				       $(this).addClass('empty-surely');
				    } else if ( !isValidNumber($(this).val()) || $(this).val().length < 10) { 	
				       $(this).addClass('empty-surely');
			            $(this).blur(function(){
			            	$(this).css({'border-color':'#cc0000'});			            	
			            })
				    } else { 			      
				       $(this).removeClass('empty-surely');
				       $(this).removeAttr('style');
				    } 

			    });				    

			    forms.find('.rkpp').each(function(){

		      		if ( $(this).val() == '' && $(this).val().length < 9 ) {
				       $(this).addClass('empty-surely');
				    } else if ( !isValidNumber($(this).val()) || $(this).val().length < 9) { 	
				       $(this).addClass('empty-surely');
			            $(this).blur(function(){
			            	$(this).css({'border-color':'#cc0000'});			            	
			            })
				    } else { 			      
				       $(this).removeClass('empty-surely');
				       $(this).removeAttr('style');
				    } 

			    });				    

			    forms.find('.rogrn').each(function(){

		      		if ( $(this).val() == '' && $(this).val().length < 13 ) {
				       $(this).addClass('empty-surely');
				    } else if ( !isValidNumber($(this).val()) || $(this).val().length < 13) { 	
				       $(this).addClass('empty-surely');
			            $(this).blur(function(){
			            	$(this).css({'border-color':'#cc0000'});			            	
			            })
				    } else { 			      
				       $(this).removeClass('empty-surely');
				       $(this).removeAttr('style');
				    } 

			    });				    

			    forms.find('.rokato').each(function(){

		      		if ( $(this).val() == '' && $(this).val().length < 11 ) {
				       $(this).addClass('empty-surely');
				    } else if ( !isValidNumber($(this).val()) || $(this).val().length < 11) { 	
				       $(this).addClass('empty-surely');
			            $(this).blur(function(){
			            	$(this).css({'border-color':'#cc0000'});			            	
			            })
				    } else { 			      
				       $(this).removeClass('empty-surely');
				       $(this).removeAttr('style');
				    } 

			    });				    

			    forms.find('.phone1').each(function(){
			        if ( ($(this).val().indexOf("_") != -1) || $(this).val() == '' ) {
			            $(this).addClass('empty-surely');
			        } else {
			            $(this).removeClass('empty-surely');
			            $(this).removeAttr('style');
			        }
			    });			    

			    forms.find('.phone-gor').each(function(){
			        if ( ($(this).val().indexOf("_") != -1) || $(this).val() == '' ) {
			            $(this).addClass('empty-surely');
			        } else {
			            $(this).removeClass('empty-surely');
			            $(this).removeAttr('style');
			        }
			    });	  


				forms.find('.remail').each(function(){

		      		if ( $(this).val() == 0) {
				       $(this).removeClass('empty-surely');
				       $(this).removeAttr('style');
				    } else if (!isValidEmail1($(this).val())) { 	
			            $(this).addClass('empty-surely');
			            $(this).blur(function(){
			            	$(this).css({'border-color':'#cc0000'});			            	
			            })			            
				    }  else { 			      
			            $(this).removeClass('empty-surely');
			            $(this).removeAttr('style');
				    }    

				});					

				forms.find('.remailob').each(function(){

		      		if ( $(this).val() == 0) {
				       $(this).addClass('empty-surely');
				    } else if (!isValidEmail1($(this).val())) { 	
			            $(this).addClass('empty-surely');
			            $(this).blur(function(){
			            	$(this).css({'border-color':'#cc0000'});			            	
			            })
				    }  else { 			      
			            $(this).removeClass('empty-surely');
			            $(this).removeAttr('style');
				    }    

				});				

				forms.find('.website').each(function(){

		      		if ( $(this).val() == 0) {
				       $(this).removeClass('empty-surely');
				       $(this).removeAttr('style');
				    } else if ( !Lat($(this).val()) ) { 	
				       $(this).addClass('empty-surely');
			            $(this).blur(function(){
			            	$(this).css({'border-color':'#cc0000'});			            	
			            })
				    } else { 			      
				       $(this).removeClass('empty-surely');
				       $(this).removeAttr('style');
				    }

				});					

				forms.find('.password').each(function(){

		            var pas1 = $("#pas1").val();
		            var pas2 = $("#pas2").val();

		      		if ( $(this).val() == 0 && $(this).val().length < 6 ) {
				       $(this).addClass('empty-surely');
				    } else if ( pas1!=pas2 || $(this).val().length < 6 ) { 	
				       $(this).addClass('empty-surely');
			            $("#pas2").blur(function(){		            	
				       		$(this).css({'border-color':'#cc0000'});
				       		$("#pas1").css({'border-color':'#cc0000'});
			            })				       
				    } else { 			      
				       $(this).removeClass('empty-surely');
				       $(this).removeAttr('style');
				    }
				    
				});				

				forms.find('.rfile').each(function(){

		      		if ( $(this).val() == 0) {
				       $(this).addClass('empty-surely');
				       $(this).parents('.wh-3').find('.remove-file').hide();
				    } else { 			      
				       $(this).parents('.wh-3').find('.remove-file').show();
				       $(this).removeClass('empty-surely');
				       $(this).parents('.file_upload').removeAttr('style');
				    }
				    
				});			    


				forms.find('.rcaptcha').each(function(){

		            var rcaptcha1 = $(this).parents('.captcha').find(".realperson-text input").attr('value');
		            var rcaptcha2 = $(this).val();

		      		if ( $(this).val() == 0 ) {
				       $(this).addClass('empty-surely');
				    } else if ( rcaptcha2!=rcaptcha1 ) { 	
				       $(this).addClass('empty-surely');
				       $(this).css({'color':'#cc0000','background-color':'#f6d8d8'});
				    } else { 			      
				       $(this).removeClass('empty-surely');
				       $(this).removeAttr('style');
				    }
				    
				});	

		    }

			setInterval(function(){
				checkInputYua();
				var sizeOsn = formosn.find('.empty-surely').size(),
					sizeYua = formyua.find('.empty-surely').size(),
					sizePocht = formpocht.find('.empty-surely').size(),
					sizeFacta = formfacta.find('.empty-surely').size(),
					sizeBankRekv = formbankrekv.find('.empty-surely').size(),
					sizeSved = formsved.find('.empty-surely').size(),
					sizePunktVud = formpunktvud.find('.empty-surely').size(),
					sizeIdentif = formidentif.find('.empty-surely').size();
					sizeDocum = formdocum.find('.empty-surely').size(),
					sizeCapcha = blcapcha.find('.empty-surely').size();


				//  Идентификационные данные контактного лица   	
				if(sizeIdentif > 0){	

					if(btnosn.hasClass('disabled')){
						btnidentif.find('.form-edit').hide();
						btnidentif.find('span').removeClass('icon-ok');
						formosn.removeClass('fhide');
						$(this).parents('.rj').find('.form-control').removeAttr('style');
						return false
					} else {
						btnidentif.addClass('disabled');
						$('.btn-later').addClass('disabled');
						btnosn.addClass('disabled');
						btnidentif.find('span').addClass('icon-ok');
						btnidentif.find('.form-edit').hide();
						formosn.removeClass('fhide');
						$(this).parents('.rj').find('.form-control').removeAttr('style');
					}					

				} else {
					$('.btn-later').removeClass('disabled');
					btnidentif.find('span').addClass('icon-ok');
					btnosn.removeClass('disabled');
					btnidentif.removeClass('disabled');
					btnidentif.find('.form-edit').show();
					formidentif.addClass('fhide');
				}

				// Основные сведения
				if(sizeOsn > 0){

					if(btnyua.hasClass('disabled')){
						btnosn.find('span').removeClass('icon-ok');
						formyua.removeClass('fhide');
						$(this).parents('.rj').find('.form-control').removeAttr('style');
						return false
					} else {
						btnyua.addClass('disabled');
						formyua.removeClass('fhide');
						btnosn.find('span').removeClass('icon-ok');
						btnosn.find('.form-edit').hide();
						$(this).parents('.rj').find('.form-control').removeAttr('style');
					}					
				} else {
					btnosn.find('span').addClass('icon-ok');
					btnyua.removeClass('disabled');
					btnosn.find('.form-edit').show();
					formyua.addClass('fhide');
				}

				// Юридический адрес
				if(sizeYua > 0){			      	
						// btnpocht.addClass('disabled');

					if(btnpocht.hasClass('disabled')){
						btnyua.find('.form-edit').hide();
				        btnpocht.find('.matches-jur').hide();
						btnyua.find('span').removeClass('icon-ok');
						formpocht.removeClass('fhide');
						$(this).parents('.rj').find('.form-control').removeAttr('style');
						return false
					} else {
						btnpocht.addClass('disabled');
						btnyua.find('.form-edit').hide();
				        btnpocht.find('.matches-jur').hide();
						btnyua.find('span').removeClass('icon-ok');
						formpocht.removeClass('fhide');
						$(this).parents('.rj').find('.form-control').removeAttr('style');
					}			        
				} else {
					btnyua.find('span').addClass('icon-ok');
					btnpocht.removeClass('disabled');
					btnyua.find('.form-edit').show();
				    btnpocht.find('.matches-jur').show();
					formpocht.addClass('fhide');
				}				

				// Почтовый адрес
				if(sizePocht > 0){		

					if(btnfacta.hasClass('disabled')){
						btnpocht.find('span').removeClass('icon-ok');
						btnpocht.find('.form-edit').hide();
				        btnfacta.find('.matches-jur').hide();
				        btnfacta.find('.matches-mail').hide();
						formfacta.removeClass('fhide');
						$(this).parents('.rj').find('.form-control').removeAttr('style');
						return false
					} else {
						btnfacta.addClass('disabled');
						btnpocht.find('.form-edit').hide();
				        btnfacta.find('.matches-jur').hide();
				        btnfacta.find('.matches-mail').hide();
						btnpocht.find('span').removeClass('icon-ok');
						formfacta.removeClass('fhide');
						$(this).parents('.rj').find('.form-control').removeAttr('style');
					}			        
				} else {
					btnpocht.find('span').addClass('icon-ok');
					btnfacta.removeClass('disabled');
					btnpocht.find('.form-edit').show();
				    btnfacta.find('.matches-jur').show();
				    btnfacta.find('.matches-mail').show();
					formfacta.addClass('fhide');
				}				

				// Фактический адрес
				if(sizeFacta > 0){			 

					if(btnbankrekv.hasClass('disabled')){
						btnfacta.find('.form-edit').hide();
						btnfacta.find('span').removeClass('icon-ok');
						formbankrekv.removeClass('fhide');
						$(this).parents('.rj').find('.form-control').removeAttr('style');
						return false
					} else {
						btnfacta.find('span').removeClass('icon-ok');
						btnbankrekv.addClass('disabled');
						btnfacta.find('.form-edit').hide();
						formbankrekv.removeClass('fhide');
						$(this).parents('.rj').find('.form-control').removeAttr('style');
					}			        
				} else {
					btnfacta.find('span').addClass('icon-ok');
					btnbankrekv.removeClass('disabled');
					btnfacta.find('.form-edit').show();
					formbankrekv.addClass('fhide');
				}				

				// Банковские реквизиты
				if(sizeBankRekv > 0){		

					if(btnsved.hasClass('disabled')){
						btnbankrekv.find('span').removeClass('icon-ok');
						btnbankrekv.find('.form-edit').hide();
						formsved.removeClass('fhide');
						$(this).parents('.rj').find('.form-control').removeAttr('style');
						return false
					} else {
						btnbankrekv.find('span').removeClass('icon-ok');
						btnsved.addClass('disabled');
						btnbankrekv.find('.form-edit').hide();
						formsved.removeClass('fhide');
						$(this).parents('.rj').find('.form-control').removeAttr('style');
					}			        
				} else {
					btnbankrekv.find('span').addClass('icon-ok');
					btnsved.removeClass('disabled');
					formsved.addClass('fhide');
					btnbankrekv.find('.form-edit').show();
				}				

				// Сведения о лице подавшему заявку
				if(sizeSved > 0){	

					if(btndocum.hasClass('disabled')){
						btndocum.find('span').removeClass('icon-ok');
						btnsved.find('.form-edit').hide();
						formdocum.removeClass('fhide');
						$(this).parents('.rj').find('.form-control').removeAttr('style');
						return false
					} else {
						btnsved.find('span').removeClass('icon-ok');
						btndocum.addClass('disabled');
						btnsved.find('.form-edit').hide();
						formdocum.removeClass('fhide');
						$(this).parents('.rj').find('.form-control').removeAttr('style');
					}					
				} else {
					btnsved.find('span').addClass('icon-ok');
					btndocum.removeClass('disabled');
					btnsved.find('.form-edit').show();
					formdocum.addClass('fhide');
				}				

				// Документы
				if(sizeDocum > 0 ){	
					if(blcapcha.hasClass('disabled')){
						blcapcha.find('.rcaptcha').attr('disabled','disabled');
						btndocum.find('.form-edit').hide();
						btndocum.find('span').removeClass('icon-ok');
						return false
					} else {
						blcapcha.addClass('disabled');
						blcapcha.find('.rcaptcha').attr('disabled','disabled');
						btndocum.find('span').removeClass('icon-ok');
						btndocum.find('.form-edit').hide();
					}					
			        
				} else {
					blcapcha.removeClass('disabled');
					blcapcha.find('.rcaptcha').removeAttr('disabled','disabled');
					btndocum.find('span').addClass('icon-ok');
					btndocum.find('.form-edit').show();
				}			

				// Капча
				if(sizeCapcha > 0){		
					if(btnap.hasClass('disabled')){
						blcapcha.find('.realperson-text').removeClass('icon-ok');
						return false
					} else {
						btnap.addClass('disabled');
						blcapcha.find('.realperson-text').removeClass('icon-ok');
						blcapcha.find('.empty-surely').parents('.file_upload').removeAttr('style');
					}						
			        
				} else {
					btnap.removeClass('disabled');
					blcapcha.find('.realperson-text').addClass('icon-ok');
				}

			},500);

				// Идентификационные данные контактного лица
				btnidentif.click(function(){

				});				

				// Основные сведения
			    btnosn.click(function(){
			      	if(btnosn.hasClass('disabled')){
				      	$('.fhide.form-identif').find('.empty-surely').css({'border-color':'#cc0000'});
				        // btnosn.find('.form-edit').hide();
				        return false
				    } else {
				        formosn.show();
				        formpocht.hide();
				        formfacta.hide();
				        formbankrekv.hide();
				        formsved.hide();
				        formpunktvud.hide();
				        formidentif.hide();
				        formdocum.hide();
				        formyua.hide();
				        btnidentif.find('.form-edit').show();
				    	$(this).parents('.rj').find(".form-osn input[name='emailOsn']").val($(this).parents('.rj').find(".form-identif input[name='emailOsn']").val());
				        $("html, body").animate({ scrollTop: $('.reg-identif').offset().top }, 500);
				    }			      	


				    if(btnyua.hasClass('disabled')){
				      	$('.form-osn').addClass('fhide');
				        return false
				    } else {
				      	$('.form-osn').removeClass('fhide');
				    }
				});

				// Юридический адрес
			    btnyua.click(function(){
			      	if(btnyua.hasClass('disabled')){
				      	$('.fhide.form-osn').find('.empty-surely').css({'border-color':'#cc0000'});
				        btnosn.find('.form-edit').hide();
				        return false
				    } else {
				        formosn.hide();
				        formpocht.hide();
				        formfacta.hide();
				        formbankrekv.hide();
				        formsved.hide();
				        formpunktvud.hide();
				        formidentif.hide();
				        formdocum.hide();
				        formyua.show();
				        btnosn.find('.form-edit').show();
				        $("html, body").animate({ scrollTop: $('.reg-yua').offset().top }, 500);
				    }

				    if(btnpocht.hasClass('disabled')){
				      	$('.form-yua').addClass('fhide');
				        return false
				    } else {
				      	$('.form-yua').removeClass('fhide');
				    }
				});	

			    // Почтовый адрес
				btnpocht.click(function(){
			      	if(btnpocht.hasClass('disabled')){
				      	$('.fhide.form-yua').find('.empty-surely').css({'border-color':'#cc0000'});
				        return false
				    } else {
				        formfacta.hide();
				        formbankrekv.hide();
				        formsved.hide();
				        formpunktvud.hide();
				        formidentif.hide();
				        formdocum.hide();
				        formosn.hide();
				        formyua.hide();
				        formpocht.show();
				        btnyua.find('.form-edit').show();
				        btnpocht.find('.matches-jur').show();
				        $("html, body").animate({ scrollTop: $('.reg-pocht').offset().top }, 500);
				    }

				    if(btnfacta.hasClass('disabled')){
				      	$('.form-pocht').addClass('fhide');
				        return false
				    } else {
				      	$('.form-pocht').removeClass('fhide');
				    }
				});			    

				// Фактический адрес
				btnfacta.click(function(){
			      	if(btnfacta.hasClass('disabled')){
				      	$('.fhide.form-pocht').find('.empty-surely').css({'border-color':'#cc0000'});
				        return false
				    } else {
				        formbankrekv.hide();
				        formsved.hide();
				        formpunktvud.hide();
				        formidentif.hide();
				        formdocum.hide();
				        formosn.hide();
				        formyua.hide();
				        formpocht.hide();
				        formfacta.show();
				        btnpocht.find('.form-edit').show();
				        btnfacta.find('.matches-jur').show();
				        btnfacta.find('.matches-mail').show();
				        $("html, body").animate({ scrollTop: $('.reg-facta').offset().top }, 500);
				    }

				    if(btnbankrekv.hasClass('disabled')){
				      	$('.form-facta').addClass('fhide');
				        return false
				    } else {
				      	$('.form-facta').removeClass('fhide');
				    }

				});				

				// Банковские реквизиты
				btnbankrekv.click(function(){
			      	if(btnbankrekv.hasClass('disabled')){
				      	$('.fhide.form-facta').find('.empty-surely').css({'border-color':'#cc0000'});
				        return false
				    } else {
				        formsved.hide();
				        formpunktvud.hide();
				        formidentif.hide();
				        formdocum.hide();
				        formosn.hide();
				        formyua.hide();
				        formpocht.hide();
				        formfacta.hide();
				        formbankrekv.show();
				        btnfacta.find('.form-edit').show();

				        $("html, body").animate({ scrollTop: $('.reg-bank-rekv').offset().top }, 500);
				    }

				    if(btnsved.hasClass('disabled')){
				      	$('.form-bankrekv').addClass('fhide');
				        return false
				    } else {
				      	$('.form-bankrekv').removeClass('fhide');
				    }

				});				

				// Сведения о лице подавшему заявку
				btnsved.click(function(){
			      	if(btnsved.hasClass('disabled')){
				      	$('.fhide.form-bank-rekv').find('.empty-surely').css({'border-color':'#cc0000'});
				        // btnbankrekv.find('.form-edit').hide();
				        return false
				    } else {
				        formpunktvud.hide();
				        formidentif.hide();
				        formdocum.hide();
				        formosn.hide();
				        formyua.hide();
				        formpocht.hide();
				        formfacta.hide();
				        formbankrekv.hide();
				        formsved.show();
				        btnbankrekv.find('.form-edit').show();
				        $("html, body").animate({ scrollTop: $('.reg-sved').offset().top }, 500);
				    }

				    if(btndocum.hasClass('disabled')){
				      	$('.form-sved').addClass('fhide');
				        return false
				    } else {
				      	$('.form-sved').removeClass('fhide');
				    }

				});

				// Документы
				btndocum.click(function(){
			      	if(btndocum.hasClass('disabled')){
				      	$('.fhide.form-identif').find('.empty-surely').css({'border-color':'#cc0000'});
				        btnsved.find('.form-edit').hide();
				        return false
				    } else {
				        formosn.hide();
				        formyua.hide();
				        formpocht.hide();
				        formfacta.hide();
				        formbankrekv.hide();
				        formsved.hide();
				        formpunktvud.hide();
				        formidentif.hide();
				        formdocum.show();
				        btnsved.find('.form-edit').show();
				        $("html, body").animate({ scrollTop: $('.reg-docum').offset().top }, 500);
				    }

				    if( btnap.hasClass('disabled')){
				      	$('.form-docum').addClass('fhide');
				        return false
				    } else {
				      	$('.form-docum').removeClass('fhide');
				    }

				});				

				// Подать заявку
				btnap.click(function(){
			      	if ( btnap.hasClass('disabled') ) {
				      	$('.form-docum').find('.empty-surely').parents('.file_upload').css({'border-color':'#f00202','box-shadow':'0px 0px 3px #cc0000','color':'#cc0000'});
				      	blcapcha.find('.rcaptcha').css({'color':'#cc0000','background-color':'#f6d8d8'});
				        return false
				    } else {
				        formosn.hide();
				        formyua.hide();
				        formpocht.hide();
				        formfacta.hide();
				        formbankrekv.hide();
				        formsved.hide();
				        formpunktvud.hide();
				        formidentif.hide();
				        formdocum.hide();
				        btndocum.find('.form-edit').show();
				      	$('.form-docum').find('.empty-surely').parents('.file_upload').removeAttr('style');
				      	blcapcha.find('.rcaptcha').removeAttr('style');
				      	// forms.submit();
				    }
			      	$("html, body").animate({ scrollTop: $('.bg-wrapper').offset().top }, 500);
				});				

				// Заполнить позже
				$('.btn-later').click(function(){
			      	if ( $('.btn-later').hasClass('disabled') ) {
				      	$('.fhide.form-identifv').find('.empty-surely').css({'border-color':'#cc0000'});
				      	blcapcha.find('.rcaptcha').css({'color':'#cc0000','background-color':'#f6d8d8'});
				        return false
				    } else {
				        formosn.hide();
				        formyua.hide();
				        formpocht.hide();
				        formfacta.hide();
				        formbankrekv.hide();
				        formsved.hide();
				        formpunktvud.hide();
				        formidentif.hide();
				        formdocum.hide();
				        btnidentif.find('.form-edit').show();
				      	blcapcha.find('.rcaptcha').removeAttr('style');
				      	// forms.submit();
				    }
			      	$("html, body").animate({ scrollTop: $('.bg-wrapper').offset().top }, 500);
				});


			});


	});

function Lat(obj) {
    var rlink = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(.((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(.\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    return rlink.test(obj);
}

function isValidN(validName) {
    var valname = new RegExp(/^[«»""а-яё\s\.\,]+$/i);
    return valname.test(validName);
}

function isValidAll(validNameAll) {
    var valnameall = new RegExp(/^[«\»\"\"\а-яё0-9\s\.\,]+$/i);
    return valnameall.test(validNameAll);
}

function isValidLogin(validNameLogin) {
    var valnamelogin = new RegExp(/^[[A-Za-z0-9\.\-]+$/i);
    return valnamelogin.test(validNameLogin);
}

function isValidLoginName(validNameLoginName) {
    var valnameloginname = new RegExp(/^[[A-Za-z\.\-]+$/i);
    return valnameloginname.test(validNameLoginName);
}

function isValidNumber(validNumber) {
    var valnumber = new RegExp(/^[0-9]+$/i);
    return valnumber.test(validNumber);
}

function isValidEmail1(emailAddress1) {
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    return pattern.test(emailAddress1);
}

$('.defaultReal').realperson();

})(jQuery);