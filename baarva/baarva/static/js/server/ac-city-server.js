$(document).ready(function() {

    get_filter_id = function(element){
      filter_id = element.closest('.list-filter').find('.filter-title').data('filter-id');
      return filter_id;
    };
    get_checked_classes = function(){
      steel_classes = $('.steel_classes');

      class_id_list = [];
      steel_classes.find('.theone').each(function(){
          class_id_list.push($(this).data('object-id'));  
      });
      return class_id_list;
    };

    get_checked_gosts = function(){
      
      gost_id_list = [];
      gost_id = $(".gosts").data('gost-id');
      if ($(".gosts").val()) {
        gost_id_list.push(gost_id);
        }
      else {
          $('.gosts').autocomplete( "option", "source" ).forEach(function(item, i, arr) {
            gost_id_list.push(item.gost_id);
          });
        }
      return gost_id_list;
      };

    get_checked_proizvoditel = function(){
      
      proizvoditel_id_list = [];
      proizvoditel_id = $(".proizvoditel.armatura").data('proizvoditel-id');
      if ($(".proizvoditel.armatura").val()) {
        proizvoditel_id_list.push(proizvoditel_id);
        }
      else {
          source = $('.proizvoditel.armatura').autocomplete( "option", "source" )
          if (source.lenght > 0 )
           {
              source.forEach(function(item, i, arr) {
                proizvoditel_id_list.push(item.id);
              });
            }
        }
      return proizvoditel_id_list;
      };
    
    get_profile_value = function (){
      val = $('.steel_profile').find('.cs-selected').data('value');
      profile_list = [];
      if (val){
        profile_list.push(val);
      }
      return profile_list;
    };

    get_diameter_value = function (){
      val = $('.steel_diameter').find('.cs-selected').data('value');
      diameter_list = [];
      if (val){
        diameter_list.push(val);
      }
      return diameter_list;
    };

    get_mark_value = function (){
      val = $('.steel_mark').find('.cs-selected').data('value');
      mark_list = [];
      if (val){
        mark_list.push(val);
      }
      return mark_list;
    };

    get_steel_length_value = function () {
      val = $('.steel_length').find('.cs-selected').data('value');
      length_list = [];
      if (val){
        length_list.push(val);
      }
      return length_list;

    };
    var timer = 0;
    $('.armatura ').on('keydown', 'input', function() {
        clearTimeout(timer);
        timer = setTimeout(submit_armatura_form, 1500); 
      });

    $('.armatura ').on('keydown', 'input',  function(evt){
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode != 190 && charCode != 46 && charCode > 31 
          && (charCode < 48 || charCode > 57))
           return false;
        return true;
      });


    init_proizvoditel_field = function(value){

      container = $( ".proizvoditel.armatura" ).parent().parent();
      flag = get_profile_value();
      if (flag.length !== 0){
        flag = get_diameter_value()
        gost_id_list = get_checked_gosts();
        if (gost_id_list != '3'){
          if (flag.length !== 0){
            flag = get_mark_value();
          } 
        }
      }
      if (flag.length !== 0 && value) {
        proizvoditel = value;
        $( ".proizvoditel.armatura" ).autocomplete({
            select: function(event, ui){
              $( this ).val( ui.item.value );
              $( this ).data("proizvoditel-id", ui.item.id);
              submit_armatura_form();
            }
          }
          );
        $( ".proizvoditel.armatura" ).autocomplete('option', 'source', proizvoditel);
        $( ".proizvoditel.armatura" ).autocomplete('enable');
        container = $( ".proizvoditel.armatura" ).parent().parent();
        container.find('.closed').hide();
        container.find('.open').show();
        submit_armatura_form();
      }
      else{
        $( ".proizvoditel.armatura" ).autocomplete('option', 'source', []);
        $( ".proizvoditel.armatura" ).autocomplete('disable');
        $( ".proizvoditel.armatura" ).val('')
        container = $( ".proizvoditel.armatura" ).parent().parent();
        container.find('.closed').show();
        container.find('.open').hide();
      }
    };

    init_length_field = function(value){
      l_selector = ".steel_length";
      flag = get_profile_value();
      if (flag.length !== 0){
        flag = get_diameter_value()
        gost_id_list = get_checked_gosts();
        if (gost_id_list != '3'){
          if (flag.length !== 0){
            flag = get_mark_value();
          } 
        }
      }
      if (flag.length !== 0 && value) {
        
        $(l_selector).find('.closed').hide();
        $(l_selector).find('.open').show();
        $(l_selector).find('.open').html(value);

        select = $(l_selector).find('.open').find('.cs-select')[0];
        

        new SelectFx(select, {
          onChange: function(val) {
            $('.armatura input.start').val('');
            $('.armatura input.stop').val('');
            selector = $('.steel_length');
            val = selector.find('.cs-selected').data('value');
            selector.find('select').find('option').each(function()
              {
                if ($(this).val() == val)
                {
                  selector.find('.selections-lenght').text($(this).data('label'));
                } 
              });
            submit_armatura_form();
          }
        });
      }
      else {
        $(l_selector).find('.closed').show();
        $(l_selector).find('.open').html("").hide();

      }
    };
    var proizvoditel = [];  
    init_select_field = function(value, selector){
      
      if (value){
        
        $(selector).find('.closed').hide();
        $(selector).find('.open').html();
        $(selector).find('.open').html(value);
        $(selector).find('.open').show();
       
        select = $(selector).find('.open').find('.cs-select')[0];
          
          new SelectFx(select, {
            onChange: function(val) {
              class_id_list = get_checked_classes();
              steel_profile_list = get_profile_value();
              steel_diameter_list = get_diameter_value();
              filter_id = get_filter_id($(selector));
              submit_armatura_form();
              $.ajax({
                url: '/catalog/armatura/length/',
                type : "GET",
                data : { 
                  class_list: class_id_list,
                  profile_list: steel_profile_list,
                  diameter_list: steel_diameter_list,
                  filter_id: filter_id,
                  },
                success : function(json) {
                  init_length_field(json.steel_length_types_html);
                  init_proizvoditel_field(json.proizvoditel);
                }
              });
            }
          });
      }
      else{
        $(selector).find('.closed').show();
        $(selector).find('.open').html("").hide();
        
      }

    };

    $( document ).on( "click", ".list-klas input:radio", function() {
      
      var inp = $(this);
      
      if (inp.is(".theone")) {
          inp.prop("checked", false).removeClass("theone");
      } else {
          $(".list-klas input:radio[name='"+inp.prop("name")+"'].theone").removeClass("theone");
          inp.addClass("theone");
      }

      class_id_list = get_checked_classes();

      disable_stal();
      disable_diam();

      $.ajax({
          url: '/catalog/armatura/steel/',
          type : "GET",
          data : { class_list : class_id_list },
          success : function(json) {
            steel_mark_list = json.steel_mark_list;
            
            init_select_field(json.profile_list_html, '.steel_profile');           
            init_select_field(json.steel_mark_list_html, '.steel_mark');
            init_select_field(json.steel_diameter_list_html, '.steel_diameter');
            init_length_field("");
            init_proizvoditel_field('');

            submit_armatura_form();
          },
        });
       });
    
    disable_profile = function(){
      field = $(".steel_profile");
      container = $(".filter-sel.steel_profile");
      container.find(".cs-selected").removeClass("cs-selected");
      container.find('.closed').show();
      container.find('.open').hide();
       
    };
    disable_proizvoditel = function(){
      field = $(".proizvoditel");
      field.autocomplete('disable');
      $(".proizvoditel.armatura").val('');
      container = $(".producer.armatura");
      container.find('.closed').show();
      container.find('.open').hide();
    };

    disable_stal = function(){
      field = $(".stal");
      field.autocomplete('disable');
      container = $(".filter-sel.steel_mark");
      container.find(".cs-selected").removeClass("cs-selected");
      container.find('.closed').show();
      container.find('.open').hide();
    };

    disable_diam = function(){
      field = $(".diam");
      field.autocomplete('disable');
      container = $(".filter-sel.steel_diameter");
      container.find(".cs-selected").removeClass("cs-selected");
      container.find('.closed').show();
      container.find('.open').hide();
    };

    disable_length = function(){
      $("#arm_length").prop('disabled', true);
      $("#arm_length_ot").disabled = true;
      $("#arm_length_ot").val("");
      $("#arm_length_do").disabled = true;
      $("#arm_length_do").val("");
      container = $(".filter-sel.steel_length");
      container.find(".cs-selected").removeClass("cs-selected");
      container.find('.closed').show();
      container.find('.open').hide();
    };


    disable_fields = function(){
      disable_proizvoditel();
      disable_stal();
      disable_diam();
      disable_profile();
      disable_diam();
      disable_length();
    };

    init_long_address = function(){
      $(".address-seller p").each(function() {
   
          $(this).html(function(index,text) {
              var str = $(this).text();
                if (str.length > 75) {
                    var res = str.replace(str, str.substring(0, str.indexOf('',75)) + "...");
                    return text.replace(str, res);
                }
          });
      });         
    };

    init_show_tel = function(){
      $('.show-tel').on('click', function(){
          $(this).hide();
          $(this).parents('.tdm_tel').find(".all-tel").animate({
            opacity: "toggle"
            }, {
            duration: "slow"
          });      
    
          return false;
        });
     };

    init_catalog_scroll = function(){
     $(".block-scroll").mCustomScrollbar({
              axis:"y",
              scrollInertia: 1,
              live: "on"
            });
      };

    $(".filter-title").click(function(e){
      
      if ($(this).parent().hasClass('active')){
        filter_id = $(this).data('filter-id');
        
        $.ajax({
            url: '/catalog/filter/',
            type : "GET",
            data : { 
              filter_id: filter_id,
              },
            success : function(json) {
              $('.main-content').html("");
              $('.main-content').html(json.html);
              init_show_tel();
              init_catalog_scroll();

            },
          });
        e.preventDefault();
      }
      else {
        $('.closed').show();
        $('.open').html("").hide();
        submit_armatura_form();
        e.preventDefault();
      } 
    });

    function popupDetail() {
    
      $('.popup-with-form').magnificPopup({
          type: 'inline',
          removalDelay: 300,
          preloader: false,
          focus: '#name'
      });

    }


    $(".proizvoditel").autocomplete({
        source: function(req, responseFn) {
            var prRe = $.ui.autocomplete.escapeRegex(req.term);
            var matcher = new RegExp( "^" + prRe, "i" );
            var pr = $.grep( proizvoditel, function(item,index){
                return matcher.test(item);
            });
            responseFn( pr );
        }
    });

    $(".stal").autocomplete({
        source: function(req, responseFn) {
            var stre = $.ui.autocomplete.escapeRegex(req.term);
            var matcher = new RegExp( "^" + stre, "i" );
            var st = $.grep( stal, function(item,index){
                return matcher.test(item);
            });
            responseFn( st );
        },
        select: function(){
          submit_armatura_form();
        }
    });

    $(".diam").autocomplete({
        source: function(req, responseFn) {
            var dmRe = $.ui.autocomplete.escapeRegex(req.term);
            var matcher = new RegExp( "^" + dmRe, "i" );
            var dm = $.grep( diam, function(item,index){
                return matcher.test(item);
            });
            responseFn( dm );
        }
    });

    init_more_met_button = function(){
      $('.more-met-button').each( function(){
          init_popup($(this));
        });
    };

    init_popup = function(el){
        el.magnificPopup({
          type: 'ajax',
          removalDelay: 300,
          preloader: false,
          focus: '#name',
          callbacks: {
            parseAjax: function(mfpResponse) {
              mfpResponse.data = $(mfpResponse.data)[0].more_met_html;
            },
          }
      });
    };
    
    submit_armatura_form = function(){
      gost_id_list = get_checked_gosts();
      class_id_list = get_checked_classes();
      steel_profile_list = get_profile_value();
      steel_diameter_list = get_diameter_value();
      steel_mark_list = get_mark_value();
      steel_length = get_steel_length_value();
      proizvoditel_id_list = get_checked_proizvoditel();

      steel_length_start = $('.armatura input.start').val();
      steel_length_stop = $('.armatura input.stop').val();

      tax_type = $('.selected_tax_type').data('tax-type');
      batch_type = $('.selected_batch_type').data('batch-type');
      
      if (gost_id_list == '3'){
              $('.steel_mark').hide();
          }
      else{
        $('.steel_mark').show();
      }

      $.ajax({
          url: '/catalog/armatura/form/',
          type : "GET",
          data : { 
            gost_id_list: gost_id_list,
            class_list: class_id_list,
            profile_list: steel_profile_list,
            diameter_list: steel_diameter_list,
            mark_list: steel_mark_list,
            steel_length_type_list: steel_length,
            steel_length_from: steel_length_start,
            steel_length_to: steel_length_stop,
            proizvoditel_list: proizvoditel_id_list,
            tax_type: tax_type,
            batch_type: batch_type,

            },
          success : function(json) {
            $('.main-content').html("");
            $('.main-content').html(json.html);
            init_long_address();
            init_more_met_button();
          },
        });
    };
    $.fn.extend({
      submit_armatura_form: submit_armatura_form,
      init_more_met_button: init_more_met_button,

    });

    //  Фильтер металлопрокат-балка
    var gostsBalka = [
      {
        label: "8239-89 ГОСТ — двутавры стальные горячекатаные",
        value: "8239-89",
        gost: "ГОСТ",
        showlabel: "8239-89",
        desc: "- двутавры стальные горячекатаные"
      },
      {
        label: "19425-74 ГОСТ — балка двутавровая стальная специальная",
        value: "19425-74 ГОСТ",
        gost: "ГОСТ",
        showlabel: "19425-74",
        desc: "- балка двутавровая стальная специальная"
      },
      {
        label: "26020-83 ГОСТ — двутавры стальные горячекатаные",
        value: "26020-83 ГОСТ",
        gost: "ГОСТ",
        showlabel: "10884-94",
        desc: "- двутавры стальные горячекатаные"
      },
      {
        label: "СТО АСЧМ 20-93",
        value: "СТО АСЧМ 20-93",
        showlabel: "СТО АСЧМ 20-93",
        gost: "",
        desc:""
      },
      {
        label: "Сварные двутавры",
        value: "Сварные двутавры",
        showlabel: "Сварные двутавры",
        gost: "",
        desc:""
      }
    ];     

    $( ".gostsBalka" ).autocomplete({
      minLength: 1,
      source: gostsBalka,
      select: function( event, ui ) {
        $( this ).val( ui.item.value );
 
        return false;
      }
    })
    .autocomplete( "instance" )._renderItem = function( ul, item ) {
      return $( "<li></li>" )
        .append( "<a><span>" + item.showlabel + "<small>" + item.gost + "</small><i>" + item.desc + "</i></span></a>" )
        .appendTo( ul );
    };

    var stalBalka = ["Ст0","Ст1кп","Ст1пс","Ст1сп","Ст2кп","Ст2пс","Ст2сп","Ст3кп","Ст3пс","Ст3сп","Ст3Гпс","Ст3Гсп","Ст4кп","Ст4пс","Ст4сп","Ст5пс","Ст5сп","Ст5Гпс","Ст6пс","Ст6сп","Е 185 (Fe 310)","Е 235 (Fe 360)","Е 275 (Fe 430)","Е 355 (Fe 510)","Fe 490","Fe 590","Fe 690" ];     

    $(".stalBalka").autocomplete({
        source: function(req, responseFn) {
            var stbRe = $.ui.autocomplete.escapeRegex(req.term);
            var matcher = new RegExp( "^" + stbRe, "i" );
            var stb = $.grep( stalBalka, function(item,index){
                return matcher.test(item);
            });
            responseFn( stb );
        }
    });

    var sortament = ["Балка двутавровая с параллельными гранями полок","Балка двутавровая с параллельными гранями полок (доп. серия)","Балка широкополочная с параллельными гранями полок","Балка колонная двутавровая с параллельными гранями полок"];         

    $(".sortament").autocomplete({
        source: function(req, responseFn) {
            var smRe = $.ui.autocomplete.escapeRegex(req.term);
            var matcher = new RegExp( "^" + smRe, "i" );
            var sm = $.grep( sortament, function(item,index){
                return matcher.test(item);
            });
            responseFn( sm );
        }
    });    

    var numberDv = ["10","12","14","16","18","20","22","24","27","30","33","36","40","45","50","55","60"];         

    $(".numberDv").autocomplete({
        source: function(req, responseFn) {
            var ndRe = $.ui.autocomplete.escapeRegex(req.term);
            var matcher = new RegExp( "^" + ndRe, "i" );
            var nd = $.grep( numberDv, function(item,index){
                return matcher.test(item);
            });
            responseFn( nd );
        }
    });


//фильтр Катанка

    var gostsKatanka = [
      {
        label: "30136-95 ГОСТ — (катанка обычного качества)",
        value: "30136-95 ГОСТ",
        gost: "ГОСТ",
        showlabel: "30136-95",
        desc: "- (катанка обычного качества)"
      },
      {
        label: "14-1-5283-94 ТУ — (катанка для перетяжки на проволоку)",
        value: "14-1-5283-94 ТУ",
        gost: "ТУ",
        showlabel: "14-1-5283-94",
        desc: "- (катанка для перетяжки на проволоку)"
      },
      {
        label: "14-1-5282-94 ТУ — (катанка для упаковки и других целей)",
        value: "14-1-5282-94 ТУ",
        gost: "ТУ",
        showlabel: "14-1-5282-94",
        desc: "- (катанка для упаковки и других целей)"
      },
      {
        label: "14-121-74-2006 ТУ — (катанка из углеродистой стали)",
        value: "14-121-74-2006 ТУ",
        gost: "ТУ",
        showlabel: "14-121-74-2006",
        desc: "- (катанка из углеродистой стали)"
      }
    ];     

    $( ".gostsKatanka" ).autocomplete({
      minLength: 1,
      source: gostsKatanka,
      select: function( event, ui ) {
        $( this ).val( ui.item.value );
 
        return false;
      }
    })
    .autocomplete( "instance" )._renderItem = function( ul, item ) {
      return $( "<li></li>" )
        .append( "<a><span>" + item.showlabel + "<small>" + item.gost + "</small><i>" + item.desc + "</i></span></a>" )
        .appendTo( ul );
    };     

    var stalKat = ["Ст0","Ст1кп","Ст1пс","Ст1сп","Ст2кп","Ст2пс","Ст2сп","Ст3кп","Ст3пс","Ст3сп"];     

    $(".stalKat").autocomplete({
        source: function(req, responseFn) {
            var stre = $.ui.autocomplete.escapeRegex(req.term);
            var matcher = new RegExp( "^" + stre, "i" );
            var st = $.grep( stalKat, function(item,index){
                return matcher.test(item);
            });
            responseFn( st );
        }
    });    

    var ohlagdenie = [
      {
        label: "УО1 — (одностадийное охлаждение)",
        value: "УО1",
        desc: "- (одностадийное охлаждение)"
      },
      {
        label: "УО1 — (двухстадийное охлаждение)",
        value: "УО1",
        desc: "- (двухстадийное охлаждение)"
      },
      {
        label: "ВО — (охлаждение на воздухе)",
        value: "ВО",
        desc: "- (охлаждение на воздухе)"
      }
    ];     

    $( ".ohlagdenie" ).autocomplete({
      minLength: 1,
      source: ohlagdenie,
      select: function( event, ui ) {
        $( this ).val( ui.item.value );
 
        return false;
      }
    })
    .autocomplete( "instance" )._renderItem = function( ul, item ) {
      return $( "<li></li>" )
        .append( "<a><span>" + item.value + "<i>" + item.desc + "</i></span></a>" )
        .appendTo( ul );
    }; 

    var tochnost = [
      {
        label: "Б — (повышенной точности)",
        value: "Б",
        desc: "- (повышенной точности)"
      },
      {
        label: "В — (обычной точности)",
        value: "В",
        desc: "- (обычной точности)"
      }
    ];     

    $( ".tochnost" ).autocomplete({
      minLength: 1,
      source: tochnost,
      select: function( event, ui ) {
        $( this ).val( ui.item.value );
 
        return false;
      }
    })
    .autocomplete( "instance" )._renderItem = function( ul, item ) {
      return $( "<li></li>" )
        .append( "<a><span>" + item.value + "<i>" + item.desc + "</i></span></a>" )
        .appendTo( ul );
    }; 


    var diamKat = ["5 мм","5,5 мм","6 мм","6,3 мм","6,5 мм","7 мм","8 мм","9 мм","10 мм","11 мм","12 мм","13 мм","14 мм","15 мм","16 мм","17 мм","18 мм","19 мм","20 мм"];       

    $(".diamKat").autocomplete({
        source: function(req, responseFn) {
            var dmRe = $.ui.autocomplete.escapeRegex(req.term);
            var matcher = new RegExp( "^" + dmRe, "i" );
            var dm = $.grep( diamKat, function(item,index){
                return matcher.test(item);
            });
            responseFn( dm );
        }
    });


// фильтр общее    

    $(document).mouseup(function (f){ 
      var uiauto = $('.btn-list'); 
      if (!uiauto.is(f.target) 
        && uiauto.has(f.target).length === 0) { 
        uiauto.parents('.form-group').find('.ui-autocomplete-input').removeClass('hover-autocomplete');  
      }
    });              

    function monkeyPatchAutocomplete() {
        var oldFn = $.ui.autocomplete.prototype._renderItem;

        $.ui.autocomplete.prototype._renderItem = function( ul, item) {
            var re = new RegExp("^" + this.term, "i") ;
            var t = item.label.replace(re,"<strong>" + this.term + "</strong>");
            return $( "<li></li>" )
                .data( "item.autocomplete", item )
                .append( "<a><span>" + t + "</span></a>" )
                .appendTo( ul );
        };
    }

    monkeyPatchAutocomplete();
    

    $('.btn-list-small').click(function () {

      $(this).parents('body').find('.ui-autocomplete').addClass('wrap-small-auto');
      $(this).parents('body').find('.ui-autocomplete').removeClass('wrap-mid-auto');
      $(this).parents('body').find('.ui-autocomplete').removeClass('mw-btn-search');

      $(this).parents('.form-group').find('.ui-autocomplete-input').toggleClass('hover-autocomplete');

      if ( $(this).parents('.form-group').find('.ui-autocomplete-input').hasClass('hover-autocomplete') ) {
        $(this).parents('.form-group').find('.ui-autocomplete-input').autocomplete('option', 'minLength', 0);
        $(this).parents('.form-group').find('.ui-autocomplete-input').autocomplete('search', $(this).val()) ;
      } 

    });  

    $('.btn-list-mid').click(function () {

      $(this).parents('body').find('.ui-autocomplete').addClass('wrap-mid-auto');
      $(this).parents('body').find('.ui-autocomplete').removeClass('wrap-small-auto');
      $(this).parents('body').find('.ui-autocomplete').removeClass('mw-btn-search');

      $(this).parents('.form-group').find('.ui-autocomplete-input').toggleClass('hover-autocomplete');

      if ( $(this).parents('.form-group').find('.ui-autocomplete-input').hasClass('hover-autocomplete') ) {
        $(this).parents('.form-group').find('.ui-autocomplete-input').autocomplete('option', 'minLength', 0);
        $(this).parents('.form-group').find('.ui-autocomplete-input').autocomplete('search', $(this).val()) ;
      }

    });

    $('.btn-search').click(function () {

      $(this).parents('body').find('.ui-autocomplete').addClass('mw-btn-search');
      $(this).parents('body').find('.ui-autocomplete').removeClass('wrap-small-auto');
      $(this).parents('body').find('.ui-autocomplete').removeClass('wrap-mid-auto');

    });              


    $('.btn-list-big').click(function () {

      $(this).parents('body').find('.ui-autocomplete').removeClass('wrap-small-auto');
      $(this).parents('body').find('.ui-autocomplete').removeClass('wrap-mid-auto');
      $(this).parents('body').find('.ui-autocomplete').removeClass('mw-btn-search');

      $(this).parents('.form-group').find('.ui-autocomplete-input').toggleClass('hover-autocomplete');

      if ( $(this).parents('.form-group').find('.ui-autocomplete-input').hasClass('hover-autocomplete') ) {
        $(this).parents('.form-group').find('.ui-autocomplete-input').autocomplete('option', 'minLength', 0);
        $(this).parents('.form-group').find('.ui-autocomplete-input').autocomplete('search', $(this).val()) ;
      }

    });              
               

$('.btn-list').hover(function(){
  $(this).parents('.widget-auto').find('.ui-autocomplete-input').css('border','1px solid #996633');
},function(){
  $(this).parents('.widget-auto').find('.ui-autocomplete-input').css('border','');
})

$(".cs-placeholder").click(function () {   

    $(this).parents('.form-group').find('.cs-active').removeClass('cs-active');

});    

$('.widget-auto').each(function() {

  $(".btn-list").click(function () {   

    $(this).parents('form').find('.cs-active').removeClass('cs-active');  


    	// бегущая строка при hover в списке
		$('.ui-menu-item span').each(function() {

			var widthSpan = $(this).width();
				widthUl = $(this).parents('.ui-autocomplete').width();					
				$(this).css({'width': widthSpan});

        if ( widthUl <= widthSpan ) {

          var widthBol = (widthSpan - widthUl) + 10;

					$(this).parents('.ui-menu-item').hover(function(){
						$(this).find('span')
						.animate({'margin-left': '-' + widthBol }, 3000);
					},
					function(){
						$(this)
						.find('a > span').css({'margin-left':'0'})
						.stop();
					})

				} else {

					$(this).parents('.ui-menu-item').hover(function(){
						$(this)
						.find('a > span').css({'margin-left':'0'})
						.stop();
					})
				}

		});
	});

});

	$(document).mouseup(function (e){ 
        var div = $('.ui-front'); 
        if (!div.is(e.target) 
            && div.has(e.target).length === 0) { 
            div.hide(); 
        }
    });

  var region = ["Республика Адыгея","Республика Башкортостан","АбаРеспублика Бурятия","Удмуртская Республика","Алтайский край","Краснодарский край","Приморский край","Владимирская область","Волгоградская область","Пензенская область","Пермский край","Ростовская область","Рязанская область","Самарская область","Саратовская область","Свердловская область","Тюменская область","Ярославская область","Чеченская республика"];

  var npunkt = ["Абаза","Абакан","Абдулино","Абинск","Автуры","Агидель","Агрыз","Азнакаево","Азов","Айхал","Акбулак","Аксай","Алагир","Алапаевск","Алатырь","Алдан","Алейск","Александров","Александровск","Александровское","Алексеевка","Алексин","Алушта","Альметьевск","Амурск","Анадырь","Анапа","Ангарск","Анжеро-Судженск","Анна","Апатиты","Апрелевка","Апшеронск","Арамиль","Аргун","Ардон","Арзамас","Арзгир","Аркадак","Армавир","Армянск","Арсеньев","Арск","Артем","Артемовский","Архангельск","Асбест","Асино","Астрахань","Аткарск","Афипский","Ахтубинск","Ахтырский","Ачинск","Ачхой-Мартан","Аша","Бавлы","Багаевская","Байкальск","Байконур","Баймак","Бакал","Баксан","Балабаново","Балаково","Балахна","Балашиха","Балашов","Балезино","Балей","Балтийск","Барабинск","Барнаул","Барыш","Батайск","Бахчисарай","Бачатский","Бачи-Юрт","Бежецк","Безенчук","Белая Глина","Белая Калитва","Белгород","Белебей","Белев","Белово","Белогорск","Белокуриха","Белоозерский","Белорецк","Белореченск","Белоярский","Белый Яр","Бердск","Березники","Березовка","Березовский (Кемеровская область)","Березовский (Свердловская область)","Беслан","Бийск","Бикин","Биробиджан","Бирск","Благовещенск (Амурская область)","Благовещенск (Республика Башкортостан)","Благодарный","Бобров","Богданович","Богородицк","Богородск","Боготол","Бодайбо","Бокситогорск","Бологое","Болотное","Большой Камень","Бор","Борзя","Борисовка","Борисоглебск","Боровичи","Боровский","Бородино","Братск","Бронницы","Брюховецкая","Брянск","Бугульма","Бугуруслан","Буденновск","Бузулук","Буинск","Буй","Буйнакск","Бутурлиновка","Валдай","Валуйки","Ванино","Варениковская","Васильево","Великие Луки","Великий Новгород","Великий Устюг","Вельск","Венев","Верещагино","Верхнеднепровский","Верхний Уфалей","Верхняя Пышма","Верхняя Салда","Видное","Вилючинск","Вихоревка","Вичуга","Владивосток","Владикавказ","Владимир","Внуково","Волгоград","Волгодонск","Волгореченск","Волжск","Волжский","Вологда","Волоколамск","Волхов","Вольск","Воргашор","Воркута","Воронеж","Воскресенск","Востряково","Воткинск","Врангель","Всеволожск","Вуктыл","Выборг","Выкса","Выселки","Вышний Волочек","Вяземский","Вязники","Вязьма","Вятские Поляны","Гаврилов-Ям","Гагарин","Гай","Галич","Гатчина","Гвардейск","Геленджик","Георгиевск","Гиагинская","Глазов","Голицыно","Горно-Алтайск","Горняк (Алтайский край)","Горняк (Челябинская область)","Городец","Городище","Гороховец","Горьковский","Горячеводский","Горячий ключ","Грамотеино","Грибановский","Грозный","Грязи","Грязовец","Губаха","Губкин","Губкинский","Гудермес","Гуково","Гулькевичи","Гурьевск","Гусев","Гусиноозерск","Гусь-Хрустальный","Давлеканово","Дагестанские Огни","Далматово","Дальнегорск","Дальнереченск","Данилов","Данков","Дегтярск","Дедовск","Дербент","Десногорск","Джалиль","Джанкой","Дзержинск","Дзержинский","Дивногорск","Дивное","Димитровград","Динская","Дмитров","Добрянка","Долгопрудный","Домодедово","Донецк","Донское","Донской","Дубна","Дубовка","Дугулубгей","Дудинка","Дюртюли","Дятьково","Евпатория","Егорлыкская","Егорьевск","Ейск","Екатеринбург","Елабуга","Елань","Елец","Елизаветинская","Елизово","Еманжелинск","Емва","Енисейск","Ершов","Ессентуки","Ессентукская","Ефремов","Железноводск","Железногорск (Красноярский край)","Железногорск (Курская область)","Железногорск-Илимский","Железнодорожный","Жердевка","Жигулевск","Жирновск","Жуковка","Жуковский","Завитинск","Заводоуковск","Заводской (Приморский край)","Заводской (Республика Северная Осетия - Алания)","Заволжье","Заинск","Заполярный","Зарайск","Заречный (Пензенская область)","Заречный (Свердловская область)","Заринск","Зверево","Зеленогорск","Зеленоград","Зеленодольск","Зеленокумск","Зеленчукская","Зерноград","Зея","Зима","Зимовники","Златоуст","Знаменск","Иваново","Ивантеевка","Ивдель","Игра","Ижевск","Избербаш","Излучинск","Изобильный","Иланский","Ильский","Инза","Иноземцево","Инта","Ипатово","Ирбит","Иркутск","Исилькуль","Искитим","Истра","Ишим","Ишимбай","Йошкар-Ола","Кавалерово","Казань","Кайеркан","Калач","Калач-на-Дону","Калачинск","Калининград","Калининец","Калинино","Калининск","Калтан","Калуга","Калязин","Каменка","Каменск-Уральский","Каменск-Шахтинский","Камень-на-Оби","Камешково","Камские Поляны","Камызяк","Камышин","Камышлов","Канаш","Кандалакша","Каневская","Канск","Кантышево","Карабаново","Карабаш","Карабулак","Карасук","Карачаевск","Карачев","Карпинск","Карталы","Касимов","Касли","Каспийск","Катав-Ивановск","Катайск","Качканар","Кашин","Кашира","Кедровка","Кемерово","Кемь","Керчь","Кизел","Кизилюрт","Кизляр","Кимовск","Кимры","Кингисепп","Кинель","Кинель-Черкассы","Кинешма","Киреевск","Киржач","Кириши","Киров (Калужская область)","Киров (Кировская область)","Кировград","Кирово-Чепецк","Кировск (Ленинградская область)","Кировск (Мурманская область)","Кирсанов","Киселевск","Кисловодск","Климово","Климовск","Клин","Клинцы","Ковдор","Ковров","Ковылкино","Когалым","Кодинск","Козельск","Козьмодемьянск","Коломна","Колпашево","Колпино","Кольцово","Кольчугино","Коммунар","Комсомольск-на-Амуре","Комсомольский","Конаково","Кондопога","Кондрово","Константиновск","Копейск","Кораблино","Кореновск","Коркино","Королев","Корсаков","Коряжма","Косая Гора","Костомукша","Кострома","Котельники","Котельниково","Котельнич","Котлас","Котово","Котовск","Кохма","Коченево","Кочубеевское","Красноармейск (Московская область)","Красноармейск (Саратовская область)","Красновишерск","Красногвардейское","Красногорск","Краснодар","Красное Село","Краснознаменск","Краснокаменск","Краснокамск","Краснообск","Красноперекопск","Краснослободск","Краснотурьинск","Красноуральск","Красноуфимск","Красноярск","Красный Кут","Красный Сулин","Кропоткин","Крыловская","Крымск","Крюково","Кстово","Кубинка","Кувандык","Кудымкар","Кузнецк","Куйбышев","Кукмор","Кулебаки","Кулешовка","Кулунда","Кумертау","Кунгур","Купино","Курагино","Курган","Курганинск","Куровское","Курск","Куртамыш","Курчалой","Курчатов","Куса","Кушва","Кущевская","Кызыл","Кыштым","Кяхта","Лабинск","Лабытнанги","Лагань","Ладожская","Лакинск","Лангепас","Лебедянь","Ленинградская","Лениногорск","Ленинск","Ленинск-Кузнецкий","Ленск","Лермонтов","Лесной","Лесозаводск","Лесосибирск","Ливны","Ликино-Дулево","Линево","Липецк","Лиски","Лобня","Лодейное Поле","Лопатинский","Лосино-Петровский","Луга","Луховицы","Лучегорск","Лысково","Лысьва","Лыткарино","Льгов","Люберцы","Людиново","Лянтор","Магадан","Магас","Магнитогорск","Майкоп","Майма","Майский","Малаховка","Малая Вишера","Малгобек","Малоярославец","Мантурово","Мариинск","Маркс","Матвеев Курган","Махачкала","Мегион","Медведево","Медведовская","Медвежьегорск","Медногорск","Межгорье","Междуреченск","Меленки","Мелеуз","Менделеевск","Мензелинск","Металлострой","Миасс","Миллерово","Минеральные Воды","Минусинск","Мирный (Архангельская область)","Мирный (Республика Саха (Якутия))","Михайловка","Михайловск","Мичуринск","Можайск","Можга","Моздок","Монино","Мончегорск","Морозовск","Моршанск","Москва","Московский","Мостовской","Муравленко","Мурманск","Мурмаши","Муром","Мценск","Мыски","Мытищи","Набережные Челны","Навашино","Навля","Надым","Назарово","Назрань","Нальчик","Наро-Фоминск","Нарткала","Нарьян-Мар","Нахабино","Находка","Невель","Невельск","Невинномысск","Невьянск","Незлобная","Нелидово","Нерехта","Нерчинск","Нерюнгри","Нестеровская","Нефтегорск","Нефтекамск","Нефтекумск","Нефтеюганск","Нижневартовск","Нижнекамск","Нижнеудинск","Нижний Ломов","Нижний Новгород","Нижний Тагил","Нижняя Салда","Нижняя Тура","Никель","Николаевск","Николаевск-на-Амуре","Никольск","Никольско-Архангельский","Никольское","Новая Ляля","Новая Усмань","Новоалександровск","Новоалтайск","Новоаннинский","Нововоронеж","Новодвинск","Новозыбков","Новокубанск","Новокузнецк","Новокуйбышевск","Новомичуринск","Новомосковск","Новопавловск","Новопокровская","Новороссийск","Новосибирск","Новосиликатный","Новосинеглазовский","Новотитаровская","Новотроицк","Новоузенск","Новоульяновск","Новоуральск","Новочебоксарск","Новочеркасск","Новошахтинск","Новый Городок","Новый Оскол","Новый Уренгой","Ногинск","Норильск","Ноябрьск","Нурлат","Нытва","Нягань","Няндома","Обнинск","Обоянь","Обь","Одинцово","Озерск","Озеры","Октябрьск","Октябрьский","Окуловка","Оленегорск","Омск","Омутнинск","Онега","Орджоникидзевская","Орел","Оренбург","Орехово-Зуево","Орловский","Орск","Оса","Осинники","Осташков","Остров","Острогожск","Отрадная","Отрадное","Отрадный","Оха","Очер","Павлово","Павловск (Алтайский край)","Павловск (Воронежская область)","Павловск (Ленинградская область)","Павловская","Павловский Посад","Палласовка","Партизанск","Пашковский","Пенза","Первомайск","Первоуральск","Пересвет","Переславль-Залесский","Пермь","Персиановский","Пестово","Петергоф","Петров Вал","Петровск","Петровск-Забайкальский","Петрозаводск","Петропавловск-Камчатский","Петушки","Печора","Пикалево","Плавск","Пласт","Поворино","Подольск","Подпорожье","Пойковский","Покачи","Покров","Полевской","Полтавская","Полысаево","Полярные Зори","Полярный","Поронайск","Похвистнево","Почеп","Приволжск","Приволжский","Придонской","Приморско-Ахтарск","Приозерск","Приютово","Прокопьевск","Пролетарск","Промышленная","Протвино","Прохладный","Псков","Пугачев","Пушкин","Пушкино","Пущино","Пыть-Ях","Пятигорск","Радужный (Владимирская область)","Радужный (Ханты-Мансийский автономный округ)","Раевский","Разумное","Райчихинск","Раменское","Рассказово","Ревда","Реж","Реутов","Рефтинский","Ржев","Родники","Роза","Рославль","Россошь","Ростов","Ростов-на-Дону","Рошаль","Ртищево","Рубцовск","Рузаевка","Рыбинск","Рыбное","Рыльск","Ряжск","Рязань","Саки","Салават","Салехард","Сальск","Самара","Санкт-Петербург","Саракташ","Саранск","Сарапул","Саратов","Саров","Сасово","Сатка","Сафоново","Саяногорск","Саянск","Светлоград","Светлый","Светогорск","Свирск","Свободный","Свободы","Севастополь","Северо-Задонск","Северобайкальск","Северодвинск","Североморск","Североуральск","Северск","Северская","Сегежа","Селенгинск","Сельцо","Семенов","Семикаракорск","Семилуки","Сергач","Сергиев Посад","Сердобск","Серов","Серпухов","Сертолово","Сестрорецк","Сибай","Сим","Симферополь","Скопин","Славгород","Славянка","Славянск-на-Кубани","Сланцы","Слободской","Слюдянка","Смоленск","Снежинск","Собинка","Советск (Калининградская область)","Советск (Кировская область)","Советская Гавань","Советский","Сокол","Соликамск","Солнечногорск","Солнечный","Соль-Илецк","Сорочинск","Сортавала","Сосновоборск","Сосновый Бор","Сосногорск","Софрино","Сочи","Спасск-Дальний","Среднеуральск","Ставрополь","Старая Купавна","Старая Русса","Стародуб","Староминская","Старощербиновская","Старый Оскол","Степное","Стерлитамак","Стрежевой","Строитель (Белгородская область)","Строитель (Тамбовская область)","Струнино","Ступино","Суворов","Суворовская","Судак","Сузун","Сургут","Суровикино","Сурхахи","Сухиничи","Суходол","Сухой Лог","Сходня","Сызрань","Сыктывкар","Сысерть","Тавда","Таганрог","Тайга","Тайшет","Талица","Талнах","Тальменка","Тамбов","Тара","Тарко-Сале","Татарск","Таштагол","Тбилисская","Тверь","Тейково","Темрюк","Терек","Тимашевск","Тихвин","Тихорецк","Тобольск","Товарково","Тогучин","Тольятти","Томилино","Томск","Топки","Торжок","Торопец","Тосно","Тоцкое Второе","Трехгорный","Троицк (Московская область)","Троицк (Челябинская область)","Троицкая","Трубчевск","Трудовое","Туапсе","Туймазы","Тула","Тулун","Туринск","Тутаев","Тучково","Тында","Тырныауз","Тюмень","Тяжинский","Ува","Уварово","Углич","Удачный","Удомля","Ужур","Узловая","Улан-Удэ","Ульяновск","Унеча","Урай","Урус-Мартан","Урюпинск","Усинск","Усмань","Усолье-Сибирское","Уссурийск","Усть-Абакан","Усть-Джегута","Усть-Илимск","Усть-Катав","Усть-Кут","Усть-Лабинск","Усть-Ордынский","Уфа","Ухта","Учалы","Учкекен","Федоровский","Феодосия","Фокино (Брянская область)","Фокино (Приморский край)","Фролово","Фрязино","Фурманов","Хабаровск","Хадыженск","Ханты-Мансийск","Харабали","Хасавюрт","Химки","Холмск","Холмская","Хотьково","Цимлянск","Цоцин-Юрт","Чайковский","Чалтырь","Чапаевск","Чебаркуль","Чебоксары","Чегдомын","Чегем","Челябинск","Черемхово","Черепаново","Череповец","Черкесск","Черниговка","Черноголовка","Черногорск","Чернушка","Чернянка","Черняховск","Чехов","Чистополь","Чита","Чишмы","Чудово","Чунский","Чусовой","Шадринск","Шали","Шарыпово","Шарья","Шатура","Шахты","Шахунья","Шебекино","Шексна","Шелехов","Шерловая Гора","Шилка","Шилово","Шимановск","Шумерля","Шумиха","Шушары","Шушенское","Шуя","Щекино","Щелково","Щербинка","Щигры","Экажево","Электрогорск","Электросталь","Электроугли","Элиста","Энгельс","Энем","Юбилейный","Югорск","Южа","Южно-Сахалинск","Южноуральск","Южный","Юрга","Юрьев-Польский","Юрюзань","Яблоновский","Якутск","Ялта","Ялуторовск","Янаул","Яранск","Яровое","Ярославль","Ярцево","Ясногорск","Ясный","Яшкино","Алтайский край","Амурская область","Архангельская область","Астраханская область","Белгородская область","Брянская область","Владимирская область","Волгоградская область","Вологодская область","Воронежская область","Еврейская АО","Забайкальский край","Ивановская область","Иркутская область","Кабардино-Балкарская Республика","Калининградская область","Калужская область","Камчатский край","Карачаево-Черкесская Республика","Кемеровская область","Кировская область","Костромская область","Краснодарский край","Красноярский край","Курганская область","Курская область","Ленинградская область","Липецкая область","Магаданская область","Московская область","Мурманская область","Ненецкий АО","Нижегородская область","Новгородская область","Новосибирская область","Омская область","Оренбургская область","Орловская область","Пензенская область","Пермский край","Приморский край","Псковская область","Республика Адыгея","Республика Алтай","Республика Башкортостан","Республика Бурятия","Республика Дагестан","Республика Ингушетия","Республика Калмыкия","Республика Карелия","Республика Коми","Республика Крым","Республика Марий Эл","Республика Мордовия","Республика Саха (Якутия)","Республика Северная Осетия - Алания","Республика Татарстан","Республика Тыва","Республика Хакасия","Ростовская область","Рязанская область","Самарская область","Саратовская область","Сахалинская область","Свердловская область","Смоленская область","Ставропольский край","Тамбовская область","Тверская область","Томская область","Тульская область","Тюменская область","Удмуртская Республика","Ульяновская область","Хабаровский край","Ханты-Мансийский АО","Челябинская область","Чеченская Республика","Чувашская Республика","Чукотский АО","Ямало-Ненецкий АО","Ярославская область"];

  var rayon = ["Акбулак","Аксай","Алагир","Алапаевск","Алатырь","Алдан","Алейск","Александров","Александровск","Александровское","Алексеевка","Алексин","Алушта","Альметьевск","Амурск","Анадырь","Анапа","Ангарск","Анжеро-Судженск","Анна","Апатиты","Апрелевка","Апшеронск","Арамиль","Аргун","Ардон","Арзамас","Арзгир","Аркадак","Армавир","Армянск","Арсеньев","Арск","Артем","Артемовский","Архангельск","Асбест","Асино","Астрахань","Аткарск","Афипский","Ахтубинск","Ахтырский","Ачинск","Ачхой-Мартан","Аша","Бавлы","Багаевская","Байкальск","Байконур","Баймак","Бакал","Баксан","Балабаново","Балаково","Балахна","Балашиха","Балашов","Балезино","Балей","Балтийск","Барабинск","Барнаул","Барыш","Батайск","Бахчисарай","Бачатский","Бачи-Юрт","Бежецк","Безенчук","Белая Глина","Белая Калитва","Белгород","Белебей","Белев","Белово","Белогорск","Белокуриха","Белоозерский","Белорецк","Белореченск","Белоярский","Белый Яр","Бердск","Березники","Березовка","Березовский (Кемеровская область)","Березовский (Свердловская область)","Беслан","Бийск","Бикин","Биробиджан","Бирск","Благовещенск (Амурская область)","Благовещенск (Республика Башкортостан)","Благодарный","Бобров","Богданович","Богородицк","Богородск","Боготол","Бодайбо","Бокситогорск","Бологое","Болотное","Большой Камень","Бор","Борзя","Борисовка","Борисоглебск","Боровичи","Боровский","Бородино","Братск","Бронницы","Брюховецкая","Брянск","Бугульма","Бугуруслан","Буденновск","Бузулук","Буинск","Буй","Буйнакск","Бутурлиновка","Валдай","Валуйки","Ванино","Варениковская","Васильево","Великие Луки","Великий Новгород","Великий Устюг","Вельск","Венев","Верещагино","Верхнеднепровский","Верхний Уфалей","Верхняя Пышма","Верхняя Салда","Видное","Вилючинск","Вихоревка","Вичуга","Владивосток","Владикавказ","Владимир","Внуково","Волгоград","Волгодонск","Волгореченск","Волжск","Волжский","Вологда","Волоколамск","Волхов","Вольск","Воргашор","Воркута","Воронеж","Воскресенск","Востряково","Воткинск","Врангель","Всеволожск","Вуктыл","Выборг","Выкса","Выселки","Вышний Волочек","Вяземский","Вязники","Вязьма","Вятские Поляны","Гаврилов-Ям","Гагарин","Гай","Галич","Гатчина","Гвардейск","Геленджик","Георгиевск","Гиагинская","Глазов","Голицыно","Горно-Алтайск","Горняк (Алтайский край)","Горняк (Челябинская область)","Городец","Городище","Гороховец","Горьковский","Горячеводский","Горячий ключ","Грамотеино","Грибановский","Грозный","Грязи","Грязовец","Губаха","Губкин","Губкинский","Гудермес","Гуково","Гулькевичи","Гурьевск","Гусев","Гусиноозерск","Гусь-Хрустальный","Давлеканово","Дагестанские Огни","Далматово","Дальнегорск","Дальнереченск","Данилов","Данков","Дегтярск","Дедовск","Дербент","Десногорск","Джалиль","Джанкой","Дзержинск","Дзержинский","Дивногорск","Дивное","Димитровград","Динская","Дмитров","Добрянка","Долгопрудный","Домодедово","Донецк","Донское","Донской","Дубна","Дубовка","Дугулубгей","Дудинка","Дюртюли","Дятьково","Евпатория","Егорлыкская","Егорьевск","Ейск","Екатеринбург","Елабуга","Елань","Елец","Елизаветинская","Елизово","Еманжелинск","Емва","Енисейск","Ершов","Ессентуки","Ессентукская","Ефремов","Железноводск","Железногорск (Красноярский край)","Железногорск (Курская область)","Железногорск-Илимский","Железнодорожный","Жердевка","Жигулевск","Жирновск","Жуковка","Жуковский","Завитинск","Заводоуковск","Заводской (Приморский край)","Заводской (Республика Северная Осетия - Алания)","Заволжье","Заинск","Заполярный","Зарайск","Заречный (Пензенская область)","Заречный (Свердловская область)","Заринск","Зверево","Зеленогорск","Зеленоград","Зеленодольск","Зеленокумск","Зеленчукская","Зерноград","Зея","Зима","Зимовники","Златоуст","Знаменск","Иваново","Ивантеевка","Ивдель","Игра","Ижевск","Избербаш","Излучинск","Изобильный","Иланский","Ильский","Инза","Иноземцево","Инта","Ипатово","Ирбит","Иркутск","Исилькуль","Искитим","Истра","Ишим","Ишимбай","Йошкар-Ола","Кавалерово","Казань","Кайеркан","Калач","Калач-на-Дону","Калачинск","Калининград","Калининец","Калинино","Калининск","Калтан","Калуга","Калязин","Каменка","Каменск-Уральский","Каменск-Шахтинский","Камень-на-Оби","Камешково","Камские Поляны","Камызяк","Камышин","Камышлов","Канаш","Кандалакша","Каневская","Канск","Кантышево","Карабаново","Карабаш","Карабулак","Карасук","Карачаевск","Карачев","Карпинск","Карталы","Касимов","Касли","Каспийск","Катав-Ивановск","Катайск","Качканар","Кашин","Кашира","Кедровка","Кемерово","Кемь","Керчь","Кизел","Кизилюрт","Кизляр","Кимовск","Кимры","Кингисепп","Кинель","Кинель-Черкассы","Кинешма","Киреевск","Киржач","Кириши","Киров (Калужская область)","Киров (Кировская область)","Кировград","Кирово-Чепецк","Кировск (Ленинградская область)","Кировск (Мурманская область)","Кирсанов","Киселевск","Кисловодск","Климово","Климовск","Клин","Клинцы","Ковдор","Ковров","Ковылкино","Когалым","Кодинск","Козельск","Козьмодемьянск","Коломна","Колпашево","Колпино","Кольцово","Кольчугино","Коммунар","Комсомольск-на-Амуре","Комсомольский","Конаково","Кондопога","Кондрово","Константиновск","Копейск","Кораблино","Кореновск","Коркино","Королев","Корсаков","Коряжма","Косая Гора","Костомукша","Кострома","Котельники","Котельниково","Котельнич","Котлас","Котово","Котовск","Кохма","Коченево","Кочубеевское","Красноармейск (Московская область)","Красноармейск (Саратовская область)","Красновишерск","Красногвардейское","Красногорск","Краснодар","Красное Село","Краснознаменск","Краснокаменск","Краснокамск","Краснообск","Красноперекопск","Краснослободск","Краснотурьинск","Красноуральск","Красноуфимск","Красноярск","Красный Кут","Красный Сулин","Кропоткин","Крыловская","Крымск","Крюково","Кстово","Кубинка","Кувандык","Кудымкар","Кузнецк","Куйбышев","Кукмор","Кулебаки","Кулешовка","Кулунда","Кумертау","Кунгур","Купино","Курагино","Курган","Курганинск","Куровское","Курск","Куртамыш","Курчалой","Курчатов","Куса","Кушва","Кущевская","Кызыл","Кыштым","Кяхта","Лабинск","Лабытнанги","Лагань","Ладожская","Лакинск","Лангепас","Лебедянь","Ленинградская","Лениногорск","Ленинск","Ленинск-Кузнецкий","Ленск","Лермонтов","Лесной","Лесозаводск","Лесосибирск","Ливны","Ликино-Дулево","Линево","Липецк","Лиски","Лобня","Лодейное Поле","Лопатинский","Лосино-Петровский","Луга","Луховицы","Лучегорск","Лысково","Лысьва","Лыткарино","Льгов","Люберцы","Людиново","Лянтор","Магадан","Магас","Магнитогорск","Майкоп","Майма","Майский","Малаховка","Малая Вишера","Малгобек","Малоярославец","Мантурово","Мариинск","Маркс","Матвеев Курган","Махачкала","Мегион","Медведево","Медведовская","Медвежьегорск","Медногорск","Межгорье","Междуреченск","Меленки","Мелеуз","Менделеевск","Мензелинск","Металлострой","Миасс","Миллерово","Минеральные Воды","Минусинск","Мирный (Архангельская область)","Мирный (Республика Саха (Якутия))","Михайловка","Михайловск","Мичуринск","Можайск","Можга","Моздок","Монино","Мончегорск","Морозовск","Моршанск","Москва","Московский","Мостовской","Муравленко","Мурманск","Мурмаши","Муром","Мценск","Мыски","Мытищи","Набережные Челны","Навашино","Навля","Надым","Назарово","Назрань","Нальчик","Наро-Фоминск","Нарткала","Нарьян-Мар","Нахабино","Находка","Невель","Невельск","Невинномысск","Невьянск","Незлобная","Нелидово","Нерехта","Нерчинск","Нерюнгри","Нестеровская","Нефтегорск","Нефтекамск","Нефтекумск","Нефтеюганск","Нижневартовск","Нижнекамск","Нижнеудинск","Нижний Ломов","Нижний Новгород","Нижний Тагил","Нижняя Салда","Нижняя Тура","Никель","Николаевск","Николаевск-на-Амуре","Никольск","Никольско-Архангельский","Никольское","Новая Ляля","Новая Усмань","Новоалександровск","Новоалтайск","Новоаннинский","Нововоронеж","Новодвинск","Новозыбков","Новокубанск","Новокузнецк","Новокуйбышевск","Новомичуринск","Новомосковск","Новопавловск","Новопокровская","Новороссийск","Новосибирск","Новосиликатный","Новосинеглазовский","Новотитаровская","Новотроицк","Новоузенск","Новоульяновск","Новоуральск","Новочебоксарск","Новочеркасск","Новошахтинск","Новый Городок","Новый Оскол","Новый Уренгой","Ногинск","Норильск","Ноябрьск","Нурлат","Нытва","Нягань","Няндома","Обнинск","Обоянь","Обь","Одинцово","Озерск","Озеры","Октябрьск","Октябрьский","Окуловка","Оленегорск","Омск","Омутнинск","Онега","Орджоникидзевская","Орел","Оренбург","Орехово-Зуево","Орловский","Орск","Оса","Осинники","Осташков","Остров","Острогожск","Отрадная","Отрадное","Отрадный","Оха","Очер","Павлово","Павловск (Алтайский край)","Павловск (Воронежская область)","Павловск (Ленинградская область)","Павловская","Павловский Посад","Палласовка","Партизанск","Пашковский","Пенза","Первомайск","Первоуральск","Пересвет","Переславль-Залесский","Пермь","Персиановский","Пестово","Петергоф","Петров Вал","Петровск","Петровск-Забайкальский","Петрозаводск","Петропавловск-Камчатский","Петушки","Печора","Пикалево","Плавск","Пласт","Поворино","Подольск","Подпорожье","Пойковский","Покачи","Покров","Полевской","Полтавская","Полысаево","Полярные Зори","Полярный","Поронайск","Похвистнево","Почеп","Приволжск","Приволжский","Придонской","Приморско-Ахтарск","Приозерск","Приютово","Прокопьевск","Пролетарск","Промышленная","Протвино","Прохладный","Псков","Пугачев","Пушкин","Пушкино","Пущино","Пыть-Ях","Пятигорск","Радужный (Владимирская область)","Радужный (Ханты-Мансийский автономный округ)","Раевский","Разумное","Райчихинск","Раменское","Рассказово","Ревда","Реж","Реутов","Рефтинский","Ржев","Родники","Роза","Рославль","Россошь","Ростов","Ростов-на-Дону","Рошаль","Ртищево","Рубцовск","Рузаевка","Рыбинск","Рыбное","Рыльск","Ряжск","Рязань","Саки","Салават","Салехард","Сальск","Самара","Санкт-Петербург","Саракташ","Саранск","Сарапул","Саратов","Саров","Сасово","Сатка","Сафоново","Саяногорск","Саянск","Светлоград","Светлый","Светогорск","Свирск","Свободный","Свободы","Севастополь","Северо-Задонск","Северобайкальск","Северодвинск","Североморск","Североуральск","Северск","Северская","Сегежа","Селенгинск","Сельцо","Семенов","Семикаракорск","Семилуки","Сергач","Сергиев Посад","Сердобск","Серов","Серпухов","Сертолово","Сестрорецк","Сибай","Сим","Симферополь","Скопин","Славгород","Славянка","Славянск-на-Кубани","Сланцы","Слободской","Слюдянка","Смоленск","Снежинск","Собинка","Советск (Калининградская область)","Советск (Кировская область)","Советская Гавань","Советский","Сокол","Соликамск","Солнечногорск","Солнечный","Соль-Илецк","Сорочинск","Сортавала","Сосновоборск","Сосновый Бор","Сосногорск","Софрино","Сочи","Спасск-Дальний","Среднеуральск","Ставрополь","Старая Купавна","Старая Русса","Стародуб","Староминская","Старощербиновская","Старый Оскол","Степное","Стерлитамак","Стрежевой","Строитель (Белгородская область)","Строитель (Тамбовская область)","Струнино","Ступино","Суворов","Суворовская","Судак","Сузун","Сургут","Суровикино","Сурхахи","Сухиничи","Суходол","Сухой Лог","Сходня","Сызрань","Сыктывкар","Сысерть","Тавда","Таганрог","Тайга","Тайшет","Талица","Талнах","Тальменка","Тамбов","Тара","Тарко-Сале","Татарск","Таштагол","Тбилисская","Тверь","Тейково","Темрюк","Терек","Тимашевск","Тихвин","Тихорецк","Тобольск","Товарково","Тогучин","Тольятти","Томилино","Томск","Топки","Торжок","Торопец","Тосно","Тоцкое Второе","Трехгорный","Троицк (Московская область)","Троицк (Челябинская область)","Троицкая","Трубчевск","Трудовое","Туапсе","Туймазы","Тула","Тулун","Туринск","Тутаев","Тучково","Тында","Тырныауз","Тюмень","Тяжинский","Ува","Уварово","Углич","Удачный","Удомля","Ужур","Узловая","Улан-Удэ","Ульяновск","Унеча","Урай","Урус-Мартан","Урюпинск","Усинск","Усмань","Усолье-Сибирское","Уссурийск","Усть-Абакан","Усть-Джегута","Усть-Илимск","Усть-Катав","Усть-Кут","Усть-Лабинск","Усть-Ордынский","Уфа","Ухта","Учалы","Учкекен","Федоровский","Феодосия","Фокино (Брянская область)","Фокино (Приморский край)","Фролово","Фрязино","Фурманов","Хабаровск","Хадыженск","Ханты-Мансийск","Харабали","Хасавюрт","Химки","Холмск","Холмская","Хотьково","Цимлянск","Цоцин-Юрт","Чайковский","Чалтырь","Чапаевск","Чебаркуль","Чебоксары","Чегдомын","Чегем","Челябинск","Черемхово","Черепаново","Череповец","Черкесск","Черниговка","Черноголовка","Черногорск","Чернушка","Чернянка","Черняховск","Чехов","Чистополь","Чита","Чишмы","Чудово","Чунский","Чусовой","Шадринск","Шали","Шарыпово","Шарья","Шатура","Шахты","Шахунья","Шебекино","Шексна","Шелехов","Шерловая Гора","Шилка","Шилово","Шимановск","Шумерля","Шумиха","Шушары","Шушенское","Шуя","Щекино","Щелково","Щербинка","Щигры","Экажево","Электрогорск","Электросталь","Электроугли","Элиста","Энгельс","Энем","Юбилейный","Югорск","Южа","Южно-Сахалинск","Южноуральск","Южный","Юрга","Юрьев-Польский","Юрюзань","Яблоновский","Якутск","Ялта","Ялуторовск","Янаул","Яранск","Яровое","Ярославль","Ярцево","Ясногорск","Ясный","Яшкино","Алтайский край","Амурская область","Архангельская область","Астраханская область","Белгородская область","Брянская область","Владимирская область","Волгоградская область","Вологодская область","Воронежская область","Еврейская АО","Забайкальский край","Ивановская область","Иркутская область","Кабардино-Балкарская Республика","Калининградская область","Калужская область","Камчатский край","Карачаево-Черкесская Республика","Кемеровская область","Кировская область","Костромская область","Краснодарский край","Красноярский край","Курганская область","Курская область","Ленинградская область","Липецкая область","Магаданская область","Московская область","Мурманская область","Ненецкий АО","Нижегородская область","Новгородская область","Новосибирская область","Омская область","Оренбургская область","Орловская область","Пензенская область","Пермский край","Приморский край","Псковская область","Республика Адыгея","Республика Алтай","Республика Башкортостан","Республика Бурятия","Республика Дагестан","Республика Ингушетия","Республика Калмыкия","Республика Карелия","Республика Коми","Республика Крым","Республика Марий Эл","Республика Мордовия","Республика Саха (Якутия)","Республика Северная Осетия - Алания","Республика Татарстан","Республика Тыва","Республика Хакасия","Ростовская область","Рязанская область","Самарская область","Саратовская область","Сахалинская область","Свердловская область","Смоленская область","Ставропольский край","Тамбовская область","Тверская область","Томская область","Тульская область","Тюменская область","Удмуртская Республика","Ульяновская область","Хабаровский край","Ханты-Мансийский АО","Челябинская область","Чеченская Республика","Чувашская Республика","Чукотский АО","Ямало-Ненецкий АО","Ярославская область"];
  
  var allsity = ["Акбулак","Аксай","Алагир","Алапаевск","Алатырь","Алдан","Алейск","Александров","Александровск","Александровское","Алексеевка","Алексин","Алушта","Альметьевск","Амурск","Анадырь","Анапа","Ангарск","Анжеро-Судженск","Анна","Апатиты","Апрелевка","Апшеронск","Арамиль","Аргун","Ардон","Арзамас","Арзгир","Аркадак","Армавир","Армянск","Арсеньев","Арск","Артем","Артемовский","Архангельск","Асбест","Асино","Астрахань","Аткарск","Афипский","Ахтубинск","Ахтырский","Ачинск","Ачхой-Мартан","Аша","Бавлы","Багаевская","Байкальск","Байконур","Баймак","Бакал","Баксан","Балабаново","Балаково","Балахна","Балашиха","Балашов","Балезино","Балей","Балтийск","Барабинск","Барнаул","Барыш","Батайск","Бахчисарай","Бачатский","Бачи-Юрт","Бежецк","Безенчук","Белая Глина","Белая Калитва","Белгород","Белебей","Белев","Белово","Белогорск","Белокуриха","Белоозерский","Белорецк","Белореченск","Белоярский","Белый Яр","Бердск","Березники","Березовка","Березовский (Кемеровская область)","Березовский (Свердловская область)","Беслан","Бийск","Бикин","Биробиджан","Бирск","Благовещенск (Амурская область)","Благовещенск (Республика Башкортостан)","Благодарный","Бобров","Богданович","Богородицк","Богородск","Боготол","Бодайбо","Бокситогорск","Бологое","Болотное","Большой Камень","Бор","Борзя","Борисовка","Борисоглебск","Боровичи","Боровский","Бородино","Братск","Бронницы","Брюховецкая","Брянск","Бугульма","Бугуруслан","Буденновск","Бузулук","Буинск","Буй","Буйнакск","Бутурлиновка","Валдай","Валуйки","Ванино","Варениковская","Васильево","Великие Луки","Великий Новгород","Великий Устюг","Вельск","Венев","Верещагино","Верхнеднепровский","Верхний Уфалей","Верхняя Пышма","Верхняя Салда","Видное","Вилючинск","Вихоревка","Вичуга","Владивосток","Владикавказ","Владимир","Внуково","Волгоград","Волгодонск","Волгореченск","Волжск","Волжский","Вологда","Волоколамск","Волхов","Вольск","Воргашор","Воркута","Воронеж","Воскресенск","Востряково","Воткинск","Врангель","Всеволожск","Вуктыл","Выборг","Выкса","Выселки","Вышний Волочек","Вяземский","Вязники","Вязьма","Вятские Поляны","Гаврилов-Ям","Гагарин","Гай","Галич","Гатчина","Гвардейск","Геленджик","Георгиевск","Гиагинская","Глазов","Голицыно","Горно-Алтайск","Горняк (Алтайский край)","Горняк (Челябинская область)","Городец","Городище","Гороховец","Горьковский","Горячеводский","Горячий ключ","Грамотеино","Грибановский","Грозный","Грязи","Грязовец","Губаха","Губкин","Губкинский","Гудермес","Гуково","Гулькевичи","Гурьевск","Гусев","Гусиноозерск","Гусь-Хрустальный","Давлеканово","Дагестанские Огни","Далматово","Дальнегорск","Дальнереченск","Данилов","Данков","Дегтярск","Дедовск","Дербент","Десногорск","Джалиль","Джанкой","Дзержинск","Дзержинский","Дивногорск","Дивное","Димитровград","Динская","Дмитров","Добрянка","Долгопрудный","Домодедово","Донецк","Донское","Донской","Дубна","Дубовка","Дугулубгей","Дудинка","Дюртюли","Дятьково","Евпатория","Егорлыкская","Егорьевск","Ейск","Екатеринбург","Елабуга","Елань","Елец","Елизаветинская","Елизово","Еманжелинск","Емва","Енисейск","Ершов","Ессентуки","Ессентукская","Ефремов","Железноводск","Железногорск (Красноярский край)","Железногорск (Курская область)","Железногорск-Илимский","Железнодорожный","Жердевка","Жигулевск","Жирновск","Жуковка","Жуковский","Завитинск","Заводоуковск","Заводской (Приморский край)","Заводской (Республика Северная Осетия - Алания)","Заволжье","Заинск","Заполярный","Зарайск","Заречный (Пензенская область)","Заречный (Свердловская область)","Заринск","Зверево","Зеленогорск","Зеленоград","Зеленодольск","Зеленокумск","Зеленчукская","Зерноград","Зея","Зима","Зимовники","Златоуст","Знаменск","Иваново","Ивантеевка","Ивдель","Игра","Ижевск","Избербаш","Излучинск","Изобильный","Иланский","Ильский","Инза","Иноземцево","Инта","Ипатово","Ирбит","Иркутск","Исилькуль","Искитим","Истра","Ишим","Ишимбай","Йошкар-Ола","Кавалерово","Казань","Кайеркан","Калач","Калач-на-Дону","Калачинск","Калининград","Калининец","Калинино","Калининск","Калтан","Калуга","Калязин","Каменка","Каменск-Уральский","Каменск-Шахтинский","Камень-на-Оби","Камешково","Камские Поляны","Камызяк","Камышин","Камышлов","Канаш","Кандалакша","Каневская","Канск","Кантышево","Карабаново","Карабаш","Карабулак","Карасук","Карачаевск","Карачев","Карпинск","Карталы","Касимов","Касли","Каспийск","Катав-Ивановск","Катайск","Качканар","Кашин","Кашира","Кедровка","Кемерово","Кемь","Керчь","Кизел","Кизилюрт","Кизляр","Кимовск","Кимры","Кингисепп","Кинель","Кинель-Черкассы","Кинешма","Киреевск","Киржач","Кириши","Киров (Калужская область)","Киров (Кировская область)","Кировград","Кирово-Чепецк","Кировск (Ленинградская область)","Кировск (Мурманская область)","Кирсанов","Киселевск","Кисловодск","Климово","Климовск","Клин","Клинцы","Ковдор","Ковров","Ковылкино","Когалым","Кодинск","Козельск","Козьмодемьянск","Коломна","Колпашево","Колпино","Кольцово","Кольчугино","Коммунар","Комсомольск-на-Амуре","Комсомольский","Конаково","Кондопога","Кондрово","Константиновск","Копейск","Кораблино","Кореновск","Коркино","Королев","Корсаков","Коряжма","Косая Гора","Костомукша","Кострома","Котельники","Котельниково","Котельнич","Котлас","Котово","Котовск","Кохма","Коченево","Кочубеевское","Красноармейск (Московская область)","Красноармейск (Саратовская область)","Красновишерск","Красногвардейское","Красногорск","Краснодар","Красное Село","Краснознаменск","Краснокаменск","Краснокамск","Краснообск","Красноперекопск","Краснослободск","Краснотурьинск","Красноуральск","Красноуфимск","Красноярск","Красный Кут","Красный Сулин","Кропоткин","Крыловская","Крымск","Крюково","Кстово","Кубинка","Кувандык","Кудымкар","Кузнецк","Куйбышев","Кукмор","Кулебаки","Кулешовка","Кулунда","Кумертау","Кунгур","Купино","Курагино","Курган","Курганинск","Куровское","Курск","Куртамыш","Курчалой","Курчатов","Куса","Кушва","Кущевская","Кызыл","Кыштым","Кяхта","Лабинск","Лабытнанги","Лагань","Ладожская","Лакинск","Лангепас","Лебедянь","Ленинградская","Лениногорск","Ленинск","Ленинск-Кузнецкий","Ленск","Лермонтов","Лесной","Лесозаводск","Лесосибирск","Ливны","Ликино-Дулево","Линево","Липецк","Лиски","Лобня","Лодейное Поле","Лопатинский","Лосино-Петровский","Луга","Луховицы","Лучегорск","Лысково","Лысьва","Лыткарино","Льгов","Люберцы","Людиново","Лянтор","Магадан","Магас","Магнитогорск","Майкоп","Майма","Майский","Малаховка","Малая Вишера","Малгобек","Малоярославец","Мантурово","Мариинск","Маркс","Матвеев Курган","Махачкала","Мегион","Медведево","Медведовская","Медвежьегорск","Медногорск","Межгорье","Междуреченск","Меленки","Мелеуз","Менделеевск","Мензелинск","Металлострой","Миасс","Миллерово","Минеральные Воды","Минусинск","Мирный (Архангельская область)","Мирный (Республика Саха (Якутия))","Михайловка","Михайловск","Мичуринск","Можайск","Можга","Моздок","Монино","Мончегорск","Морозовск","Моршанск","Москва","Московский","Мостовской","Муравленко","Мурманск","Мурмаши","Муром","Мценск","Мыски","Мытищи","Набережные Челны","Навашино","Навля","Надым","Назарово","Назрань","Нальчик","Наро-Фоминск","Нарткала","Нарьян-Мар","Нахабино","Находка","Невель","Невельск","Невинномысск","Невьянск","Незлобная","Нелидово","Нерехта","Нерчинск","Нерюнгри","Нестеровская","Нефтегорск","Нефтекамск","Нефтекумск","Нефтеюганск","Нижневартовск","Нижнекамск","Нижнеудинск","Нижний Ломов","Нижний Новгород","Нижний Тагил","Нижняя Салда","Нижняя Тура","Никель","Николаевск","Николаевск-на-Амуре","Никольск","Никольско-Архангельский","Никольское","Новая Ляля","Новая Усмань","Новоалександровск","Новоалтайск","Новоаннинский","Нововоронеж","Новодвинск","Новозыбков","Новокубанск","Новокузнецк","Новокуйбышевск","Новомичуринск","Новомосковск","Новопавловск","Новопокровская","Новороссийск","Новосибирск","Новосиликатный","Новосинеглазовский","Новотитаровская","Новотроицк","Новоузенск","Новоульяновск","Новоуральск","Новочебоксарск","Новочеркасск","Новошахтинск","Новый Городок","Новый Оскол","Новый Уренгой","Ногинск","Норильск","Ноябрьск","Нурлат","Нытва","Нягань","Няндома","Обнинск","Обоянь","Обь","Одинцово","Озерск","Озеры","Октябрьск","Октябрьский","Окуловка","Оленегорск","Омск","Омутнинск","Онега","Орджоникидзевская","Орел","Оренбург","Орехово-Зуево","Орловский","Орск","Оса","Осинники","Осташков","Остров","Острогожск","Отрадная","Отрадное","Отрадный","Оха","Очер","Павлово","Павловск (Алтайский край)","Павловск (Воронежская область)","Павловск (Ленинградская область)","Павловская","Павловский Посад","Палласовка","Партизанск","Пашковский","Пенза","Первомайск","Первоуральск","Пересвет","Переславль-Залесский","Пермь","Персиановский","Пестово","Петергоф","Петров Вал","Петровск","Петровск-Забайкальский","Петрозаводск","Петропавловск-Камчатский","Петушки","Печора","Пикалево","Плавск","Пласт","Поворино","Подольск","Подпорожье","Пойковский","Покачи","Покров","Полевской","Полтавская","Полысаево","Полярные Зори","Полярный","Поронайск","Похвистнево","Почеп","Приволжск","Приволжский","Придонской","Приморско-Ахтарск","Приозерск","Приютово","Прокопьевск","Пролетарск","Промышленная","Протвино","Прохладный","Псков","Пугачев","Пушкин","Пушкино","Пущино","Пыть-Ях","Пятигорск","Радужный (Владимирская область)","Радужный (Ханты-Мансийский автономный округ)","Раевский","Разумное","Райчихинск","Раменское","Рассказово","Ревда","Реж","Реутов","Рефтинский","Ржев","Родники","Роза","Рославль","Россошь","Ростов","Ростов-на-Дону","Рошаль","Ртищево","Рубцовск","Рузаевка","Рыбинск","Рыбное","Рыльск","Ряжск","Рязань","Саки","Салават","Салехард","Сальск","Самара","Санкт-Петербург","Саракташ","Саранск","Сарапул","Саратов","Саров","Сасово","Сатка","Сафоново","Саяногорск","Саянск","Светлоград","Светлый","Светогорск","Свирск","Свободный","Свободы","Севастополь","Северо-Задонск","Северобайкальск","Северодвинск","Североморск","Североуральск","Северск","Северская","Сегежа","Селенгинск","Сельцо","Семенов","Семикаракорск","Семилуки","Сергач","Сергиев Посад","Сердобск","Серов","Серпухов","Сертолово","Сестрорецк","Сибай","Сим","Симферополь","Скопин","Славгород","Славянка","Славянск-на-Кубани","Сланцы","Слободской","Слюдянка","Смоленск","Снежинск","Собинка","Советск (Калининградская область)","Советск (Кировская область)","Советская Гавань","Советский","Сокол","Соликамск","Солнечногорск","Солнечный","Соль-Илецк","Сорочинск","Сортавала","Сосновоборск","Сосновый Бор","Сосногорск","Софрино","Сочи","Спасск-Дальний","Среднеуральск","Ставрополь","Старая Купавна","Старая Русса","Стародуб","Староминская","Старощербиновская","Старый Оскол","Степное","Стерлитамак","Стрежевой","Строитель (Белгородская область)","Строитель (Тамбовская область)","Струнино","Ступино","Суворов","Суворовская","Судак","Сузун","Сургут","Суровикино","Сурхахи","Сухиничи","Суходол","Сухой Лог","Сходня","Сызрань","Сыктывкар","Сысерть","Тавда","Таганрог","Тайга","Тайшет","Талица","Талнах","Тальменка","Тамбов","Тара","Тарко-Сале","Татарск","Таштагол","Тбилисская","Тверь","Тейково","Темрюк","Терек","Тимашевск","Тихвин","Тихорецк","Тобольск","Товарково","Тогучин","Тольятти","Томилино","Томск","Топки","Торжок","Торопец","Тосно","Тоцкое Второе","Трехгорный","Троицк (Московская область)","Троицк (Челябинская область)","Троицкая","Трубчевск","Трудовое","Туапсе","Туймазы","Тула","Тулун","Туринск","Тутаев","Тучково","Тында","Тырныауз","Тюмень","Тяжинский","Ува","Уварово","Углич","Удачный","Удомля","Ужур","Узловая","Улан-Удэ","Ульяновск","Унеча","Урай","Урус-Мартан","Урюпинск","Усинск","Усмань","Усолье-Сибирское","Уссурийск","Усть-Абакан","Усть-Джегута","Усть-Илимск","Усть-Катав","Усть-Кут","Усть-Лабинск","Усть-Ордынский","Уфа","Ухта","Учалы","Учкекен","Федоровский","Феодосия","Фокино (Брянская область)","Фокино (Приморский край)","Фролово","Фрязино","Фурманов","Хабаровск","Хадыженск","Ханты-Мансийск","Харабали","Хасавюрт","Химки","Холмск","Холмская","Хотьково","Цимлянск","Цоцин-Юрт","Чайковский","Чалтырь","Чапаевск","Чебаркуль","Чебоксары","Чегдомын","Чегем","Челябинск","Черемхово","Черепаново","Череповец","Черкесск","Черниговка","Черноголовка","Черногорск","Чернушка","Чернянка","Черняховск","Чехов","Чистополь","Чита","Чишмы","Чудово","Чунский","Чусовой","Шадринск","Шали","Шарыпово","Шарья","Шатура","Шахты","Шахунья","Шебекино","Шексна","Шелехов","Шерловая Гора","Шилка","Шилово","Шимановск","Шумерля","Шумиха","Шушары","Шушенское","Шуя","Щекино","Щелково","Щербинка","Щигры","Экажево","Электрогорск","Электросталь","Электроугли","Элиста","Энгельс","Энем","Юбилейный","Югорск","Южа","Южно-Сахалинск","Южноуральск","Южный","Юрга","Юрьев-Польский","Юрюзань","Яблоновский","Якутск","Ялта","Ялуторовск","Янаул","Яранск","Яровое","Ярославль","Ярцево","Ясногорск","Ясный","Яшкино","Алтайский край","Амурская область","Архангельская область","Астраханская область","Белгородская область","Брянская область","Владимирская область","Волгоградская область","Вологодская область","Воронежская область","Еврейская АО","Забайкальский край","Ивановская область","Иркутская область","Кабардино-Балкарская Республика","Калининградская область","Калужская область","Камчатский край","Карачаево-Черкесская Республика","Кемеровская область","Кировская область","Костромская область","Краснодарский край","Красноярский край","Курганская область","Курская область","Ленинградская область","Липецкая область","Магаданская область","Московская область","Мурманская область","Ненецкий АО","Нижегородская область","Новгородская область","Новосибирская область","Омская область","Оренбургская область","Орловская область","Пензенская область","Пермский край","Приморский край","Псковская область","Республика Адыгея","Республика Алтай","Республика Башкортостан","Республика Бурятия","Республика Дагестан","Республика Ингушетия","Республика Калмыкия","Республика Карелия","Республика Коми","Республика Крым","Республика Марий Эл","Республика Мордовия","Республика Саха (Якутия)","Республика Северная Осетия - Алания","Республика Татарстан","Республика Тыва","Республика Хакасия","Ростовская область","Рязанская область","Самарская область","Саратовская область","Сахалинская область","Свердловская область","Смоленская область","Ставропольский край","Тамбовская область","Тверская область","Томская область","Тульская область","Тюменская область","Удмуртская Республика","Ульяновская область","Хабаровский край","Ханты-Мансийский АО","Челябинская область","Чеченская Республика","Чувашская Республика","Чукотский АО","Ямало-Ненецкий АО","Ярославская область"];
  
  var searchproduckt = ["арматура","балка","катанка","круг","лист","полоса", "Метизы", "крепеж","болты","саморезы","проволока","сетка","уголки","металлопрофил","Промышленные мат","ремни","рукава","топливо","масла","полимерные","химически"];


  $("#searchproduckt").autocomplete({
      source: function(req, responseFn) {
          var re = $.ui.autocomplete.escapeRegex(req.term);
          var matcher = new RegExp( "^" + re, "i" );
          var a = $.grep( searchproduckt, function(item,index){
              return matcher.test(item);
          });
          responseFn( a );
      }
  });   

  $("#allsity").autocomplete({
      source: function(req, responseFn) {
          var re = $.ui.autocomplete.escapeRegex(req.term);
          var matcher = new RegExp( "^" + re, "i" );
          var a = $.grep( allsity, function(item,index){
              return matcher.test(item);
          });
          responseFn( a );
      }
  }); 
  

  $("#order-fld-2").autocomplete({
      source: function(req, responseFn) {
          var re = $.ui.autocomplete.escapeRegex(req.term);
          var matcher = new RegExp( "^" + re, "i" );
          var a = $.grep( region, function(item,index){
              return matcher.test(item);
          });
          responseFn( a );
      }
  });  

  $("#order-fld-1").autocomplete({
      source: function(req, responseFn) {
          var re = $.ui.autocomplete.escapeRegex(req.term);
          var matcher = new RegExp( "^" + re, "i" );
          var a = $.grep( rayon, function(item,index){
              return matcher.test(item);
          });
          responseFn( a );
      }
  });  

  $("#order-fld-3").autocomplete({
      source: function(req, responseFn) {
          var re = $.ui.autocomplete.escapeRegex(req.term);
          var matcher = new RegExp( "^" + re, "i" );
          var a = $.grep( npunkt, function(item,index){
              return matcher.test(item);
          });
          responseFn( a );
      }
  });


// *********************************************************************************

    $('.tab-reg').on('click', function() {
      $(this).addClass('active');
      $(this).parents('.tab-links').find('.tab-n').removeClass('active');
      $(this).parents('.tab-links').find('.tab-r').removeClass('active');
      $(this).parents('.form-city').find('#order-fld-2').show();  
      $(this).parents('.form-city').find('#order-fld-1').hide();  
      $(this).parents('.form-city').find('#order-fld-3').hide();  
    })      


    $('.form-city .form-control').focus(function() {
      $(this).each(function(){
          if($(this).val() != ''){
            $(this).attr('placeholder', '');
          } else {
            $(this).attr('placeholder', '');
          }
        });   
    })   

    $('.form-city .form-control').blur(function() {
      $(this).each(function(){
          if($(this).val() != ''){
            $(this).attr('placeholder', '');
          } else {
            $(this).attr('placeholder', 'Масштаб поиска');
          }
        });   
    })   


    $('.tab-n').on('click', function() {
      $(this).addClass('active');
      $(this).parents('.tab-links').find('.tab-reg').removeClass('active');
      $(this).parents('.tab-links').find('.tab-r').removeClass('active');
      $(this).parents('.form-city').find('#order-fld-1').show();  
      $(this).parents('.form-city').find('#order-fld-2').hide();    
      $(this).parents('.form-city').find('#order-fld-3').hide();    
   })  


    $('.tab-r').on('click', function() {
    	$(this).addClass('active');
      $(this).parents('.tab-links').find('.tab-reg').removeClass('active');
    	$(this).parents('.tab-links').find('.tab-n').removeClass('active');
    	$(this).parents('.form-city').find('#order-fld-3').show();	
      $(this).parents('.form-city').find('#order-fld-2').hide();    
    	$(this).parents('.form-city').find('#order-fld-1').hide();		
   })   


    // поведение отображения блока, выбранного/сохранённого фильтра

      $('.btn-sf').hover(function(){
        $(this).parents('li').addClass('hover-sf');
        $(this).parents('li').next().removeClass('hover-sf');
        $(this).parents('body').find('.ui-autocomplete').hide();
        $(this).parents('.open-filter').find('.cs-select').removeClass('cs-active');
      }, function() {
        $('.list-filter').removeClass('hover-sf');
        $('.selFlist').removeClass('open-rsf');
      }); 

      $('.selections-filter').hover(function(){  
        $(this).parents('li').addClass('hover-sf');
        $(this).parents('li').next().removeClass('hover-sf'); 
      }, function() {
        if ( $('.selFlist').hasClass('open-rsf')) {
          // при клике вне области сохранённого списка, он скрыватся
            $('.bg-wrapper').click(function(e){
              if ($(e.target).closest(".selections-filter").length) return;
              $('.selections-filter').parents('.list-filter').removeClass('hover-sf');  
              $('.selFlist').removeClass('open-rsf');
              e.stopPropagation();
            });
        } else {
          $(this).parents('li').removeClass('hover-sf');  
        }
      }); 


      // удаление сохранённого списка/фильтров
      $('.close-fl').on('click', function() {
        if ( $("div").is(".selFlist") ) {             
          $(this).parents('.selFlist').remove();
        } else {
          $('.list-filter').removeClass('hover-sf');
          $('.list-filter').find('.btn-sf').hide();
        }
      });

      $('.selFlist').each(function() {

        $('.close-fl').on('click', function() {
          if ( $("div").is(".selFlist") ) {             
            $(this).parents('.selFlist').remove();
          } else {
            $('.list-filter').removeClass('hover-sf');
            $('.list-filter').find('.btn-sf').hide();
          }
        });

      }) 

  // сохранённый выбранный фильтр (гост)

  $('.selFlist > a').click(function() {
    $(this).parents('.selFlist').prependTo($(this).parents('.selFlist').parent()).toggleClass('open-rsf');
    $(this).parents('.selFlist').next().removeClass('open-rsf');
  });     


  $('.revise-saved-filter').addClass('no-edit');

  $('.btn-edit-g').on('click', function() {
    $(this).addClass('btn-disabled');
    $(this).parents('.btn-sfg').find('.btn-saved-g').removeClass('btn-disabled');      
    $(this).parents('.selFlist').find('.revise-saved-filter').removeClass('no-edit');
  });

  $('.btn-saved-g').click( function() {
    $(this).addClass('btn-disabled');
    $(this).parents('.btn-sfg').find('.btn-edit-g').removeClass('btn-disabled');
    $(this).parents('.selFlist').find('.revise-saved-filter').addClass('no-edit');
  });

});