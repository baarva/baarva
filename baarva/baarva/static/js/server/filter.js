
$(document).ready(function(){
	
	save_params = function() {
		
		tax_type = $('.selected_tax_type').data('tax-type');
		batch_type = $('.selected_batch_type').data('batch-type');
		
		$.ajax({
    	url: '/user/params/save/',
      type : "POST",
      data : { 
      	tax_type: tax_type,
    		batch_type: batch_type,  	
      },
    });
	};

	$('.tax-type').click(function(e) {

		tax_type = $(this).data('tax-type');
		$('.selected_tax_type').data('tax-type', tax_type);
		save_params();

		if ($('.armatura-filter').hasClass('active'))	{
				$('document').submit_armatura_form();
			}
		e.preventDefault();
	
	});

	$('.batch-type').click(function(e) {

		batch_type = $(this).data('batch-type');
		$('.selected_batch_type').data('batch-type', batch_type);
		save_params();
		
		if ($('.armatura-filter').hasClass('active'))	{
				$('document').submit_armatura_form();
			}
		e.preventDefault();
	
	});

});