﻿(function($){

     $(document).ready(function(){

     	// налогооблажение и опт/розница
		$(".dropdown-toggle").dropdown();
		$('.taxation li').click(function() {
			$(this).parents('.open').removeClass('open');
		});

		// *******
		$(".block-scroll").mCustomScrollbar({
			axis:"y",
			scrollInertia: 1
		});

		$(".ui-autocomplete").niceScroll({
			cursoropacitymin: 1,
			horizrailenabled: false,
			cursorcolor: '#b2b2b2',
			cursorwidth: 3,
			cursorborder: "1px solid #b2b2b2",
			cursorborderradius: 0,
			background: "#ffffff",
			railpadding: {top:1,right:-2,left:0,bottom:0}
		}).hide(); 	

    	$('.ui-autocomplete').hover(function(){  
			$(this).niceScroll().show(); 
	    }, function() {
			$(this).niceScroll().hide(); 
	    }); 

		
		$('.wrap-column-user').columnize({width:260});
		$(".phone").mask("+7(999) 999-99-99");
		$(".teltime").mask("99:99");

		$(document).on("click", ".tooltip", function() {
		    $(this).tooltip(
		        { 
		            items: ".tooltip", 
		            position: { my: "right-15 center", at: "center top-12" },
		            content: function(){
		                return $(this).data('description');
		            }, 
		            close: function( event, ui ) {
		                var me = this;
		                ui.tooltip.hover(
		                    function () {
		                        $(this).stop(true).fadeTo(400, 1); 
		                    },
		                    function () {
		                        $(this).fadeOut("400", function(){
		                            $(this).remove();
		                        });
		                    }
		                );
		                ui.tooltip.on("remove", function(){
		                    $(me).tooltip("destroy");
		                });
		          },
		        }
		    );
		    $(this).tooltip("open");
		});			

		$(document).on("click", ".tooltipRight", function() {
		    $(this).tooltip(
		        { 
		            items: ".tooltipRight", 
		            position: { my: "left-37 center", at: "center top-18" },
		            content: function(){
		                return $(this).data('description');
		            }, 
		            close: function( event, df ) {
		                var gt = this;
		                df.tooltip.hover(
		                    function () {
		                        $(this).stop(true).fadeTo(400, 1); 
		                    },
		                    function () {
		                        $(this).fadeOut("400", function(){
		                            $(this).remove();
		                        });
		                    }
		                );
		                df.tooltip.on("remove", function(){
		                    $(gt).tooltip("destroy");
		                });
		          },
		        }
		    );
		    $(this).tooltip("open");
		    $(this).parents("body").find('.ui-tooltip').addClass("tipRight");
		});	

		// checkbox для фильтра
		$('.list-klas input').each(function(i) {
	        $(this).attr("id","#checkbox" + parseInt(i+1));	
	    });		
	    $('.list-klas label').each(function(i) {
	        $(this).attr("for","#checkbox" + parseInt(i+1));	
	    });


	    // add partners
		$('.bl-choice input').each(function(i) {

			$(this).attr("id","c" + parseInt(i+1)).on( "click", function() {
	            if($(this).is(":checked")) {
	            	$(this).parents('.bl-choice').find('.hint-part').show().text('Уже в партнёрах!');
	            }
	   			else {
					$(this).parents('.bl-choice').find('.hint-part').hide().text("");
	   			}	            
			});	

	    });	

    	$('.bl-choice').hover(function(){  
			if($(this).find('input').is(":checked")) {
            	$(this).find('.hint-part').show().text('Уже в партнёрах!');
            }
   			else {
				$(this).find('.hint-part').show().text('Добавить в партнёры!');
   			}
	    }, function() {
	        $(this).find('.hint-part').hide().text("");
	    }); 

	    $('.bl-choice label').each(function(i) {
	        $(this).attr("for","c" + parseInt(i+1));	
	    });	

	    // ****************

		$('.tipcl').poshytip({
			alignTo: 'target',
			alignX: 'right',			
			offsetX: -105,
			offsetY: 5,
			showTimeout: 50
		});		


		$(".skins").poshytip({
			alignTo: 'target',
			alignX: 'left',			
			offsetX: -50,
			offsetY: -11,
			showTimeout: 50
		});			

		$(".tip-right").poshytip({
			alignTo: 'target',
			alignX: 'right',			
			offsetX: -35,
			offsetY: 5,
			showTimeout: 50
		});		

		$(".tip-right-end").poshytip({
			alignTo: 'target',
			alignX: 'right',			
			offsetX: 0,
			offsetY: 6,
			showTimeout: 50
		});		

		$(".tip-left").poshytip({
			alignTo: 'target',
			alignX: 'left',			
			offsetX: -14,
			offsetY: 5,
			showTimeout: 50
		});

		$('.tipsms').poshytip({
			showOn: 'hover',
			alignTo: 'target',
			alignX: 'center',
			offsetX: 0,
			offsetY: 2,
			showTimeout: 50,
			timeOnScreen: 2000
		});			

		$('.tipbtn').poshytip({
			alignTo: 'target',
			alignX: 'bottom',			
			offsetX: -192,
			offsetY: -10,
			showTimeout: 50
		});			

		$('.tipfile').poshytip({
			alignTo: 'target',
			alignX: 'bottom',			
			offsetX: -24,
			offsetY: 5,
			showTimeout: 50,
			timeOnScreen: 1000
		});			

		$(".gorizont-list").mCustomScrollbar({
			axis:"x",
			advanced:{autoExpandHorizontalScroll:true},
			scrollButtons:{enable:true},
			autoDraggerLength: true,
			snapOffset: 1,
			scrollInertia: 1
		});		

		var pc = $('.gorizont-list .product-collection'),
		    cntGroup = 2; // по сколько элементов в группе
		var divsArr = pc.get();
		for(var i = 0; i < pc.length; i += cntGroup){
		    $(divsArr.slice(i,i+cntGroup)).wrapAll('<div class="wrapper-gor"></div>');
		}


		$(".gorizont-line-one").mCustomScrollbar({
			axis:"x",
			scrollbarPosition: "outside",
			advanced:{autoExpandHorizontalScroll:true},
			scrollButtons:{enable:true},
			autoDraggerLength: true,
			scrollInertia: 1
		});	


		popupDetail();
		lookHide();
		calendarNotes();
		selectMonth();
		addNotes();
		hover();
		selectWrite();
		dropMenu();
		chatMenu();
		mfpRollup();
		mfpCalendar();
		dropMenuSmall();
		infContacts();
		infCall();
		infOtPred();
		myLI();
		myLIop();
		menuFilter();
		zakazatCall();
		vvodTime();
		groupInput();
		validatorFormCacl();
		readMore();
		readMoreOtz();

		$('[data-toggle="tooltip"]').tooltip();



		[].slice.call( document.querySelectorAll( 'select.cs-select' ) ).forEach( function(el) {	
			new SelectFx(el);
		});

		$(".cs-placeholder").click(function () {   
		    $(this).parents('.cs-active').find('ul').niceScroll({
				cursoropacitymin: 1,
				horizrailenabled: false,
				cursorcolor: '#b2b2b2',
				cursorwidth: 3,
				cursorborder: "1px solid #b2b2b2",
				cursorborderradius: 0,
				background: "#ffffff",
				railpadding: {top:1,right:-2,left:0,bottom:0}
			}).hide();


		});

    	$('.cs-options').find('ul').hover(function(){  
			$(this).niceScroll().show(); 
	    }, function() {
			$(this).niceScroll().hide(); 
	    }); 

	// при выборе Длины, отображается уточнение

		var $li = $('#ul_lenght li');

		$li.click(function(event) {

			event.preventDefault();

			var text = $(this).attr('data-value');

			$(this).parents('.form-group').find('.selections-lenght').text(text);

		});

     	// если адрес длинный - продавцы отдельного товара
		$(".address-seller p").each(function() {
	 
	        $(this).html(function(index,text) {
	            var str = $(this).text();
                if (str.length > 75) {
                    var res = str.replace(str, str.substring(0, str.indexOf('',75)) + "...");
                    return text.replace(str, res);
                }
	        });
	    });		


});

// при выборе елемента в списке, отображается в подсказке

onload=function(){
	var m, k, f, l, op, lg;
	m=document.getElementById("ul_1").getElementsByClassName("ng");
	op=document.getElementById("ul_2").getElementsByTagName("li");
	k=m.length;
	f=op.length;
	while(k--){m[k].onclick=myLI;};
	while(f--){op[f].onclick=myLIop;};	
 };
 
function myLI() {
	var s;
	s=this.innerHTML;
	$(".bl-select-taxation .selectionNalog").text(s);
 };	

 function myLIop() {
	var n;
	n=this.innerHTML;
	$(".bl-opt .selectionNalog").text(n);
 };	 

		

 // **********************************************


function popupDetail() {
    
    $('.popup-with-form').magnificPopup({
        type: 'inline',
        removalDelay: 300,
        preloader: false,
        focus: '#name'
    });

}

function calendarNotes() {
	// var calendar = $.calendars.instance('persian');
	// $('#inlineDatepicker').calendarsPicker($.extend(
	// 	{calendar: $.calendars.instance('gregorian', 'ru')},
	// 	$.calendarsPicker.regionalOptions['ru'])
	// );
}

function selectMonth() {

    $('.month').on('click', function() {
    	$(this).parent().find('.list-month').animate({
			height: "toggle",
			opacity: "toggle"
			}, {
			duration: "slow"
		});  
   })     

    $('.list-month span').on('click', function() {
		$(this).parents('.calendars-month-header').find('.list-month').animate({
			height: "toggle",
			opacity: "toggle"
			}, {
			duration: "slow"
		});      
   })  

    $(document).mouseup(function (lm){ 
      var listmonth = $('.list-month'); 
      if (!listmonth.is(lm.target) 
        && listmonth.has(lm.target).length === 0) { 
        listmonth.hide();  
      }
    });   

}

function dropMenu() {

   $('.btn-filter').on('click', function() {
    	$(this).parent('.btn-group').toggleClass('open-filter');
   })    

   $('.click-page-on').on('click', function() {
    	$(this).parent('.bread-crumbs').toggleClass('open-list');
   })     

   $('.drop-menu').on('click', function() {
    	$(this).parent('.btn-group').toggleClass('open');
   })   

   	$('.btn-group').hover(function(){  
    }, function() {
        $(this).removeClass('open');
        // $(this).removeClass('open-filter');
    });       	

    $('.taxation .bread-crumbs').hover(function(){
    }, function() {
        $(this).removeClass('open-list');
    });      

}

function dropMenuSmall() {

   $('.dp-menu').on('click', function() {
    	$(this).parent('.dp-dropdown').toggleClass('open');
   })

   	$('.dp-dropdown').hover(function(){        
    }, function() {
        $(this).removeClass('open');
    });   	

	$('.dp-hover-menu').hover(function(){        
        $(this).parent('.dp-right-menu').find('> span').addClass('active-sub');
    }, function() {
        $(this).parent('.dp-right-menu').find('> span').removeClass('active-sub');
    });

}

function lookHide() {

	$('.show-tel').click(function(){
		$(this).hide();
		$(this).parents('.tdm_tel').find(".all-tel").animate({
			opacity: "toggle"
			}, {
			duration: "slow"
		});

    	return false;
	});

}

function addNotes() {

    $('.add-notes').on('click', function() {
    	$(this).toggleClass("clickadd");
    	$(this).parent().find('.bl-add-notes').animate({
			opacity: "toggle"
			}, {
			duration: "fast"
		});  
   })    

    $(document).mouseup(function (e){ 
        var divBN = $('.bl-add-notes'); 
        if (!divBN.is(e.target) 
            && divBN.has(e.target).length === 0) { 
            divBN.hide(); 
        }
    });    

    $(document).mouseup(function (e){ 
        var divBNM = $('.bl-add-notes-more'); 
        if (!divBNM.is(e.target) 
            && divBNM.has(e.target).length === 0) { 
            divBNM.hide(); 
        }
    });

    $('.addmorenotes').on('click', function() {
    	$(this).parents('.list-notes').find('.bl-add-notes-more').animate({
			height: "toggle",
			opacity: "toggle"
			}, {
			duration: "slow"
		});  
   })     

   $('.ok-notes').on('click', function() {
    	$(this).parents('.list-notes').find('.clickadd').removeClass("clickadd");
    	$(this).parents('.list-notes').find('.bl-add-notes').animate({
			height: "toggle",
			opacity: "toggle"
			}, {
			duration: "slow"
		});  
   })   

   $('.new-orders-number').on('click', function() {
    	$(this).find('.btn-arhive').animate({
			height: "toggle",
			opacity: "toggle"
			}, {
			duration: "fast"
		});  
   })     
    
}

function hover() {

	$('.drophover-menu').hover(function(){        
        $(this).parent('.dropright-menu').find('> span').addClass('active-sub');
    }, function() {
        $(this).parent('.dropright-menu').find('> span').removeClass('active-sub');
    });

	$('.dropsub-menu').hover(function(){        
        $(this).parents('.drophover-menu').find('> li > span').addClass('active-sub');
    }, function() {
        $(this).parents('.drophover-menu').find('> li > span').removeClass('active-sub');
    });


	$('.writeoff-organizations').hover(function(){        
        $(this).find(".more-inform-org").parent('.writeoff-organizations').addClass('select');
    }, function() {
        $(this).find(".more-inform-org").parent('.writeoff-organizations').removeClass('select');
    });	

    $('.page-selection').hover(function(){   
    }, function() {
        $(this).find('.bread-crumbs').removeClass('open');
    });

}

function selectWrite() {

   $('.list-writeoff .writeoff-number span').on('click', function() {
    	$(this).parents(".list-writeoff").toggleClass("selectpr");
   })  

}


function chatMenu() {

   $('.drop-menu').on('click', function() {
    	$(this).parent('.dropdown-chat').toggleClass('open');
   })

   	$('.dropdown-chat').hover(function(){        
    }, function() {
        $(this).removeClass('open');
    });   	

}

function infContacts() {

   $('.inform-contakts').on('click', function() {
    	$(this).addClass('open');
   })   

    $(".footer-contakts").click(function() { 
		$('html,body').animate({
            scrollTop: $('.inform-contakts').addClass('open').offset().top},
            'slow'); 
    });

   	$('.block-inf-contakts').hover(function(){        
    }, function() {
        $(this).parents(".inform-contakts").removeClass('open');
    });   	

}

function infCall() {

    $('.request-call').click(function() {
		$(this).parents('.user-order-call').toggleClass('open');
	});
	
    $('.bg-wrapper').click(function(k){
      if ( $(k.target).closest(".request-call").length || $(k.target).closest(".block-inf-call").length ) return;
      $('.block-inf-call').parents('.user-order-call').removeClass('open');
      // k.stopPropagation();
    });	

}

function infOtPred() {

   	$('.ready-vopr').hover(function(){        
        $(this).parents(".top-otpred").addClass('hideVop');
    }, function() {
        $(this).parents(".top-otpred").removeClass('hideVop');
    });   	   	

    $('.inf-op').hover(function(){        
        $(this).parents(".top-otpred").addClass('hideVop');
    }, function() {
        $(this).parents(".top-otpred").removeClass('hideVop');
    });   	

}

function mfpRollup() {

   $('.mfp-rollup').on('click', function() {
   		$.magnificPopup.close('fast');
   		$(this).parents('body').find('.popupRollUp').animate({
			height: "toggle",
			opacity: "toggle"
			}, {
			duration: 1000
		});  
   })   

   $('.close-chat').on('click', function() {
   		$(this).parents('.content-tarnslation').find('.popupRollUp').animate({
			height: "toggle",
			opacity: "toggle"
			}, {
			duration: 1000
		});  
   })   

   $('.mfp-rollopen').on('click', function() {
   		$(this).parents('.popupRollUp').hide();
   })

}

function mfpCalendar() {

   $('.for-calendar-filter a').on('click', function() {
    	$(this).parents('.for-calendar-filter').find('.block-calendar').toggleClass('open');
   })   

   $('.mfp-close').on('click', function() {
    	$(this).parents('.for-calendar-filter').find('.block-calendar').removeClass('open');
   })

   	$('.block-calendar').hover(function(){   
    }, function() {
        $(this).removeClass('open');
    });   

}

function isValidName(validName) {
    var valname = new RegExp(/^[А-Яа-яЁё]+$/);
    return valname.test(validName);
}

function zakazatCall() {
	$('.rf').each(function(){
    // Объявляем переменные (форма и кнопка отправки)
	var form = $(this),
        btn = form.find('.btn_submit');

    // Добавляем каждому проверяемому полю, указание что поле пустое
	form.find('.rfield').addClass('empty_field');

    // Функция проверки полей формы
    function checkInput(){
      form.find('.rfield').each(function(){

            if ( $(this).val().length == 0) {
		       $(this).addClass('empty_field');
		    }  else { 	
		       $(this).removeClass('empty_field');
		       $(this).removeAttr('style');
		    }       
      });      

      form.find('.alf').each(function(){

      		if ( $(this).val().length == 0) {
		       $(this).addClass('empty_field');
		    } else if ( !isValidName($(this).val()) ) { 	
		       $(this).addClass('empty_field');
		    }  else { 			      
		       $(this).removeClass('empty_field');
       	       $(this).removeAttr('style');
		    }       

      });      

    }

    // Функция подсветки незаполненных полей
    function lightEmpty(){
      form.find('.empty_field').css({'border-color':'#cc0000'});
      form.find('.empty_field').parents('.block-inf-call').find('.mes-errors').show();
      setTimeout(function(){
         form.find('.empty_field').parents('.block-inf-call').find('.mes-errors').hide();
      },4000);

    }

    // Проверка в режиме реального времени
    setInterval(function(){
      // Запускаем функцию проверки полей на заполненность
	  checkInput();
      // Считаем к-во незаполненных полей
      var sizeEmpty = form.find('.empty_field').size();
      // Вешаем условие-тригер на кнопку отправки формы
      if(sizeEmpty > 0){
        if(btn.hasClass('disabled')){
          return false
        } else {
          btn.addClass('disabled')
        }
      } else {
        btn.removeClass('disabled')
      }
    },500);

    // Событие клика по кнопке отправить
    btn.click(function(){
      if($(this).hasClass('disabled')){
        // подсвечиваем незаполненные поля и форму не отправляем, если есть незаполненные поля
		lightEmpty();
        return false
      } else {
        // Все хорошо, все заполнено, отправляем форму
        form.submit();
      }
        $(this).parents('.user-order-call').find('#call-ok').show();
    });
  });
}

// фильтр меню с алфавитной сортировкой

function menuFilter() {

	$('#cssmenu > ul > .list-filter').each(function() {

		$(this).addClass('abc');

		$(this).find('> a').click(function(){

			$(this).parents('#cssmenu .dropdown-menu .list-filter').prependTo($(this).parents('#cssmenu .dropdown-menu .list-filter').parent()).toggleClass('active').toggleClass('abc');
				// алфавитная сортировка
				$('#cssmenu .dropdown-menu .abc > a').click(function () {
					var mylist = $(this).parents('#cssmenu .dropdown-menu');
					var listitems = mylist.children('.abc').get();
					listitems.sort(function(a, b) {
					   var compA = $(a).text().toUpperCase();
					   var compB = $(b).text().toUpperCase();
					   return (compA < compB) ? -1 : (compA > compB) ? 1 : 0;
					})

					$.each(listitems, function(idx, itm) { mylist.append(itm); });

				});	

				$(this).parents('#cssmenu .dropdown-menu .list-filter').next().removeClass('active').addClass('abc');

				//очистка формы

				$(this).next('ul').find('input:radio').each(function() {
					var inpall=$(this);
					inpall.prop("checked",false).removeClass("theone");
				});				

				$(this).next('ul').find("input").val('');
				$(this).next('ul').find(".cs-placeholder").empty();

		});

		$(this).find('.btn-save').click(function(){
      		$(this).parents('ul.dropdown-menu li').find('a .btn-sf').show();

			$(this).parents('#cssmenu .dropdown-menu .list-filter').prependTo($(this).parents('#cssmenu .dropdown-menu .list-filter').parent()).toggleClass('active').toggleClass('abc');
				// алфавитная сортировка
				$('#cssmenu .dropdown-menu .abc .btn-save').click(function () {
					var mylist = $(this).parents('#cssmenu .dropdown-menu');
					var listitems = mylist.children('.abc').get();
					listitems.sort(function(a, b) {
					   var compA = $(a).text().toUpperCase();
					   var compB = $(b).text().toUpperCase();
					   return (compA < compB) ? -1 : (compA > compB) ? 1 : 0;
					})

					$.each(listitems, function(idx, itm) { mylist.append(itm); });

				});	

				$(this).parents('#cssmenu .dropdown-menu .list-filter').next().removeClass('active').addClass('abc');
				$('html, body').animate({
	                    scrollTop: $('.bl-filter').offset().top
	                }, 300);

		});

	});	


}

function vvodTime() {

	$.mask.definitions['H']='[012]';
	$.mask.definitions['M']='[012345]';
	$('#eITDbegintime').mask('H9:M9');
	$('#eITDendtime').mask('H9:M9');
	
}


function groupInput() {

	$('.tr-inn .form-control').groupinputs();

	$('.tr-inn .form-control').on('input propertychange', function(e) {
	    var elem = $(e.target),
	        value = elem.val(),
	        caret = elem.caret(),
	        newValue = value.replace(/[^0-9]/g, ''),
	        valueDiff = value.length - newValue.length;

	    if (valueDiff) {
	        elem
	            .val(newValue)
	            .caret(caret.start - valueDiff, caret.end - valueDiff);
	    }
	});
	
}

// валидация формы калькулятора, при закзае продукта
function validatorFormCacl() {

	$('.top-moremet').each(function(){
	var formCalc = $('.top-moremet'),
        btnCalc = formCalc.find('.btn-calculator');
        btnCart = formCalc.find('.btn-add-cart');

	formCalc.find('.inpCalcl input').addClass('empty_calc');

    function calcInput(){
      formCalc.find('.inpCalcl input').each(function(){
	        if ( $(this).val().length == "" ) {
		       $(this).parents('.inpCalcl').addClass('empty_calc');
		    }  else { 	
		       $(this).parents('.inpCalcl').removeClass('empty_calc');
		       $(this).removeAttr('style');
		    }       
	  }); 

    }

    function calcEmpty(){
      formCalc.find('.empty_calc').css({'border-color':'#cc0000'});
    }

    setInterval(function(){
	  calcInput();
      var sizeCalc = formCalc.find('.empty_calc input').size();
      if(sizeCalc > 0){
        if(btnCalc.hasClass('disabled')){
          return false
        } else {
          btnCalc.addClass('disabled')
        }        

        if(btnCart.hasClass('disabled')){
          return false
        } else {
          btnCart.addClass('disabled')
        }

      } else {
        btnCart.removeClass('disabled')
        btnCalc.removeClass('disabled')
      }
    },500);


    btnCalc.click(function(){
      if($(this).hasClass('disabled')){
		calcEmpty();
        return false
      } else {
      }
    });    

    btnCart.click(function(){
      if($(this).hasClass('disabled')){
		calcEmpty();
        return false
      } else {
        var magnificPopup = $.magnificPopup.instance; 
		magnificPopup.close(); 
      }
    });

  });

}

// для новостей - читать подробно
function readMore() {


	$('.menu-inf-tab a').click(function (e) {
	  	e.preventDefault()
	  	$(this).tab('show').parents('.block-inf-products').find('.text-news').readmore({
			speed: 75,
		  	moreLink: '<div class="wrap-ml"><a class="morelink" href="#">подробно&nbsp;&raquo;</a></div>',
		  	lessLink: '<div class="wrap-ml"><a class="morelink" href="#">&laquo;&nbsp;скрыть</a></div>'
		});   

	})

    $('.text-news').each(function() {
    	
		$(this).readmore({
			speed: 75,
		  	moreLink: '<div class="wrap-ml"><a class="morelink" href="#">подробно&nbsp;&raquo;</a></div>',
		  	lessLink: '<div class="wrap-ml"><a class="morelink" href="#">&laquo;&nbsp;скрыть</a></div>'
		});    	

	});

}


// для отзывов - читать подробно
function readMoreOtz() {
    var moretextOtz = "подробно&nbsp;&raquo;";
    var lesstextOtz = "&laquo;&nbsp;скрыть";
    
    $('.otz-company').each(function() {
    	var showHeightOtz = $(this).find('.text-news').height();  

 		$(this).find('.text-news-otz').css('height',showHeightOtz);

        var htmlmoreOtz = '<span class="morelinkOtz">' + moretextOtz + '</span>';
        var htmllessOtz = '<span class="lesslinkOtz">' + lesstextOtz + '</span>';

        if( showHeightOtz > 45 ) {
 
			$(this).find(".text-news").after(htmlmoreOtz);
			$(this).find(".text-news").after(htmllessOtz);
 			$(this).find(".morelinkOtz").show();
            $(this).find('.text-news').css('height','45px');
        }


	    $(".morelinkOtz").click(function(){
	            $(this).parents('.otz-company').find('.lesslinkOtz').show();
	            $(this).hide();
	            $(this).parents('.otz-company').find('.text-news').css('height','auto');
	    });		    

	    $(".lesslinkOtz").click(function(){
	            $(this).hide();
	            $(this).parents('.otz-company').find('.morelinkOtz').show();
	            $(this).parents('.otz-company').find('.text-news').css('height','45px');
	    });	 

    });    

}

})(jQuery);