﻿(function($){

    $(document).ready(function(){

    		$('.forms-otpr').each(function(){
				var form = $(this),
			        btn = form.find('.btn');

				form.find('.req').addClass('errors-fl');
				form.find('.req-em').addClass('errors-fl');

			    function checkInput(){

			      form.find('.req').each(function(){

			      		if ( $(this).val().length == 0) {
					       $(this).addClass('errors-fl');
					       $(this).parents('.block-mes').removeClass('icon-ok');
					    } else if ( !isValidName($(this).val()) ) { 	
					       $(this).addClass('errors-fl');
					       $(this).parents('.block-mes').removeClass('icon-ok');
					    } else { 			      
					       $(this).removeClass('errors-fl');
					    }       

			      });  			      

			      form.find('.req-em').each(function(){

			      		if (!isValidEmailAddress($(this).val())) { 	
					       $(this).addClass('errors-fl');
					       $(this).parents('.block-mes').removeClass('icon-ok');
					    }  else { 			      
					    	$(this).removeClass('errors-fl');
					        $(this).parents('.block-mes').removeClass('errors-fl');
					    }       

			      });      

			    }

			    function lightEmpty(){

					$('.errors-fl').css('border','1px solid #cc0000');

			    }

			    setInterval(function(){
				  checkInput();
			      var sizeEmpty = form.find('.errors-fl').size();
			      if(sizeEmpty > 0){
			        if(btn.hasClass('disabled')){
			          return false
			        } else {
			          btn.addClass('disabled')
			        }
			      } else {
			        btn.removeClass('disabled')
			      }
			    },500);

			    btn.click(function(){
			      if($(this).hasClass('disabled')){
					lightEmpty();
			        return false
			      } else {
			        form.submit();
			        $(this).parents('.forms-otpr').find('#form-ok-mes').show();
			      }
			    });
			  });


    	validatorFormOP();

	});

function Latin(obj) {
   if (/^[a-zA-Z0-9 ,.\-:"()]*?$/.test(obj.value)) 
      obj.defaultValue = obj.value;
   else 
      obj.value = obj.defaultValue;
}

function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    return pattern.test(emailAddress);
}

function isValidName(validName) {
    var valname = new RegExp(/^[А-Яа-яЁё]+$/);
    return valname.test(validName);
}

function validatorFormOP() {

	var textN = $('#inputName');
	var textO = $('#inputMiddle');
	var textE = $('#inputEmail');
	var textS = $('#inputSubject');
	var textP = $('#phone');
	var textT = $('#mes-text');

	textP.focus(function(){

	    if ( textN.val().length == 0) {
	       textN.css('border','1px solid #cc0000');   
	       textN.parents('.form-group').find('.blRequired').show();
	       textN.parents('.form-group').find('.noValid').hide();    
	    } else if ( !isValidName(textN.val()) ) { 	
	       textN.css('border','1px solid #cc0000');
	       textN.parents('.form-group').find('.blRequired').hide();
	       textN.parents('.form-group').find('.noValid').show();       
	    }  else { 	
	       textN.css('border','1px solid #b2b2b2');
	       textN.parents('.block-mes').addClass('icon-ok');
	       textN.parents('.form-group').find('.blRequired').hide();
	       textN.parents('.form-group').find('.noValid').hide();             
	   } 

	    if ( textO.val().length == 0) {
	       textO.css('border','1px solid #cc0000');
	       textO.parents('.block-mes').find('.infmore').show();
	       textO.parents('.form-group').find('.blRequired').show();
	       textO.parents('.form-group').find('.noValid').hide();    
	    } else if ( !isValidName(textO.val()) ) { 	
	       textO.css('border','1px solid #cc0000');
	       textO.parents('.form-group').find('.blRequired').hide();
	       textO.parents('.form-group').find('.noValid').show();       
	    }  else { 	
	       textO.css('border','1px solid #b2b2b2');
	       textO.parents('.block-mes').addClass('icon-ok');
	       textO.parents('.form-group').find('.blRequired').hide();
	       textO.parents('.form-group').find('.noValid').hide();     
	       textO.parents('.block-mes').find('.infmore').hide();     
	    } 

	    if (  textE.val().length == 0 ) {
	       textE.css('border','1px solid #cc0000');
	       textE.parents('.block-mes').find('.infmore').show();
	       textE.parents('.form-group').find('.blRequired').show();
	       textE.parents('.form-group').find('.noValid').hide();    
	    } else if (!isValidEmailAddress(textE.val())) {   	
		    textE.css('border','1px solid #cc0000');
	       textE.parents('.form-group').find('.blRequired').hide();
	       textE.parents('.form-group').find('.noValid').show();       
	    } else {
	       textE.css('border','1px solid #b2b2b2');
	       textE.parents('.block-mes').addClass('icon-ok');
	       textE.parents('.form-group').find('.blRequired').hide();
	       textE.parents('.form-group').find('.noValid').hide();   
	       textE.parents('.block-mes').find('.infmore').hide();   
	    }

	    if ( textS.val().length == 0) {
	       textS.css('border','1px solid #b2b2b2');
	       textS.parents('.form-group').find('.blRequired').hide();
	       textS.parents('.form-group').find('.noValid').hide();    
	       textS.parents('.block-mes').find('.infmore').hide();    
	    } else {   	
	       textS.css('border','1px solid #b2b2b2');
	       textS.parents('.block-mes').addClass('icon-ok');
	       textS.parents('.form-group').find('.blRequired').hide();
	       textS.parents('.form-group').find('.noValid').hide();    
	       textS.parents('.block-mes').find('.infmore').hide();    
	    }

	});

	textT.focus(function(){

	    if ( textN.val().length == 0) {
	       textN.css('border','1px solid #cc0000');
	       textN.parents('.form-group').find('.blRequired').show();
	       textN.parents('.form-group').find('.noValid').hide();    
	    } else if ( !isValidName(textN.val()) ) { 	
	       textN.css('border','1px solid #cc0000');
	       textN.parents('.form-group').find('.blRequired').hide();
	       textN.parents('.form-group').find('.noValid').show();       
	    }  else { 	
	       textN.css('border','1px solid #b2b2b2');
	       textN.parents('.block-mes').addClass('icon-ok');
	       textN.parents('.form-group').find('.blRequired').hide();
	       textN.parents('.form-group').find('.noValid').hide();       
	   }     

	    if ( textO.val().length == 0) {
	       textO.css('border','1px solid #cc0000');
	       textO.parents('.block-mes').find('.infmore').show();
	       textO.parents('.form-group').find('.blRequired').show();
	       textO.parents('.form-group').find('.noValid').hide();    
	    } else if ( !isValidName(textO.val()) ) { 	
	       textO.css('border','1px solid #cc0000');
	       textO.parents('.form-group').find('.blRequired').hide();
	       textO.parents('.form-group').find('.noValid').show();       
	    }  else { 	
	       textO.css('border','1px solid #b2b2b2');
	       textO.parents('.block-mes').addClass('icon-ok');
	       textO.parents('.form-group').find('.blRequired').hide();
	       textO.parents('.form-group').find('.noValid').hide();      
	       textO.parents('.block-mes').find('.infmore').hide();      
	    } 

	    if (  textE.val().length == 0 ) {
	    	textE.css('border','1px solid #cc0000');
			textE.parents('.block-mes').find('.infmore').show();
			textE.parents('.form-group').find('.blRequired').show();
			textE.parents('.form-group').find('.noValid').hide();    
	    } else if (!isValidEmailAddress(textE.val())) {   	
		    textE.css('border','1px solid #cc0000');
			textE.parents('.form-group').find('.blRequired').hide();
			textE.parents('.form-group').find('.noValid').show();       
	    } else {
	       textE.css('border','1px solid #b2b2b2');
	       textE.parents('.block-mes').addClass('icon-ok');
			textE.parents('.form-group').find('.blRequired').hide();
			textE.parents('.form-group').find('.noValid').hide();      
			textE.parents('.block-mes').find('.infmore').hide();      
	    }

	    if ( textS.val().length == 0) {
			textS.css('border','1px solid #cc0000');
			textS.parents('.block-mes').find('.infmore').show();
			textS.parents('.form-group').find('.blRequired').show();
			textS.parents('.form-group').find('.noValid').hide();    
	    } else {   	
			textS.css('border','1px solid #b2b2b2');
			textS.parents('.block-mes').addClass('icon-ok');
			textS.parents('.form-group').find('.blRequired').hide();
			textS.parents('.form-group').find('.noValid').hide();      
			textS.parents('.block-mes').find('.infmore').hide();      
	   }  

	});


	textN.blur(function(){
	  
	    if ( textN.val().length == 0) {
	       textN.css('border','1px solid #cc0000');
	       textN.parents('.form-group').find('.blRequired').show();
	       textN.parents('.form-group').find('.noValid').hide();    
	    } else if ( !isValidName(textN.val()) ) { 	
	       textN.css('border','1px solid #cc0000');
	       textN.parents('.form-group').find('.blRequired').hide();
	       textN.parents('.form-group').find('.noValid').show();      
	    }  else { 	
	       textN.css('border','1px solid #b2b2b2');
	       textN.parents('.block-mes').addClass('icon-ok');
	       textN.parents('.form-group').find('.blRequired').hide();
	       textN.parents('.form-group').find('.noValid').hide();     
	    } 

	});
	textN.focus(function(){
	  
	    if ( textN.val().length == 0) {
	    } else if ( !isValidName(textN.val()) ) { 	
	       textN.css('border','1px solid #cc0000');
	       textN.parents('.form-group').find('.blRequired').hide();
	       textN.parents('.form-group').find('.noValid').show();          
	    }  else { 	
	       textN.css('border','1px solid #b2b2b2');       
	       textN.parents('.block-mes').addClass('icon-ok');       
	       textN.parents('.form-group').find('.blRequired').hide();
	       textN.parents('.form-group').find('.noValid').hide();           
	    }    

	    if ( textO.val().length == 0) {
	       textO.css('border','1px solid #b2b2b2');
	       textO.parents('.form-group').find('.blRequired').hide();
	       textO.parents('.form-group').find('.noValid').hide();       
	       textO.parents('.block-mes').find('.infmore').hide();       
	    } else if ( !isValidName(textO.val()) ) { 	
	       textO.css('border','1px solid #cc0000');
	       textO.parents('.form-group').find('.blRequired').hide();
	       textO.parents('.form-group').find('.noValid').show();       
	    }  else { 	
	       textO.css('border','1px solid #b2b2b2');
	       textO.parents('.block-mes').addClass('icon-ok');
	       textO.parents('.form-group').find('.blRequired').hide();
	       textO.parents('.form-group').find('.noValid').hide();        
	       textO.parents('.block-mes').find('.infmore').hide();        
	    } 

	    if ( textE.val().length == 0 ) {
	    	textE.css('border','1px solid #b2b2b2');
	       textE.parents('.form-group').find('.blRequired').hide();
	       textE.parents('.form-group').find('.noValid').hide();      
	       textE.parents('.block-mes').find('.infmore').hide();      
	    } else if (!isValidEmailAddress(textE.val())) {   	
		    textE.css('border','1px solid #cc0000');
	       textE.parents('.form-group').find('.blRequired').hide();
	       textE.parents('.form-group').find('.noValid').show();      
	    } else {
	       textE.css('border','1px solid #b2b2b2');
	       textE.parents('.block-mes').addClass('icon-ok');
	       textE.parents('.form-group').find('.blRequired').hide();
	       textE.parents('.form-group').find('.noValid').hide();      
	       textE.parents('.block-mes').find('.infmore').hide();      
	    }

	    if ( textS.val().length == 0) {
	       textS.css('border','1px solid #b2b2b2');
	       textS.parents('.form-group').find('.blRequired').hide();
	       textS.parents('.form-group').find('.noValid').hide();      
	       textS.parents('.block-mes').find('.infmore').hide();      
	   } else {   	
	       textS.css('border','1px solid #b2b2b2');
	       textS.parents('.block-mes').addClass('icon-ok');
	       textS.parents('.form-group').find('.blRequired').hide();
	       textS.parents('.form-group').find('.noValid').hide();      
	       textS.parents('.block-mes').find('.infmore').hide();      
	    }

	});


	textO.blur(function(){

	    if ( textO.val().length == 0) {
	       textO.css('border','1px solid #cc0000');
	       textO.parents('.block-mes').find('.infmore').show();
	       textO.parents('.form-group').find('.blRequired').show();
	       textO.parents('.form-group').find('.noValid').hide();       
	    } else if ( !isValidName(textO.val()) ) { 	
	       textO.css('border','1px solid #cc0000');
	       textO.parents('.form-group').find('.blRequired').hide();
	       textO.parents('.form-group').find('.noValid').show();        
	    }  else { 	
	       textO.css('border','1px solid #b2b2b2');
	       textO.parents('.block-mes').addClass('icon-ok');
	       textO.parents('.form-group').find('.blRequired').hide();
	       textO.parents('.form-group').find('.noValid').hide();       
	       textO.parents('.block-mes').find('.infmore').hide();       
	    }     

	});
	textO.focus(function(){

	    if ( textN.val().length == 0) {
	       textN.css('border','1px solid #cc0000');
	       textN.parents('.form-group').find('.blRequired').show();
	       textN.parents('.form-group').find('.noValid').hide();       
	    } else if ( !isValidName(textN.val()) ) { 	
	       textN.css('border','1px solid #cc0000');
	       textN.parents('.form-group').find('.blRequired').hide();
	       textN.parents('.form-group').find('.noValid').show();         
	    }  else { 	
	       textN.css('border','1px solid #b2b2b2');
	       textN.parents('.block-mes').addClass('icon-ok');
	       textN.parents('.form-group').find('.blRequired').hide();
	       textN.parents('.form-group').find('.noValid').hide();           
	    } 

	    if ( textO.val().length == 0) {
	    } else if ( !isValidName(textO.val()) ) { 	
	       textO.css('border','1px solid #cc0000');
	       textO.parents('.form-group').find('.blRequired').hide();
	       textO.parents('.form-group').find('.noValid').show();        
	       textO.parents('.block-mes').find('.infmore').show();        
	    }  else { 	
	       textO.css('border','1px solid #b2b2b2');
	       textO.parents('.block-mes').addClass('icon-ok');
	       textO.parents('.form-group').find('.blRequired').hide();
	       textO.parents('.form-group').find('.noValid').hide();        
	       textO.parents('.block-mes').find('.infmore').hide();        
	    } 

	    if ( textE.val().length == 0 ) {
	    	textE.css('border','1px solid #b2b2b2');
	       textE.parents('.form-group').find('.blRequired').hide();
	       textE.parents('.form-group').find('.noValid').hide();      
	       textE.parents('.block-mes').find('.infmore').hide();      
	    } else if (!isValidEmailAddress(textE.val())) {   	
		    textE.css('border','1px solid #cc0000');
	       textE.parents('.form-group').find('.blRequired').hide();
	       textE.parents('.form-group').find('.noValid').show();       
	    } else {
	       textE.css('border','1px solid #b2b2b2');
	       textE.parents('.block-mes').addClass('icon-ok');
	       textE.parents('.form-group').find('.blRequired').hide();
	       textE.parents('.form-group').find('.noValid').hide();      
	       textE.parents('.block-mes').find('.infmore').hide();      
	    }

	    if ( textS.val().length == 0) {
	       textS.css('border','1px solid #b2b2b2');
	       textS.parents('.form-group').find('.blRequired').hide();
	       textS.parents('.form-group').find('.noValid').hide();      
	       textS.parents('.block-mes').find('.infmore').hide();      
	    } else {   	
	       textS.css('border','1px solid #b2b2b2');
	       textS.parents('.block-mes').addClass('icon-ok');
	       textS.parents('.form-group').find('.blRequired').hide();
	       textS.parents('.form-group').find('.noValid').hide();      
	       textS.parents('.block-mes').find('.infmore').hide();      
	    }    

	});


	textE.blur(function(){
	  
	    if (  textE.val().length == 0 ) {
	    	textE.css('border','1px solid #cc0000');
	       textE.parents('.block-mes').find('.infmore').show();
	       textE.parents('.form-group').find('.blRequired').show();
	       textE.parents('.form-group').find('.noValid').hide();    
	    } else if (!isValidEmailAddress(textE.val())) {   	
		    textE.css('border','1px solid #cc0000');
	       textE.parents('.form-group').find('.blRequired').hide();
	       textE.parents('.form-group').find('.noValid').show();       
	    } else {
	       textE.css('border','1px solid #b2b2b2');
	       textE.parents('.block-mes').addClass('icon-ok');
	       textE.parents('.form-group').find('.blRequired').hide();
	       textE.parents('.form-group').find('.noValid').hide();      
	       textE.parents('.block-mes').find('.infmore').hide();      
	    }
	 
	});
	textE.focus(function(){

	    if ( textN.val().length == 0) {
	       textN.css('border','1px solid #cc0000');
	       textN.parents('.form-group').find('.blRequired').show();
	       textN.parents('.form-group').find('.noValid').hide();    
	    } else if ( !isValidName(textN.val()) ) { 	
	       textN.css('border','1px solid #cc0000');
	       textN.parents('.form-group').find('.blRequired').hide();
	       textN.parents('.form-group').find('.noValid').show();       
	    }  else { 	
	       textN.css('border','1px solid #b2b2b2');
	       textN.parents('.block-mes').addClass('icon-ok');
	       textN.parents('.form-group').find('.blRequired').hide();
	       textN.parents('.form-group').find('.noValid').hide();      
	   } 

	    if ( textO.val().length == 0) {
	       textO.css('border','1px solid #cc0000');
	       textO.parents('.block-mes').find('.infmore').show();
	       textO.parents('.form-group').find('.blRequired').show();
	       textO.parents('.form-group').find('.noValid').hide();    
	    } else if ( !isValidName(textO.val()) ) { 	
	       textO.css('border','1px solid #cc0000');
	       textO.parents('.form-group').find('.blRequired').hide();
	       textO.parents('.form-group').find('.noValid').show();       
	   }  else { 	
	       textO.css('border','1px solid #b2b2b2');
	       textO.parents('.block-mes').addClass('icon-ok');
	       textO.parents('.form-group').find('.blRequired').hide();
	       textO.parents('.form-group').find('.noValid').hide();      
	       textO.parents('.block-mes').find('.infmore').hide();    
	    } 

	    if (  textE.val().length == 0 ) {
	    } else if (!isValidEmailAddress(textE.val())) {  
	    } else {
	       textE.css('border','1px solid #b2b2b2');
	       textE.parents('.block-mes').addClass('icon-ok');
	       textE.parents('.form-group').find('.blRequired').hide();
	       textE.parents('.form-group').find('.noValid').hide();      
	       textE.parents('.block-mes').find('.infmore').hide();
	    }

	    if ( textS.val().length == 0) {
	       textS.css('border','1px solid #b2b2b2');
	       textS.parents('.form-group').find('.blRequired').hide();
	       textS.parents('.form-group').find('.noValid').hide();      
	       textS.parents('.block-mes').find('.infmore').hide();   
	    } else {   	
	       textS.css('border','1px solid #b2b2b2');
	       textS.parents('.block-mes').addClass('icon-ok');
	       textS.parents('.form-group').find('.blRequired').hide();
	       textS.parents('.form-group').find('.noValid').hide();      
	       textS.parents('.block-mes').find('.infmore').hide();    
	    }    

	});


	textS.blur(function(){
	  
	    if (textS.val().length == 0) {
			textS.css('border','1px solid #cc0000');
	       textS.parents('.block-mes').find('.infmore').show();
	       textS.parents('.form-group').find('.blRequired').show();
	       textS.parents('.form-group').find('.noValid').hide();    
	    }else{ 
			textS.css('border','1px solid #b2b2b2');
			textS.parents('.block-mes').addClass('icon-ok');
	       textS.parents('.form-group').find('.blRequired').hide();
	       textS.parents('.form-group').find('.noValid').hide();      
	       textS.parents('.block-mes').find('.infmore').hide();    
	    } 

	});
	textS.focus(function(){

	    if ( textN.val().length == 0) {
	       textN.css('border','1px solid #cc0000');
	       textN.parents('.form-group').find('.blRequired').show();
	       textN.parents('.form-group').find('.noValid').hide();    
	   } else if ( !isValidName(textN.val()) ) { 	
	       textN.css('border','1px solid #cc0000');
	       textN.parents('.form-group').find('.blRequired').hide();
	       textN.parents('.form-group').find('.noValid').show();       
	    }  else { 	
	       textN.css('border','1px solid #b2b2b2');
	       textN.parents('.block-mes').addClass('icon-ok');
	       textN.parents('.form-group').find('.blRequired').hide();
	       textN.parents('.form-group').find('.noValid').hide();    
	    } 

	    if ( textO.val().length == 0) {
	       textO.css('border','1px solid #cc0000');
	       textO.parents('.form-group').find('.blRequired').show();
	       textO.parents('.form-group').find('.noValid').hide();    
	    } else if ( !isValidName(textO.val()) ) { 	
	       textO.css('border','1px solid #cc0000');
	       textO.parents('.form-group').find('.blRequired').hide();
	       textO.parents('.form-group').find('.noValid').show();       
	    }  else { 	
	       textO.css('border','1px solid #b2b2b2');
	       textO.parents('.block-mes').addClass('icon-ok');
	       textO.parents('.form-group').find('.blRequired').hide();
	       textO.parents('.form-group').find('.noValid').hide();      
	       textO.parents('.block-mes').find('.infmore').hide();     
	    } 


	    if (  textE.val().length == 0 ) {
	    	textE.css('border','1px solid #cc0000');
	        textE.parents('.block-mes').find('.infmore').show();
	        textE.parents('.form-group').find('.blRequired').show();
	        textE.parents('.form-group').find('.noValid').hide();    
	    } else if (!isValidEmailAddress(textE.val())) {   	
		    textE.css('border','1px solid #cc0000');
	        textE.parents('.form-group').find('.blRequired').hide();
	        textE.parents('.form-group').find('.noValid').show();       
	   } else {
	        textE.css('border','1px solid #b2b2b2');
	        textE.parents('.block-mes').addClass('icon-ok');
	        textE.parents('.form-group').find('.blRequired').hide();
	        textE.parents('.form-group').find('.noValid').hide();      
	        textE.parents('.block-mes').find('.infmore').hide();    
	    }

	    if ( textS.val().length == 0) {
	    } else {   	
	       textS.css('border','1px solid #b2b2b2');
	       textS.parents('.block-mes').addClass('icon-ok');
	       textS.parents('.form-group').find('.blRequired').hide();
	       textS.parents('.form-group').find('.noValid').hide();      
	       textS.parents('.block-mes').find('.infmore').hide();
	   }

	});

	$('.close').click(function() {
		$(this).parents('.form-group').find('alert-success').hide();
	});	

}



})(jQuery);