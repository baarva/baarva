<script type="text/javascript">
    $(document).ready(function() {
      var gostsArmatura = [
      {% for gost in gost_list %}
        {
          label: "{{ gost.gost.number }} {{ gost.gost.gost_type }} ({{ gost.gost.title }})",
          value: "{{ gost.gost.number }} {{ gost.gost.gost_type }}",
          gost: "{{ gost.gost.gost_type }}",
          showlabel: "{{ gost.gost.number }}",
          desc: "- ({{ gost.gost.title }})",
          gost_id: "{{ gost.gost.id }}"
        },
      {% endfor %}
    ];
    {% if gost_list %}   
      var active_gost = "{{ active_gost.number }} {{ active_gost.gost_type }}"
    
      $( ".gosts" ).autocomplete({
        minLength: 1,
        source: gostsArmatura,
        select: function( event, ui ) {
          
          $( this ).val( ui.item.value );
          $( this ).data("gost-id", ui.item.gost_id);
          
          $.ajax({
            url: '/catalog/armatura/gost/',
            type : "GET",
            data : { gost_id : ui.item.gost_id },
            success : function(json) {
              container = $('.steel_classes');
              container.find('.closed').hide();
              container.find('.open').html(json.html);
              container.find('.open').show();
              disable_fields();
              submit_armatura_form();
            },
          });
          
          return false;
        }
      })
      .autocomplete( "instance" )._renderItem = function( ul, item ) {
        return $( "<li></li>" )
          .append( "<a><span>" + item.showlabel + "<small>" + item.gost + "</small><i>" + item.desc + "</i></span></a>" )
          .appendTo( ul );
      };    

      $('#cssmenu').addClass('open-filter');
      $('.list-filter.default').addClass('active');
      $('.gosts').autocomplete('search', active_gost);
      $('.ui-menu-item')[0].click();
      
    $( ".gosts" ).keyup(function() {
      val = $( this ).val();
      if (val){
            }
      else {
            $('.closed').show();
            $('.open').html("").hide();
            submit_armatura_form();
      
          }
        });
    {% endif %}
    $(".proizvoditel.armatura").autocomplete({
      source: [],
        }
      );
  }
);
</script>
