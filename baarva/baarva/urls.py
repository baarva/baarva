# -*- coding: utf-8 -*-

from django.conf import settings
from django.conf.urls import include, url

from django.contrib import admin

from baarva.apps.catalog import urls as catalog_urls
from baarva.apps.catalog_armatura import urls as catalog_armatura_urls
from baarva.apps.pages import urls as pages_urls
from baarva.apps.seller import urls as seller_urls
from baarva.apps.web import urls as web_urls

urlpatterns = [
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^admin/', include(admin.site.urls)),
]

urlpatterns += [
    url(r'^', include(web_urls)),
    url(r'^catalog/', include(catalog_urls)),
    url(r'^catalog/armatura/', include(catalog_armatura_urls)),
    url(r'^seller/', include(seller_urls)),
    url(r'^pages/', include(pages_urls)),
]
if settings.DEBUG:
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns
    from django.conf.urls.static import static

    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)


handler404 = 'baarva.apps.web.views.page_not_found'
handler500 = 'baarva.apps.web.views.server_error'
